# Diagramas elétricos e eletrônicos

## Diagram Unifilar: Alimentação da Carga


<p style="text-indent: 50px; text-align: justify;">O diagrama elétrico da parte de energia segue de acordo com o que foi dimensionado no tópico de arquitetura de energia. Um diagrama elétrico, ou unifilar, consiste basicamente em uma representação de como será o circuito, mostrando as ligações entre os componente e suas descrições, afim de melhorar o entendimento à cerca do que será construido, assim como ajudar na hora de execução de montagem do protótipo e para fins de comparação entre o teórico e prático.</p>

<div align="center">
  <img alt="Figura 01: Diagram Unifilar da Alimentação de Carga (Própria, 2024) " src="../../assets/images/Diagramauni.png">
  <h4>Figura 01: Diagram Unifilar da Alimentação de Carga (Própria, 2024)</h4>
</div>

