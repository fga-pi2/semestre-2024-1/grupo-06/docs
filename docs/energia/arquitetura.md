# **Arquitetura de Energia**

## Projeto de Energia

<p style="text-indent: 50px; text-align: justify;"> O projeto de energia para o fogão de indução inteligente segue como base a problemática proposta. Tendo como base a utilização do fogão ao ar livre, para uma situação que se assemelhe a um acampamento ou a uma atividade de campo que não conte com uma fonte direta de energia, o fogão utilizará de um sistema off-grid ou autônomo de abastecimento de energia, afim de abastecer o sistema com a carga necessária para operação correta do sistema.
</p>

### Sistema autônomo de energia

<p style="text-indent: 50px; text-align: justify;">
Os sistemas OFF-Grid são conhecidos como sistemas isolados ou, também, conhecidos como sistemas não conectados à rede elétrica. Estes sistemas trabalham de forma autônoma, isto é, não trabalham em paralelo com a rede elétrica convencional <a href="#ref-1">[1]</a>.
</p>

<p style="text-indent: 50px; text-align: justify;">Os sistemas off-grid são amplamente utilizados em áreas rurais e em locais isolados onde o acesso à rede elétrica convencional é limitado. Graças à sua viabilidade e sustentabilidade, esses sistemas autônomos são altamente eficazes nesses ambientes.</p>

<p style="text-indent: 50px; text-align: justify;">
No contexto de nosso projeto, enfrentamos desafios semelhantes aos das residências situadas em áreas distantes das cidades e sem conexão com o sistema elétrico nacional integrado. Embora a maior parte do Brasil seja coberta por essa rede, algumas regiões, especialmente na Amazônia, permanecem sem acesso devido a dificuldades logísticas e custos de infraestrutura <a href="#ref-2">[2]</a>. Nessas áreas remotas, os sistemas autônomos tornam-se essenciais para suprir a demanda energética dos residentes.
</p>

<p style="text-indent: 50px; text-align: justify;">Além disso, muitos aventureiros podem se dirigir a locais remotos desprovidos de energia. Nesses casos, o sistema autônomo para fogões podem atender perfeitamente às suas necessidades energéticas.</p>

<p style="text-indent: 50px; text-align: justify;">É importante notar que os sistemas off-grid diferem dos sistemas on-grid em sua composição. Enquanto os últimos dependem de módulos fotovoltaicos, inversores e string boxes, os sistemas off-grid requerem também baterias e controladores de carga.</p>

### Equipamentos de um Sistema Autônomo

#### Módulos Fotovoltáicos

<p style="text-indent: 50px; text-align: justify;">
Os módulos fotovoltaicos são os equipamentos principais de um sistema fotovoltaico. Eles são os responsáveis por coletar a energia solar e converter em energia elétrica. Geralmente são equipamentos compostos por um conjunto de células fotovoltaicas, estas por suas vez podem ser classificadas em cristalinas (subdivididas em monocristalinas e policristalinas) e amorfas <a href="#ref-3">[3]</a>. Geralmente as células são compostas por elementos singelos como o silício.
</p>

<p style="text-indent: 50px; text-align: justify;">
Além de contar com as células fotovoltaicas, os módulos possuem uma cobertura frontal, geralmente de vidro, possui ainda um encapsulante que se trata de um polímero termoplástico transparente, eletricamente isolante e resistente à umidade, à fadiga mecânica e à ação da radiação solar (principalmente raios ultravioleta) <a href="#ref-3">[3]</a>. Além dos equipamentos já citados, O módulo ainda conta com uma cobertura posterior, geralmente feito de PVF (fluoreto de polivinil) e ainda uma moldura metálica, comumente utilizado o alumínio, essa estrutura de metal confere uma rigidez mecânica melhor ao equipamento. Todas as camadas do painel fotovoltaico podem ser visualizados na figura 1.
</p>

<div align="center">
  <img alt="Figura 01: Corte de um Módulo Fotovoltaico (PRIEB, 2002) " src="../../assets/images/Modulo_Fotovoltaico.png">
  <h4>Figura 01: Corte de um Módulo Fotovoltaico <a href="#ref-3">[3]</a></h4>
</div>

#### Inversor

<p style="text-indent: 50px; text-align: justify;">A definição básica para o inversor se consiste em um equipamento que converte corrente CC (corrente contínua) em CA (corrente alternada). Segundo Germanos <a href="#ref-7">[7]</a>, a função de um inversor de potência é converter corrente contínua em corrente alternada, como já foi mencionado. Seu princípio de funcionamento é baseado em sistemas de chaveamento que ligam e desligam a corrente elétrica (i), gerada por uma fonte contínua, com uma determinada frequência (v).</p>

<p style="text-indent: 50px; text-align: justify;">São muitos os tipos de inversor, entre eles podemos mencionar: </p>

<ul>
  <li><p style="text-align: justify;">Inversor solar on-grid: é o modelo mais utilizado no mundo, é o inversor usado para receber a carga que vem das placas e destinar essa carga para a rede, transformando a corrente CC que vem dos módulos em CA, para a compatibilidade com a rede.</p></li>
  <li><p style="text-align: justify;">Inversor solar off-grid: são os inversores que não são conectados a rede, como o próprio nome diz, ele foi desenvolvido para sistemas autônomos e utilizam da corrente CC proveniente das baterias, geralmente de 12, 24 e 48 V, e a transforma em corrente CA, no Brasil podendo ser em 110/220V a depender da região.</p></li>
  <li><p style="text-align: justify;">Inversor solar híbrido: são aqueles inversores que permitem operar sobre as duas condições, tanto conectados a rede, como não. Eles tem dispositivos de inteligência que reconhecem quando há a falta de energia na rede da residência e começa o abastecimento utilizando a energia das baterias. </p></li>
  <li><p style="text-align: justify;">Microinversor: é um inversor projetado para operar com um número reduzido de painéis, ele é um dispositivo mais caro, porém possui uma eficiência de geração maior que os demais. </p></li>

</ul>

#### Baterias

<p style="text-indent: 50px; text-align: justify;">
As baterias são dispositivos que conseguem produzir e armazenar uma certa quantidade de energia por meio dos processos de oxidação e redução. São 3 tipos principais de baterias, são elas:
</p>

- <div style="text-align: justify;">
Baterias ou acumuladores de chumbo: As baterias de chumbo-ácido são assim denominadas pois possuem o chumbo como material ativo e a solução aquosa de ácido sulfúrico como eletrólito.
</div>

- <div style="text-align: justify;">
Bateria de Níquel-Cádmio: Essa bateria apresenta dois eletrodos imersos em uma solução aquosa de KOH (hidróxido de potássio). Um eletrodo é constituído de um ânodo (oxida) de cádmio (Cd) e o outro é um cátodo (reduz) composto por óxido de Níquel (NIO2). É muito utilizada em aparelhos domésticos e tem como principal vantagem uma maior resistência contra sobrecargas.
</div>

- <div style="text-align: justify;">
Bateria de íons Lítio: é um tipo de bateria bateria recarregável que emprega compostos de lítio como um dos seus eletrodos. Sua maior vantagem reside na capacidade de recarga, que é acompanhada por um tempo de recarga menor, e também possui uma densidade de energia superior.
</div>

#### Controlador de Carga

<p style="text-indent: 50px; text-align: justify;">
São dispositivos utilizados na maioria dos sistemas fotovoltaicos com a finalidade de realizar a máxima transferência de energia para a bateria e protegê-la de cargas e descargas excessivas, aumentando assim, a vida útil da mesma. <a href="#ref-4">[4]</a>.
</p>

<p style="text-indent: 50px; text-align: justify;">
Nos circuitos de um controlador de carga, é obtida a tensão das baterias, determinado seu estado atual de carregamento além de controlar a intensidade de corrente. Em períodos de baixa insolação, os controladores de carga possuem a função de isolar o painel do resto do sistema para evitar que as baterias descarreguem, já que a potência dos painéis solares nesses períodos é quase nula <a href="#ref-5">[5]</a>. Já em períodos de insolação excessiva, o controlador de carga evita o sobrecarregamento das baterias, devido a geração de tensão maior que a tensão de carga das mesmas <a href="#ref-6">[6]</a>.
</p>

<p style="text-indent: 50px; text-align: justify;">
Segundo o Manual de Engenharia Fotovoltaica <a href="#ref-4">[4]</a>, os controladores de carga distinguem-se quanto à grandeza utilizada para o controle, forma de desconexão do painel fotovoltaico e estratégia de controle adotada, são eles:
</p>

- <div style="text-align: justify;">
Shunt (Paralelo): O controlador de carga em paralelo consome menos energia e por isso é mais comumente utilizado. Faz uso de um dispositivo de estado sólido ou um relé eletromecânico que desliga ou reduz a intensidade da corrente para a bateria assim que ela completa a carga <a href="#ref-5">[5]</a>.
</div>

- <div style="text-align: justify;">Série: Os controladores de carga em série podem usar um relé eletromecânico ou dispositivo semicondutor de chaveamento para desconectar o gerador fotovoltaico quando a bateria está totalmente carregada. <a href="#ref-5">[5]</a>
</div>

### Dimensionamento do sistema autônomo do fogão

#### Determinação do consumo de carga

<p style="text-indent: 50px; text-align: justify;">A carga de um fogão de indução tradicional é de cerca de 2000W. O sistema pensado tem como meta o oferecimento de uma potência de 1000W pensando em fatores como: disponibilidade de materiais e redução de custos. Apesar da redução da potência, o fogão conseguirá realizar as funções propostas com base nos experimentos realizados com um fogão de indução tradicional, que conseguiu ferver uma quantidade determinada de água a uma potência de 1000W normalmente, o que diferiu de uma potência maior foi apenas o tempo que levou para a água ferver. </p>

#### Determinação da radiação solar da região

<p style="text-indent: 50px; text-align: justify;">Com base nas informações disponibilizadas pelo INPE, foi possível a partir de um tratamento dos dados, estimar o recurso solar da cidade de Brasília no ano de 2015. Os resultados podem ser visualizados na figura 2. </p>

<div align="center">
  <img alt="Figura 02: Tabela de Irradiação (Própria, 2024) " src="../../assets/images/Tabela_de _Irradiacao.png">
  <h4>Figura 02: Tabela de Irradiação (Própria, 2024)</h4>
</div>

<p style="text-indent: 50px; text-align: justify;">A irradiação global horizontal é a taxa de energia total por unidade de área incidente numa superfície horizontal. A irradiância global é dada pela soma das irradiâncias direta e difusa. No caso em específico o valor médio da radiação global diária de Brasília foi realizado e foi encontrado: Média: 5,42 kWh/m² . dia.</p>

#### Escolha do inversor

<p style="text-indent: 50px; text-align: justify;">Para a estimação deste equipamento precisamos considerar as seguintes considerações: </p>

<ul>
      <li>Tensão de entrada do inversor: 12Vcc;</li>
      <li>Tensão de saída do inversor: 220Vca;</li>
      <li>Inversor de onda senoidal pura;</li>
</ul>

<p style="text-indent: 50px; text-align: justify;">Com base nas considerações, precisaremos de um inversor com no mínimo 1000W. Sendo assim foi escolhido para o projeto o inversor Inversor De Tensão Xantrex Prosine 1000w 24v 127v Conversor De Tensão Náutico, ele consiste em um inversor de peso moderado, com cerca de 6,5Kg, possui onda senoidal pura, uma tensão de entrada de 24V e tensão de saída de 127V com uma frequência de 60Hz e ele é um inversor de 100/2000W, que alimenta facilmente TV’s, pequenos aparelhos eletrônicos e com certeza alimentará facilmente o fogão que será construído.</p>

#### Determinação do número de módulos

<p style="text-indent: 50px; text-align: justify;">Visando encontrar a tecnologia que melhor atenda ao sistema proposto, para o cálculo do número de módulos é preciso as seguintes informações. </p>

Tabela 01 - Dados do sistema fotovoltaico.

| Descrição                          | Valor | Unidade    |
| ---------------------------------- | ----- | ---------- |
| Carga Instalada                    | 1000  | W          |
| Consumo diário de Energia          | 1000  | Wh/dia     |
| Menor Radiação Diária Mensal       | 5,42  | kWh/m².dia |
| Eficiência do banco de Baterias    | 95    | %          |
| Eficiência do Controlador de Carga | 95    | %          |

<p style="text-indent: 50px; text-align: justify;">Além dos dados propostos também são necessários informações sobre o módulo fotovoltaico que pretende-se usar. Sendo assim, pretendendo atrelar a geração de energia adequada, a disponibilidade de materiais encontrados na Universidade, redução de custos e a integração com o modelo de estrutura, foi escolhido a utilização do Módulo Solar Fotovoltaico Kyocera KC45. As especificações elétrica e mecânicas do painel podem ser observados na tabela 02 e 03.</p>

Tabela 02 - Especificações elétrica do módulo Kyocera KC45 para as condições de irradiância 1000W/m², temperatura de célula 25 ºC e espectro AM 1,5 (Dados do Fabricante).

| Especificações Nominais do Módulo Kyocera KC45 |        |
| ---------------------------------------------- | ------ |
| Número de Células                              | 32     |
| Potência do Painel                             | 45 Wp  |
| Tensão de Máxima Potência (Vmp)                | 15,0 V |
| Corrente de Máxima Potência (Imp)              | 3,0 A  |
| Tensão em Aberto (Voc)                         | 19,2 V |
| Corrente de Curto Circuíto (Ioc)               | 3,10 A |
| Tensão Máxima do sistema                       | 600 V  |
| Eficiência do módulo                           | 14 %   |

Tabela 03 - Especificações mecânicas do módulo Kyocera KC45
(Dados do Fabricante)

| Especificações Mecânicas do Módulo Kyocera KC45 |                            |
| ----------------------------------------------- | -------------------------- |
| Peso do módulo                                  | 4,5 Kg                     |
| Dimensões do painel                             | 652 x 573 x 54 mm          |
| Tipo de célula                                  | Silício Policristalino     |
| MAterial da Moldura                             | Liga de alumínio Anodizado |

<p style="text-indent: 50px; text-align: justify;">Com os dados gerais apresentados é possível realizar o cálculo do número de painéis que será necessário. Primeiramente devemos saber a potência mínima do gerador, que pode ser dao pela seguinte equação:</p>

<script src='https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-MML-AM_CHTML' async></script>

$$P_{min}= \frac{Ct}{HSP \times FS}$$

Onde:

<ul>
      <li>Pmin: Potência mínima do gerador (Wp);</li>
      <li>Ct: Energia que a carga solicita (Wh);</li>
      <li>HSP: Irradiação solar média diária (kWh/m² * dia);</li>
      <li>FS : Rendimento dos componentes;</li>
</ul>

Dessa fora teremos que:

$$P_{min}= \frac{1000}{5,42 \times 0,95}$$

$$P_{min}= {194 Wp}$$

<p style="text-indent: 50px; text-align: justify;">Sendo assim o podemos calcular a quantidade de módulos necessário (N) para realizar a alimentação dos equipamentos através da Equação:</p>

$$N=\frac{P_{min}}{P_{modulo}}$$

$$N=4,31$$

<p style="text-indent: 50px; text-align: justify;">Portanto, a partir da equação serão necessários 5 painéis para realizar a alimentação do sistema, logo, diante da disponibilidade de materiais e também pela pouca diferença entre a potencia de 4 módulos e 5, foi decidido a utilização de 4 painéis fotovoltaicos de 45W.</p>

#### Dimensionamento do banco de baterias

<p style="text-indent: 50px; text-align: justify;">Um sistema off-grid é fortemente conhecido pela utilização de baterias como equipamento principal. Como já foi mencionado, as pesquisas apontam que o armazenador de energia mais adequado para sistemas fotovoltaicos off-grid é a bateria ácido de chumbo, dispositivo que funciona a partir da eletroquímica.</p>

<p style="text-indent: 50px; text-align: justify;">Sendo assim, para os cálculos de dimensionamento da bateria é necessário saber o consumo de energia do sistema, o número de dias ou horas que deseja-se que a bateria opere de forma autônoma, a profundidade de descarga da bateria, o rendimento da bateria e o nível de tensão do sistema.</p>

<p style="text-indent: 50px; text-align: justify;">A partir do estudo de caso e da problemática do problema descrito, nós conseguimos chegar a uma conclusão a respeito do número de dias de autonomia da bateria, que é um fator que leva em conta o local que o Fogão ficará, em específico se o lugar recebe pouca ou nenhuma radiação solar. Além disso é importante considerar a possibilidade de eventos climáticos adversos, como chuva e dias nublados, que poderiam reduzir ainda mais a disponibilidade de energia solar. Com o objetivo de assegurar a robustez e confiabilidade do projeto, é imprescindível estabelecer uma autonomia suficiente para a bateria. Nesse sentido, concluímos que uma autonomia mínima de 4 horas é necessária para garantir o funcionamento contínuo do fogão, proporcionando uma margem de segurança adequada para as condições operacionais previstas.</p>

<p style="text-indent: 50px; text-align: justify;">A profundidade de descarga é um fator determinante da vida útil de uma bateria chumbo-ácido. Ela diz respeito a percentagem de carga retirada da bateria numa determinada descarga, considerando 0% quando não se descarrega nada, ou seja, quando a bateria está totalmente carregada e 100% de profundidade de descarga quando está totalmente descarregada. As pesquisas apontam que a profundidade de descarga de baterias chumbo-ácido varia entre 0,7 e 0,8, sendo que a vida útil da bateria é desproporcional a esse número, ou seja, quanto maior esse número menor será a vida útil da bateria.</p>

<p style="text-indent: 50px; text-align: justify;">O rendimento da bateria será definido como 0.9 e a tensão do sistema é 12V, uma vez que esse nível de tensão apresenta maior oferta de equipamentos no mercado. </p>

<p style="text-indent: 50px; text-align: justify;">Como temos todos os valores para realizar o dimensionamento da bateria, basta utilizar a equação a seguir, afim de saber capacidade da bateria, que pode ser dado pela seguinte equação: </p>

$$C_{bat}=\frac{E_{arm}}{V_{sist}}$$

Onde Earm pode ser dado por:

$$E_{arm}=\frac{E_{consumida}\times n_{dias} }{PD \times n_{rend}}$$

Onde:

<ul>
        <li>Earm: Energia armazenada pela bateria; </li> 
        <li>Econsumida: Energia consumida pelo sistema;</li>  
        <li>Ndias: numero de dias de autonomia que se deseja;</li> 
        <li>PD: é a profundidade de descarga da bateria;</li> 
        <li>Nrend: é o rendimento da bateria;</li> 
</ul>

Dessa forma teremos:

$$E_{arm}=\frac{1000Wh \times 0,16667 dias}{0,7 \times 0,9} =264,56Wh$$

<p style="text-indent: 50px; text-align: justify;">Com o número de Energia armazenada calculado, nós conseguimos então saber a capacidade da bateria, que será: </p>

$$C_{bat}=\frac{264,56}{12} =22Ah$$

<p style="text-indent: 50px; text-align: justify;">Logo, para o sistema proposto será necessário 22Ah de bateria para que ele funcione de forma autônoma e contínua por 4 horas, sem precisar do auxílio dos painéis fotovoltaicos. Sendo assim foi escolhido para o projeto, visando minimizar custos, a utilização da bateria ATM POWER AP 12-18 (12Vcc/18.0Ah), que é uma bateria de chumbo ácido regulada por válvula, disponível no laboratório da Universidade de Brasília. Suas características podem ser visualizadas na tabela abaixo.</p>

Tabela 04 - Características da Bateria ATM POWER AP 12-18 (Dados do Fabricante)

| Tensão Nominal    | 12V                           |
| ----------------- | ----------------------------- |
| Capacidade        | 18Ah                          |
| Composição básica | Chumbo, Ácido Sulfúrico e ABS |
| Peso              | 5,22Kg                        |
| Dimensões         | 18 x 8 x 17cm                 |

<p style="text-indent: 50px; text-align: justify;">É válido salientar que ao final da vida útil da bateria, o usuário deverá contatar o importador ou revendedor para desligamento final adequado, conforme a Resolução do Conama nº 401 de 04/11/2008. Pois esta pode oferecer riscos a saúde, por causa dos elementos químicos presentes na mesma, riscos ambientais, por oferecer perigo de poluição do solo e da água.</p>

#### Dimensionamento dos cabos CC/CA

<p style="text-indent: 50px; text-align: justify;">O cálculo do tamanho da seção transversal de cada cabo foi baseado nos valores de corrente máxima que o gerador e a bateria poderiam gerar. Como a corrente máxima do gerador solar é a sua corrente de curto circuito e cada módulo gera 3,10A, como se tem quatro módulos ligados em paralelo, haverá a soma das correntes que resultará em 12,4A como sendo o valor da máxima corrente que sairá do gerador solar.</p>

<p style="text-indent: 50px; text-align: justify;">Com base na literatura, os testes realizados em placas solares são para uma temperatura de 25 ºC e uma irradiância de 1000 W/m². No entanto, em algumas localidades, a irradiância pode superar essa marca em determinadas horas do dia. Portanto, é necessário aplicar um fator multiplicativo de 125% sobre os valores nominais da corrente do gerador fotovoltaico. Dessa forma, a corrente, que antes era de 12,4 A, aumenta para 15,5 A.</p>

<p style="text-indent: 50px; text-align: justify;">Com base nestes valores podemos consultar a norma 5410 que diz respeito a instalações elétricas de baixa tensão (Imagem 3), para conseguirmos saber a seção adequada para esta corrente.</p>

<div align="center">
  <img alt="Figura 03: Tabela 36: Capacidades de condução de corrente, em ampères, para os métodos de referência
A1, A2, B1, B2, C e D (NBR 5410, 2004) " src="../../assets/images/tabela_cabos.png">
  <h4>Figura 03: Tabela 36: Capacidades de condução de corrente, em ampères, para os métodos de referência
A1, A2, B1, B2, C e D (NBR 5410, 2004)</h4>
</div>

<p style="text-indent: 50px; text-align: justify;">Considerando o método de instalação número 11, que diz respeito a cabo unipolar ou multipolar sobre parede ou espaçado desta menos de 0,3 vez o diâmetro do cabo, achamos o método de referência C, na Tabela 33 da norma. Como já sabemos o método de referência, podemos consultar a Tabela 36 (Figura 03)que nos diz que a seção nominal para esta corrente é de 1,5mm².</p>

<p style="text-indent: 50px; text-align: justify;">Como o inversor requer uma entrada de 24V, serão necessárias duas baterias conectadas em série. Embora a capacidade combinada das baterias seja de 18Ah em vez dos 22Ah exigidos, essa diferença não afetará o bom funcionamento do fogão. Com as baterias ligadas em série, a corrente permanecerá constante em 18A. Assim, o valor da corrente de operação do conjunto é dado por: </p>

$$I_{bat}=1,25 \times \frac{C_{bat}}{V_{sist}} $$

$$I_{bat}=1,25 \times \frac{18}{12}$$

$$I_{bat}=1,875A$$

De acordo com a norma 5410, um cabo de 2,5mm 2, será o suficiente para atender o sistema.

#### Dimensionamento do controlador de carga

<p style="text-indent: 50px; text-align: justify;">Para dimensionar adequadamente o controlador de carga, é essencial ter conhecimento das correntes máximas às quais ele será exposto, tanto do lado dos painéis geradores quanto do lado das cargas. No que diz respeito às cargas, que no caso são as baterias, a corrente máxima já foi calculada na parte de dimensionamento dos cabos, que é 1,875A. Quanto aos painéis geradores, a corrente máxima, também previamente calculada no item anterior, é de 15,5 A. </p>

<p style="text-indent: 50px; text-align: justify;">Deste modo, a corrente mais alta verificada diz respeito aos painéis geradores. Sendo assim, podemos utilizar um controlador de carga de 50A, mais especificamente o controlador de carga solar fotovoltaico PWM 50A 12/24V LCD 50A. As especificações do controlador podem ser visualizadas na tabela a seguir.</p>

Tabela 05 - Características do Controlodor de Carga PWM RBL-30A LCD 30A (Dados do Fabricante).

| Corrente de carga | 50A             |
| ----------------- | --------------- |
| Tela LCD          | Sim             |
| Saída USB         | 5V/2,5A           |
| Dimensões         | 193 x 98 x 51 mm |
| Peso              | 390g            |

## Referências

<div id="ref-1"></div>

> [1] *ALVES, Marliana de Oliveira Lage. Energia solar: estudo da geração de energia elétrica através dos sistemas fotovoltaicos on-grid e off-grid. 2019. Acesso em 24/04/2024.*

<div id="ref-2"></div>

> [2] *CASTRO, Gabriel Malta. Avaliação do valor da energia proveniente de usinas heliotérmicas com armazenamento no âmbito do sistema interligado nacional. Universidade Federal do Rio de Janeiro/Instituto Alberto Luiz Coimbra de Pós-Graduação e Pesquisa de Engenharia/Programa de Planejamento Energético, 2015. Acesso em 24/04/2024.*

<div id="ref-3"></div>

> [3] *PRIEB, César Wilhelm Massen. Desenvolvimento de um sistema de ensaio de módulos fotovoltaicos. 2002. Acesso em 24/04/2024.*

<div id="ref-4"></div>

> [4] *GUIMARÃES, A. P. C. et al. Manual de engenharia para sistemas fotovoltaicos. Ediouro Gráfica e Editora SA Edição Especial, Rio de Janeiro, Brasil, 2004. Acesso em 24/04/2024.*

<div id="ref-5"></div>

> [5] *FARIAS, E. M. B. et al. ESTUDO E SIMULAÇÃO DE UM CONTROLADOR DE CARGA PARA SISTEMA FOTOVOLTAICO OFF-GRID.*

<div id="ref-6"></div>

> [6] *GASQUET, Héctor L. Sistemas Fotovoltaicos.1997. El Paso, Texas.Acesso em 24/04/2024. Acesso em 24/04/2024.*

<div id="ref-7"></div>

> [7] *GERMANOS, Ricardo Alberto Coppola et al. Inversores de Potência: Conceitos teóricos e demonstração experimental. Revista Brasileira de Ensino de Física, v. 42, p. e20200113, 2020.Acesso em 24/04/2024.*