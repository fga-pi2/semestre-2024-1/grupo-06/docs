# Perfis de Usuário do Produto

### Persona 1: O Entusiasta de Camping

- **Nome:** Alex Rivera
- **Idade:** 32 anos
- **Profissão:** Consultor Ambiental
- **Familiaridade com Tecnologia:** Moderada

**Objetivos:**

- Acesso a soluções práticas de cozinha para acampamentos ou caminhadas.
- Operar dispositivos de cozinha sem depender de eletricidade da rede.

**Desafios:**

- Frequentemente em locais remotos sem acesso confiável à energia.
- Necessidade de dispositivos duráveis e aptos para uso externo.

**Utilização do Produto:**

- **Configurações do Fogão:** Utiliza o fogão para preparar alimentos rapidamente, ajustando a potência conforme necessário.
- **Uso do PWA:** Se o celular estiver disponível, inicia o aquecimento através do aplicativo enquanto monta o acampamento, monitorando a temperatura e o tempo de cozimento à distância.
- **Uso do display:** Na ausência do celular, o display permite configurar tempo e potência de cocção para alimentos pré-cadastrados. Assim, é possível ajustar as configurações rapidamente e se afastar para continuar montando o acampamento ou realizar outras atividades.

### Persona 2: A Chef Doméstica

- **Nome:** Sarah Chen
- **Idade:** 28 anos
- **Profissão:** Desenvolvedora de Software
- **Familiaridade com Tecnologia:** Alta

**Objetivos:**

- Incorporar tecnologia inteligente na rotina doméstica.
- Experimentar técnicas de cozinha que exigem controle preciso de tempo e temperatura.

**Desafios:**

- Busca por interfaces simples e informativas.
- Procura por produtos que facilitam técnicas culinárias avançadas.

**Utilização do Produto:**

- **Experimentação Culinária:** Explora novas receitas que exigem condições específicas de cozimento, como sous-vide.
- **Interação com o PWA:** Acessa receitas e configurações pré-definidas, fazendo ajustes em tempo real e recebendo alertas sobre o preparo.

### Persona 3: A Pessoa Simples

- **Nome:** David Kim
- **Idade:** 45 anos
- **Profissão:** Professor Escolar
- **Familiaridade com Tecnologia:** Baixa a Moderada

**Objetivos:**

- Reduzir o consumo de energia.
- Manter simplicidade no ambiente de cozinha.

**Desafios:**

- Pouco interesse em automação residencial.
- Preferência por soluções simples e seguras.

**Utilização do Produto:**

- **Eficiência Energética:** Valoriza o uso de energia solar no fogão, ajudando a reduzir a pegada de carbono.
- **Funcionalidades de Segurança:** Interage com o dispositivo para selecionar temperatura e tempo para receitas simples.

### Persona 4: O Morador Urbano com Espaço Limitado

- **Nome:** Mia Thompson
- **Idade:** 24 anos
- **Profissão:** Estudante de Pós-Graduação
- **Familiaridade com Tecnologia:** Moderada

**Objetivos:**

- Encontrar aparelhos compactos e eficientes para espaços reduzidos.
- Gerenciar custos de vida com tecnologias energéticamente eficientes.

**Desafios:**

- Espaço de cozinha limitado.
- Necessidade de preparo rápido e fácil de refeições devido a uma agenda cheia.

**Utilização do Produto:**

- **Maximização do Espaço:** Opta pelo fogão de indução inteligente devido ao seu design compacto, facilitando a integração em espaços pequenos.
- **Utilização do PWA:** Programa o tempo de cozimento através do aplicativo para otimizar o preparo de alimentos.