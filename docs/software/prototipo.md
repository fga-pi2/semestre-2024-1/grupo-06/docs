# Protótipos

## Protótipo de baixa fidelidade

### Introdução
<p style="text-indent: 50px; text-align: justify;">Para a realização dos protótipos de baixa fidelidade foi utilizado o software <a href="https://balsamiq.com/">balsamiq</a> por ter os traços de baixa fidelidade. Tal protótipo tem como objetivo analisar apenas se as funcionalidades estão de acordo com os requisitos que foram elicitados, não tendo total fidelidade ao design que será aplicado.
</p>

### Protótipo de baixa fidelidade
<div align="center">

  <img title="Protótipo baixa fidelidade" alt="Portótipo baixa fidelidade" src="../../assets/images/prototipo-baixa.jpeg" width="75%">
  <h4> Figura 01: Protótipo baixa fidelidade. Fonte: Autoria Própria, 2024</h4>
</div>

## Protótipo de Alta Fidelidade

### Paleta de cores

</br>
<div style="text-align:center;">
  <img src="../../assets/images/paletaCores.png">
  <div style="text-align:center;">Figura 02: Paleta de Cores. Fonte: Autoria Própria, 2024</div>
</div>
</br>

### Tipografia

<p style="text-indent: 50px;text-align: justify;">
Escolhemos a fonte <strong>Poppins</strong> por seu design moderno e geométrico, que oferece excelente legibilidade e um toque contemporâneo, alinhando-se perfeitamente com a estética limpa e profissional desejada para o sistema.
</p>

### Protótipo de representação

#### Telas Inicial, Tela Ligar, Tela principal

</br>
<div style="text-align:center;">
  <img src="../../assets/images/prototipo-alta2.jpeg">
  <div style="text-align:center;">Figura 03: Páginas inicial, ligar e principal. Fonte: Autoria Própria, 2024</div>
</div>
</br>

#### Telas rotina e histórico de cozimento

</br>
<div style="text-align:center;">
  <img src="../../assets/images/prototipo-alta1.jpeg">
  <div style="text-align:center;">Figura 04: Páginas de rotina. Fonte: Autoria Própria, 2024</div>
</div>
</br>

## Referências

<div id="ref-1"></div>

>[1] *Prototipação. Disponível em [http://web.mit.edu/6.813/www/sp17/classes/08-prototyping/](http://web.mit.edu/6.813/www/sp17/classes/08-prototyping/). Acesso em 23 de abril de 2024.*