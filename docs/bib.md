# **Referências Bibliográficas**

<!-- *ABNT. ABNT NBR NM 60335-1:2010 Segurança de aparelhos eletrodomésticos e similares Parte 1: Requisitos gerais (IEC 60335-1:2006 - edição 4.2, MOD). 2010. Disponível em: <https://www.abntcatalogo.com.br/pnm.aspx?Q=ZGdGTDJTZEhRNUZ1Tmd1a3drdW9oaDk0NkU4MTdBcnQwNWxNQWRKRXlEZz0=>. Acesso em: 11 de abril de 2024. Citado na página [**Visão Geral**](/visaogeral/#levantamento-de-normas-tecnicas-relacionadas-ao-problema).*

*ABNT. IEC 62133 2:2017/AMD1:2021/COR1:2021 Corrigendum 1 - Amendment 1 - Secondary cells and batteries containing alkaline or other non-acid electrolytes - Safety requirements  or portable sealed secondary cells, and for batteries made from them, for use in portable applications - Part 2: Lithium systems. 2021. Disponível em: <https://www.abntcatalogo.com.br/pnm.aspx?Q=azJzWHJtZUREQjB0VEJkeVMwdmR5MWhBZWZsRjJRQm9mWXk1TUV1eHBUND0=>. Acesso em: 11 de abril de 2024. Citado na página [**Visão Geral**](/visaogeral/#levantamento-de-normas-tecnicas-relacionadas-ao-problema).*

*ABNT. ISO/IEC/IEEE 12207-2:2020 Engenharia de sistemas e software — Processos de ciclo de vida de software — Parte 2: Relação e mapeamento entre ISO/IEC/IEEE 12207:2017 e ISO/IEC 12207:2008. 2020. Disponível em: <https://www.abntcatalogo.com.br/pnm.aspx?Q=U29aeUVCQVg3UUUwU1JJb0hod2hMU0QzTWxxTG92eDJ1N1Y5MmJGRmJhUT0=>. Acesso em: 11 de abril de 2024.Citado na página [**Visão Geral**](/visaogeral/#levantamento-de-normas-tecnicas-relacionadas-ao-problema).*

*ABNT. ABNT NBR IEC 60335-2-42:2013 Aparelhos eletrodomésticos e similares — Segurança Parte 2-42: Requisitos particulares para fornos elétricos à convecção forçada, cozedores a vapor e fornos combinados. 2013. Disponível em: <https://www.abntcatalogo.com.br/pnm.aspx?Q=YlZuSWhOUHJocWpDZU9tYlZlTm1OdEdqaVhoekdzMHorTXhzS3hqV0hMND0=>. Acesso em: 11 de abril de 2024.Citado na página [**Visão Geral**](/visaogeral/#levantamento-de-normas-tecnicas-relacionadas-ao-problema).*

*UML Component Diagrams. Disponível em: <https://www.uml-diagrams.org/component-diagrams.html>. Acesso em: 22 de abril de 2024.Citado na página [**Apendice 04**](/apendices/apendice4/#diagrama-de-componentes).* -->

<!-- *IIBA. Um guia para o Corpo de Conhecimento de Análise de Negócios™ (Guia BABOK®) Versão 2.0. Toronto, Canadá: International Institute of Business Analysis, 2011.* -->

<!-- *Prototipação. Disponível em [http://web.mit.edu/6.813/www/sp17/classes/08-prototyping/](http://web.mit.edu/6.813/www/sp17/classes/08-prototyping/). Acesso em 23 de abril de 2024.* -->

<!-- *SOMMERVILLE, I. Engenharia de software. 9.ed. São Paulo: Pearson Education do Brasil Ltda, 2013* -->

<!-- *SUHARJITO; SUNARDI, Andri. MVC Architecture: A Comparative Study Between Laravel Framework and Slim Framework in Freelancer Project Monitoring System Web Based. In: Conferência Internacional de Ciência da Computação e INteligência Computacional, 4. 2019, Yogyakarta. Disponível em: https://doi.org/10.1016/j.procs.2019.08.150. Acesso em: 24 abr. 2024* -->

<!-- *What are Progressive Web Apps?. Web.Dev. Disponível em: <https://web.dev/articles/what-are-pwas>. Acesso em: 24 abr. 2024* -->

<!-- *Site dos Eletrodomésticos. Melhores Cooktops de Indução 1 boca em 2024. Disponível em: <https://sitedoseletrodomesticos.com.br/melhor-cooktop-de-inducao-1-boca>. Acesso em 17/04/2024.* -->

<!-- *ALVES, Marliana de Oliveira Lage. Energia solar: estudo da geração de energia elétrica através dos sistemas fotovoltaicos on-grid e off-grid. 2019. Acesso em 24/04/2024.[**Arquitetura de Energia**](/arquitetura-energia/#sistema-autonomo-de-energia).* -->

<!-- *CASTRO, Gabriel Malta. Avaliação do valor da energia proveniente de usinas heliotérmicas com armazenamento no âmbito do sistema interligado nacional. Universidade Federal do Rio de Janeiro/Instituto Alberto Luiz Coimbra de Pós-Graduação e Pesquisa de Engenharia/Programa de Planejamento Energético, 2015. Acesso em 24/04/2024.* -->


<!-- *PRIEB, César Wilhelm Massen. Desenvolvimento de um sistema de ensaio de módulos fotovoltaicos. 2002. Acesso em 24/04/2024.* -->

*GERMANOS, Ricardo Alberto Coppola et al. Inversores de Potência: Conceitos teóricos e demonstração experimental. Revista Brasileira de Ensino de Física, v. 42, p. e20200113, 2020.Acesso em 24/04/2024.*

*COPETTI, Jacqueline Biancon; MACAGNAN, Mario Henrique. Baterias em sistemas solares fotovoltaicos. In: Anais Congresso Brasileiro de Energia Solar-CBENS. 2007. Acesso em 24/04/2024.*

<!-- *GUIMARÃES, A. P. C. et al. Manual de engenharia para sistemas fotovoltaicos. Ediouro Gráfica e Editora SA Edição Especial, Rio de Janeiro, Brasil, 2004. Acesso em 24/04/2024.*

*GASQUET, Héctor L. Sistemas Fotovoltaicos.1997. El Paso, Texas.Acesso em 24/04/2024. Acesso em 24/04/2024.*

*FARIAS, E. M. B. et al. ESTUDO E SIMULAÇÃO DE UM CONTROLADOR DE CARGA PARA SISTEMA FOTOVOLTAICO OFF-GRID.* -->

<!-- *Çengel, Y. A., "Fundamentos da Transferência de Calor e Massa", 5ª edição, McGraw-Hill, 2015. Citado na página [**arquitetura-estrutura**](/arquitetura-estrutura/####Analise-Térmica).*

*Paul, C. R., "Eletromagnetismo para Engenheiros", 1° edição, John Wiley & Sons, 2017.Capitulo 05.Citado na página [**arquitetura-estrutura**](/arquitetura-estrutura/####Analise-eletromagnética).* -->

<!-- *USER STORY MAPPING: TRANSFORME O BACKLOG DE PRODUTO EM MAPA. Disponível em: <https://blog.somostera.com/product-management/user-story-mapping>. Acesso em: 27 abr. 2024* -->

<!-- *Brastemp, Disponível em: <https://www.brastemp.com.br/experience/tech/cooktop-por-inducao/>. Acesso em: 13 abr. 2024.Citado na página [**arquitetura-estrutura**](/arquitetura-estrutura/####Analise-Térmica).*

*IBAB, Disponível em: <https://ibabrubber.com.br/compostos.pdf>. Acesso em: 04 mar. 2024. Citado na página [**arquitetura-estrutura**](/arquitetura-estrutura/####Analise-Térmica).*


*Mourão, A. A. C.,Vivaldini, D. O., Pandolfelli, V. C. "Fundamentals and analysis of high emissivity refractory coatings",  Universidade Federal de São Carlos, 2017.Citado na página [**arquitetura-estrutura**](/arquitetura-estrutura/####Analise-eletromagnética).* -->
