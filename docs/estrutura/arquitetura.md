# **Arquitetura de Estrutura**


### Ambiente operacional

<p style="text-indent: 50px; text-align: justify;">
O levantamento do ambiente operacional é uma prática recomendada que visa identificar as condições às quais a estrutura estará sujeita em seu cotidiano. Isso é feito com o objetivo de reduzir a necessidade de retrabalho durante o desenvolvimento do projeto, evitando a omissão de variáveis relevantes.
</p>

<p style="text-indent: 50px; text-align: justify;"> 
Ao lidar com um fogão portátil, é necessário considerar dois ambientes operacionais distintos: o ambiente fechado e o ambiente aberto. No ambiente aberto, todas as restrições do ambiente fechado são mantidas, com a adição das restrições específicas do ambiente aberto. Devido à impossibilidade de controlar todas as variáveis naturais no ambiente aberto, questões como custo e tempo podem impedir a abordagem de certas restrições. Essas limitações serão então documentadas no manual de instruções. Prossegue-se com o levantamento das restrições operacionais:
</p>

<ul>
  <li>Ambiente fechado:
    <ul>
      <li>Temperatura ambiente;</li>
      <li>Agentes oxidantes;</li>
      <li>Sujeito a quedas de líquidos;</li>
      <li>Limpeza constante;</li>
      <li>Esforços sobre a estrutura.</li>
    </ul>
  </li>
</ul>

<ul>
  <li>Ambiente aberto:
    <ul>
      <li>Poeira;</li>
      <li>Luz Solar;</li>
      <li>Vento;</li>
      <li>Chuva;</li>
      <li>Tolerância a impacto;</li>
      <li>Maresia;</li>
      <li>Portátil.</li>
    </ul>
  </li>
</ul>
<p style="text-indent: 50px; text-align: justify;"> 
Analisando o projeto, identificou-se a necessidade de realizar análises térmicas e eletromagnéticas para orientar a seleção dos materiais a serem utilizados. Essas análises são essenciais devido à natureza do projeto e visam garantir o desempenho adequado do fogão portátil, tanto em termos de dissipação de calor quanto de interferências eletromagnéticas e eficiência energética.
</p>

### Representação dos sistemas a serem analisados

#### Análise Térmica - Estrutural - Esquematico geral
<p style="text-indent: 50px; text-align: justify;"> 
Ao iniciar a análise térmica - estrutural, é necessário elaborar um esquemático <a href="#fig-01">Fig. 1</a> que represente o sistema em questão. Este esquemático deve incluir todos os componentes relevantes do fogão portátil, como bobina, superfícies de contato com panelas, isolantes térmicos, e quaisquer outros elementos que possam influenciar na distribuição e dissipação de calor assim como nas forças sobre a estrutura. O esquemático servirá como base para a modelagem térmica-estrutural e permitirá uma compreensão detalhada do comportamento do sistema como observado pelas <a href="#fig-02">Fig. 02</a>, <a href="#fig-03">03</a>, <a href="#fig-04">04</a> e <a href="#fig-05">05</a>.
</p>

<div id="fig-01" align="center">
  <img alt="Figura 01: Esquematico da analise térmica" src="../../assets/images/esquematico.jpg">
  <h4>Figura 01: Esquematico da analise térmica</h4>
</div>


#### Análise Estrutural 

<p style="text-indent: 50px; text-align: justify;">
Para análise estrutural, foi verificada a carga aplicada no vidro refratário, na área reservada para a panela, observou-se como  a carga afetaria o restante da estrutura, quanto a deformação ou carga excessiva no vidro, para isso foi utilizado uma carga exagerada de aproximadamente 30kg. As  Fig. <a href="#fig-02">02</a>, Fig. <a href="#fig-03">03</a> e  Fig. <a href="#fig-04">04</a> ilustram as condições de contorno e propriedades utilizadas na análise:
</p>
<ul>
<li>
Propriedades do vidro para análise:
<div id="fig-02" align="center">
  <img src="../../assets/images/Dados utilizados para o vidro na simulação.png">
  <h4>Figura 02: Propriedades do vidro de acordo com a própria biblioteca do Ansys Workbench 2024</h4>
</div>
</li>

<li>
Propriedades do ABS para análise:
<div id="fig-03" align="center">
  <img src="../../assets/images/Dados utilizados para ABS simulação.png">
  <h4>Figura 03: Propriedades do ABS de acordo com a própria biblioteca do Ansys Workbench 2024</h4>
</div>
</li>
</ul>

<p style="text-indent: 50px; text-align: justify;">
Como condição de contorno, a base do fogão foi fixada e a força foi aplicada direcionalmente na área a qual a panela será localizada:
</p>

<div id="fig-04" align="center">
  <img src="../../assets/images/Força aplicada como carga da panela.png">
  <h4>Figura 04: Condições de contorno da simulação</h4>
</div>



#### Análise Térmica - Temperaturas Panela, Água, Vidro


<div id="fig-05" align="center">
  <img src="../../assets/images/diagrama_codigo1.png">
  <h4>Figura 05: Diagrama de transferência de calor panela, liquido, vidro código 01 </h4>
</div>

As condições de contorno encontram-se nas <a href="#fig-06">Fig. 06</a>.

<div id="fig-06" align="center">
  <img src="../../assets/images/5-step interactive flowchart.png">
  <h4>Figura 06: Fluxo de calor: Vidro, Neoprene, Interior do fogão código 02 <a href="#ref-1">[1]</a></h4>
</div>

**Equações utilizadas no código 01 <a href="#ref-1">[1]</a>:**



- **Transferência de calor pela base da panela por condução:**
   ![Q_{\text{cond}} = k \cdot A \cdot \Delta T](https://latex.codecogs.com/svg.latex?Q_%7B%5Ctext%7Bcond%7D%7D%20%3D%20k%20%5Ccdot%20A%20%5Ccdot%20%5CDelta%20T)

- **Transferência de calor pelas paredes laterais da panela por convecção:**
   ![Q_{\text{conv}} = h \cdot A \cdot \Delta T](https://latex.codecogs.com/svg.latex?Q_%7B%5Ctext%7Bconv%7D%7D%20%3D%20h%20%5Ccdot%20A%20%5Ccdot%20%5CDelta%20T)

- **Transferência de calor da base para o líquido por convecção:**
   ![Q_{\text{conv}} = h \cdot A \cdot \Delta T](https://latex.codecogs.com/svg.latex?Q_%7B%5Ctext%7Bconv%7D%7D%20%3D%20h%20%5Ccdot%20A%20%5Ccdot%20%5CDelta%20T)

- **Transferência de calor do líquido para a panela por convecção:**
   ![Q_{\text{conv}} = h \cdot A \cdot \Delta T](https://latex.codecogs.com/svg.latex?Q_%7B%5Ctext%7Bconv%7D%7D%20%3D%20h%20%5Ccdot%20A%20%5Ccdot%20%5CDelta%20T)

- **Transferência de calor da panela para o vidro por condução:**
   ![Q_{\text{cond}} = k \cdot A \cdot \Delta T](https://latex.codecogs.com/svg.latex?Q_%7B%5Ctext%7Bcond%7D%7D%20%3D%20k%20%5Ccdot%20A%20%5Ccdot%20%5CDelta%20T)

- **Transferência de calor do vidro para o ambiente por convecção:**
   ![Q_{\text{conv}} = h \cdot A \cdot \Delta T](https://latex.codecogs.com/svg.latex?Q_%7B%5Ctext%7Bconv%7D%7D%20%3D%20h%20%5Ccdot%20A%20%5Ccdot%20%5CDelta%20T)

- **Radiação térmica emitida pela panela:**
   ![Q_{\text{rad}} = \sigma \cdot A \cdot (T^4 - T_{\text{amb}}^4)](https://latex.codecogs.com/svg.latex?Q_%7B%5Ctext%7Brad%7D%7D%20%3D%20%5Csigma%20%5Ccdot%20A%20%5Ccdot%20%28T%5E4%20-%20T_%7B%5Ctext%7Bamb%7D%7D%5E4%29)

- **Taxa de resfriamento total:**
   ![Q_{\text{total}} = Q_{\text{conv}} + Q_{\text{cond}} + Q_{\text{rad}}](https://latex.codecogs.com/svg.latex?Q_%7B%5Ctext%7Btotal%7D%7D%20%3D%20Q_%7B%5Ctext%7Bconv%7D%7D%20%2B%20Q_%7B%5Ctext%7Bcond%7D%7D%20%2B%20Q_%7B%5Ctext%7Brad%7D%7D)

- **Variação de temperatura usando o método de Euler:**
   ![ \Delta T = \left( \frac{Q_{\text{aquecimento}} - Q_{\text{total}}}{m \cdot c} \right) \cdot \Delta t](https://latex.codecogs.com/svg.latex?%5CDelta%20T%20%3D%20%5Cleft%28%20%5Cfrac%7BQ_%7B%5Ctext%7Baquecimento%7D%7D%20-%20Q_%7B%5Ctext%7Btotal%7D%7D%7D%7Bm%20%5Ccdot%20c%7D%20%5Cright%29%20%5Ccdot%20%5CDelta%20t)


#### Análise Térmica - Fluxo de calor: Vidro, Neoprene, Interior do fogão.


<div id="fig-07" align="center">
  <img src="../../assets/images/diagrama_codigo2.png">
  <h4>Figura 07: Fluxo de calor: Vidro, Neoprene, Interior do fogão código 02</h4>
</div>

Pode-se verificar o fluxo de calor na <a href="#fig-08">Fig. 08</a>.
<div id="fig-08" align="center">
  <img src="../../assets/images/5-step interactive flowchart2.png">
  <h4>Figura 08: Fluxo de calor: Vidro, Neoprene, Interior do fogão código 02 <a href="#ref-1">[1]</a></h4>
</div>

**Equações utilizadas no código 02 <a href="#ref-1">[1]</a>:**


1. **Cálculo do calor por condução térmica**:

   ![Q = k \times A \times \frac{\Delta T}{d}](https://latex.codecogs.com/svg.latex?Q%20%3D%20k%20%5Ctimes%20A%20%5Ctimes%20%5Cfrac%7B%5CDelta%20T%7D%7Bd%7D)

2. **Cálculo da temperatura**:

   ![T = T_{\text{inicial}} - \frac{Q \times d}{k \times A}](https://latex.codecogs.com/svg.latex?T%20%3D%20T_%7B%5Ctext%7Binicial%7D%7D%20-%20%5Cfrac%7BQ%20%5Ctimes%20d%7D%7Bk%20%5Ctimes%20A%7D)

3. **Conversão de temperatura para Celsius**:

   ![T_{\text{Celsius}} = T - 273.15](https://latex.codecogs.com/svg.latex?T_%7B%5Ctext%7BCelsius%7D%7D%20%3D%20T%20-%20273.15)

4. **Adição de calor à temperatura**:

   ![T_{\text{final}} = T_{\text{inicial}} + Q](https://latex.codecogs.com/svg.latex?T_%7B%5Ctext%7Bfinal%7D%7D%20%3D%20T_%7B%5Ctext%7Binicial%7D%7D%20%2B%20Q)

5. **Porcentagem da temperatura**:

   ![ \text{porcentagem} = \left( \frac{T_{\text{final}}-273.15}{T_{\text{inicial}}-273.15} \right) \times 100](https://latex.codecogs.com/svg.latex?%5Ctext%7Bporcentagem%7D%20%3D%20%5Cleft%28%20%5Cfrac%7BT_%7B%5Ctext%7Bfinal%7D%7D-273.15%7D%7BT_%7B%5Ctext%7Binicial%7D%7D-273.15%7D%20%5Cright%29%20%5Ctimes%20100)

6. **Redução da temperatura em porcentagem**:

   ![ \text{porcentagem\_reduzida} = 100 - \left( \frac{T_{\text{final}}-273.15}{T_{\text{inicial}}-273.15} \right) \times 100](https://latex.codecogs.com/svg.latex?%5Ctext%7Bporcentagem%5C_reduzida%7D%20%3D%20100%20-%20%5Cleft%28%20%5Cfrac%7BT_%7B%5Ctext%7Bfinal%7D%7D-273.15%7D%7BT_%7B%5Ctext%7Binicial%7D%7D-273.15%7D%20%5Cright%29%20%5Ctimes%20100)

## Resultados

### Análises Estruturais

<p style="text-indent: 50px; text-align: justify;"> 
Tendo em vista as propriedades dos materiais citados para análise, o vidro e o ABS, obtiveram um resultado favorável com uma deformação muito pequena, mesmo que para uma carga exageradamente aplicada para nossas condições de contorno. Por limitação de fabricação do vidro temperado, sua espessura mínima é de 6mm, levando em conta o método de modelagem de corte para após isso temperar o vidro, um processo necessario para adequar o vidro temperado para o projeto. Toda a análise feita considerou-se dados teóricos, por se tratar de um material óxido metálico transparente, de elevada dureza, essencialmente inerte e biologicamente inativo. Resultados numéricos das simulações estruturais, são ilustrados nas figuras <a href="#fig-09">Fig. 09</a>, <a href="#fig-10">Fig. 10</a>, <a href="#fig-11">Fig. 11</a>:
</p>

<ul>
<li>
Resultado da deformação em mm para estrutura:
<div id="fig-09" align="center">
  <img src="../../assets/images/deformação em mm.png">
  <h4>Figura 09: Deformação em mm</h4>
</div>
</li>
<li>
Resultado da tensão equivalente de von Mises:
<div id="fig-10" align="center">
  <img src="../../assets/images/Tensão equivalente von Mises.png">
  <h4>Figura 10: Tensão equivalente de von Mises</h4>
</div>
</li>
<li>
Resultado numérico quanto ao cisalhamento:
<div id="fig-11" align="center">
  <img src="../../assets/images/cisalhamento.png">
  <h4>Figura 11: Cisalhamento</h4>
</div>
</li>
</ul>

<p style="text-indent: 50px; text-align: justify;"> 
 Todos os resultados ficaram dentro do esperado, validando a aplicação estrutural, sem comprometer a integridade estrutural.
 </p>

### Análise Térmica

<p style="text-indent: 50px; text-align: justify;"> 
Durante o uso da estrutura do fogão portátil, várias formas de transferência de calor estão presentes. Algumas das transferências de calor relevantes presentes na análise pode sem definidas conceitualmente como:
</p>

<ul>
  <li> Condução: Transferência de calor através de um material sólido ou entre materiais em contato direto, como a condução de calor da base da base da panela para a superfície de apoio das panelas.</li>
  <li> Convecção: Transferência de calor através de um fluido em movimento, como o ar ao redor das panelas aquecidas que aquecem as panelas por convecção.</li>
  <li> Radiação / Irradiação: Transferência de calor por meio de ondas eletromagnéticas, como a radiação infravermelha emitida pelas bobinas e absorvida pelas panelas e irradiação da panela para o ambiente.</li>
</ul>


<p style="text-indent: 50px; text-align: justify;">
Considerar essas formas de transferência de calor é essencial para uma análise térmica precisa do fogão portátil e para garantir seu desempenho eficiente e seguro <a href="#ref-1">[1]</a>.
</p>

<p style="text-indent: 50px; text-align: justify;">
Os resultados das análises térmicas realizadas utilizou-se a potência de 1500W para alimentar o fogão portátil pois a potência do fogão é 1000W e utilizamos um coeficiente de segurança de 1,5, foram obtidos através de códigos em MATLAB que consideraram as formas de transferência de calor por convecção, condução e radiação / irradiação. Além disso, foi levada em conta a eficiência térmica dos fogões por indução, estabelecida em 84% <a href="#ref-3">[3]</a>. Essas análises foram conduzidas de forma transiente para avaliar as variações de temperatura ao longo do tempo.
</p>

<p style="text-indent: 50px; text-align: justify;">
Os gráficos apresentam as variações de temperatura do líquido, da panela e do vidro em função do tempo, fornecendo percepções sobre o comportamento térmico do sistema em resposta às diferentes condições de potência, geometria da panela e quantidade de liquido presente na panela. Todos os cálculos e dados foram baseados no livro de referência de <a href="#ref-1">[1]</a>.
</p>

<p style="text-indent: 50px; text-align: justify;"> Os códigos adicionais desenvolvidos visaram calcular o fluxo de calor do vidro para o neoprene por condução e, em seguida, do neoprene para o interior do fogão, levando em conta as propriedades térmicas e geométricas de ambos os materiais. Nestes cálculos, as temperaturas foram analisadas em condições extremas, sem considerar variações transientes, para representar os casos onde a temperatura chegou ao seu pico.
</p>

<p style="text-indent: 50px; text-align: justify;"> 
Além disso, a eficiência do isolante térmico foi calculada, permitindo avaliar a redução da temperatura do vidro em direção ao interior do fogão, expressa como uma porcentagem. Os coeficientes de transferência de calor convectivo e por condução (h e k, respectivamente)  e emissividade para a radiação. Foram calibrados com base em valores disponíveis no livro <a href="#ref-1">[1]</a> para situações similares.
</p>

<p style="text-indent: 50px; text-align: justify;"> 
Os gráficos apresentados a seguir ilustram os resultados obtidos, fornecendo percepções sobre a eficácia do isolamento térmico e os fluxos de calor ao longo dos materiais considerados. Para mais informações e detalhes sobre os códigos encontram-se disponíveis em anexo.
</p>


#### Casos de utilização do produto:

**1° Caso:** 
<p style="text-indent: 50px; text-align: justify;"> A simulação de um uso cotidiano com a geometria específica da panela utilizada nos experimentos práticos foi realizada para verificar o desempenho do fogão em condições realistas. Os resultados obtidos foram considerados aceitáveis, <a href="#fig-06">Fig. 06</a>, como indicado na tabela abaixo. É relevante destacar que a temperatura utilizada para avaliação foi a maior temperatura alcançada pelo objeto após um período de aquecimento de uma hora.
</p>
<p style="text-indent: 50px; text-align: justify;">
Para um dimensionamento correto da espessura do vidro foi utilizado 3 mm dado que o vidro refratário que a equipe possui.
</p>


| **Geometria** | **Temperatura Máxima(°C)** |
|:-------------:|:------------------:|
|    Panela     |       99,96       |
|    Liquido    |       99,71       |
|     Vidro     |       76,83       |

<p style="text-indent: 50px; text-align: justify;"> 
A simulação de um uso cotidiano com a geometria específica da panela utilizada nos experimentos práticos foi realizada para verificar o desempenho do fogão em condições reais. Os resultados obtidos foram considerados aceitáveis, como indicado na tabela abaixo. É relevante destacar que a temperatura utilizada para avaliação foi a maior temperatura alcançada pelo objeto após um período de aquecimento de uma hora.
</p>

<p style="text-indent: 50px; text-align: justify;"> Com base nestes valores o segundo código verificou as temperaturas resultantes após aplicação do isolante térmico sob o vidro, os dados obtidos se encontram abaixo:
</p>

| **Objeto** | **Temperatura(°C)** |
|:------------:|:---------------------:|
| Vidro      | 76,83             |
| Neoprene   | 26,23              |
| Sensor     | 24,01               |
| Eficiência do isolamento térmico (%) | 68,75               |

<p style="text-indent: 50px; text-align: justify;"> A eficiência do isolante foi medida em comparação com a temperatura do vidro e a temperatura que chegou para o sensor pelo fluxo de calor:
</p>



<p style="text-indent: 50px; text-align: justify;"> 
Esses resultados demonstram que o fogão portátil, quando utilizado com a panela específica e nas condições simuladas, conseguiu manter temperaturas internas aceitáveis, demonstrando um desempenho satisfatório <a href="#fig-12">Fig. 12</a>. A eficiência do isolamento térmico, expressa como uma porcentagem, indica a capacidade do fogão em reduzir a transferência de calor do vidro e neoprene para o interior do fogão, sendo de 68,75% conforme os dados obtidos.
</p>



<div id="fig-12" align="center">
  <img src="../../assets/images/Gráfico_variaçãodetemperatura.png">
  <h4>Figura 12: Caso 01 - Variação de temperatura liquido, panela, vidro</h4>
</div>



**2° Caso**:

<p style="text-indent: 50px; text-align: justify;"> 
A simulação realizada para verificar o comportamento do fogão em uma situação extrema para verificar a viabilidade em utilizar mais de um modelo de panela, considerando uma panela com menor massa e paredes mais finas, mas mantendo o mesmo diâmetro, revelou diferenças significativas nas temperaturas alcançadas, <a href="#fig-13">Fig. 13</a>. Foi observado que essa geometria da panela, quando utilizada pelo usuário, poderia resultar em superaquecimento do fogão, especialmente considerando a quantidade de líquido presente na panela, mostrou-se necessário um sensoriamento das temperatura com o intuito de garantir a integridade estrutura e evitar o superaquecimento.
</p>


<p style="text-indent: 50px; text-align: justify;">
A introdução desse sistema de proteção térmica permitirá garantir a segurança operacional do fogão, protegendo-o contra danos causados pelo superaquecimento e reduzindo potenciais riscos para o usuário.
</p>
  
<p style="text-indent: 50px; text-align: justify;"> 
Os valores seguem nas tabelas abaixo, as tabelas seguem o padrão do tópico anterior:
</p>

| **Geometria** | **Temperatura(°C)**|
|:---------------:|:-------------------:|
| Panela        | 230,80            |
| Liquido       | 230,50            |
| Vidro         | 167,80            |

| **Objeto** | **Temperatura(°C)** |
|:------------:|:---------------------:|
| Vidro      | 167,80              |
| Neoprene   | 30,77               |
| Sensor     | 24,01              |
| Eficiência do isolamento térmico (%) | 85.69              |



<div id="fig-13" align="center">
  <img src="../../assets/images/Gráfico_variaçãodetemperatura2.png">
  <h4>Figura 13: Caso 02 - Variação de temperatura liquido, panela, vidro</h4>
</div>



**3° Caso**:
<p style="text-indent: 50px; text-align: justify;">
No terceiro caso foi refeita a simulação do segundo caso porém com 44°C graus como temperatura ambiente a maior temperatura já registrada no Brasil <a href="#ref-9">[9]</a>, <a href="#fig-14">Fig. 14</a>, onde foi feita a análise da temperatura que passaria atráves do vidro.
</p>

<p style="text-indent: 50px; text-align: justify;">
Diante do risco de um superaquecimento e em conjunto com a análise eletrônica, foi identificada a necessidade de implementar um sistema de controle para evitar o superaquecimento do fogão. Foi definida uma temperatura de acionamento para esse sistema de superaquecimento, estabelecida em  30°C para o interior do fogão ligando o cooler de refrigeração e 50°C para o desligamento do fogão, uma faixa na qual o isolante consegue suportar sem danificar a estrutura . O Neoprene, material utilizado como isolante sob o vidro, possui uma faixa de operação de -50 °C a 120 °C, com picos de até 145 °C <a href="#ref-4">[4]</a>.
</p>


| **Geometria** | **Temperatura(°C)**|
|:---------------:|:-------------------:|
| Panela        | 245.30            |
| Liquido       | 245,10            |
| Vidro         | 184,00            |

| **Objeto** | **Temperatura(°C)** |
|:------------:|:---------------------:|
| Vidro      | 184,00              |
| Neoprene   | 49,92               |
| Sensor     | 44,01              |
| Eficiência do isolamento térmico (%) | 76,08              |

<div id="fig-14" align="center">
  <img src="../../assets/images/Gráfico_variaçãodetemperatura3.png">
  <h4>Figura 14: Caso 01 - Variação de temperatura liquido, panela, vidro</h4>
</div>

#### Distribuição da temperatura atráves do vidro
<p style="text-indent: 50px; text-align: justify;"> Com a possibilidade de encaixar o vidro temperado diretamente na estrutura ABS, foi feita uma análise sobre a distribuição da temperatura sobre o vidro utilizando o caso de uso 2, <a href="#fig-09">Fig. 09</a>, a análise foi feita no Ansys Mechanical.
</p>

<p style="text-indent: 50px; text-align: justify;">
As condições de contorno foram similares as utilizadas no código matlab:
<ul>
      <li>Temperatura Inicial = 24°C;</li>
      <li>Temperatura na base da panela = 230,8 °C;</li>
      <li>Temperatura na face inferior do vidro = 30,77 °C;</li>
      <li>Radiação na parede da panela;</li>
      <ul>
        <li>Emissividade = 0,07;</li>
        <li>Temperatura Ambiente = 24°C;</li>
      </ul>
      <li>Radiação na superficie superior do vidro;</li>
      <ul>
        <li>Emissividade = 0,92;</li>
      </ul>
      <li>Convecção na face externa da parede da panela = 18 W/m^2*°C;</li>
      <li>Convecção sobre a face superior do vidro = 15 W/m^2*°C;</li>
      <li>Convecção na face interna da parede da panela = 600 W/m^2*°C;</li>
      <li>Convecção na face superior da base da panela = 120 W/m^2*°C;</li>
    </ul>
</p>

<div id="fig-15" align="center">
  <img src="../../assets/images/distribuicao_vidro.png">
  <h4>Figura 15: Distribuição da temperatura no vidro</h4>
</div>

<div align="center">
  <video width="600" controls>
    <source src="../../assets/images/Video_distribuicaoPanela.mp4" type="video/mp4">
    Seu navegador não suporta a tag de vídeo.
  </video>
  <h4>Video 01: Distribuição da temperatura no vidro</h4>
</div>


<p style="text-indent: 50px; text-align: justify;"> Verifica-se que o o vidro em suas extremidade terá temperatura inferiores a 58°C, permitindo utilizar o vidro diretamente no ABS que tem uma faixa de temperatura superior aos 58°C, com certa margem de segurança.
</p>


#### Validação do sensor de transferência de calor
<p style="text-indent: 50px; text-align: justify;"> 
Utilizando o caso de uso mais extremo, o caso 3,  foi possível analisar que o sistema tem um tempo de resposta de 585 segundos até chegar a sua temperatura máxima, pensando nisso foi verificado no datasheet do sensor NTC 100K sua faixa de temperatura de -55°~125°C e seu tempo de resposta de 1,2 segundos <a href="#ref-7">[7]</a> para identificar 63,21% da transferência de calor onde o sensor sai do seu estado inicial até chegar em seu valor máximo em regime permanente <a href="#ref-6">[6]</a>, portanto podemos verificar que teremos 487,5 leituras do sensor neste tempo e caso ocorra um superaquecimento em  1,2 segundos o fogão será desligado.
</p>

### Análise eletromagnética

#### Coeficientes de refletância e absorvância do alumínio

<p style="text-indent: 50px; text-align: justify;"> 
A análise eletromagnética inicial consistiu na determinação da eficiência do alumínio, um material paramagnético, para refletir e absorver ondas eletromagnéticas, considerando a espessura do material. Em seguida, foi calculada a eficiência da blindagem eletromagnética, com base na geometria da blindagem. O projeto contempla três locais de blindagem;envolta da bobina;divisa com o circuito de potência:divisa com o circuito de comunicação, conforme visualizado nos desenhos mecânicos. O primeiro e principal está ao redor da bobina, com formato retângular e uma abertura superior, visando aumentar a eficiência do fogão. Em seguida, há um isolamento térmico e uma blindagem eletromagnética utilizando alumínio e Neoprene, que separam a caixa quente da caixa fria. Na caixa fria, há uma camada adicional de proteção entre o circuito de potência e o de comunicação, reduzindo a propagação das ondas eletromagnéticas até o compartimento de comunicação e controlando o transporte de calor entre os compartimentos internos. 
</p>

<p style="text-indent: 50px; text-align: justify;"> 
A eficiência do alumínio foi determinada com base nas suas propriedades refletivas e absorventes de ondas eletromagnéticas. Os resultados obtidos foram consistentes com as informações disponíveis na literatura, corroborando a precisão dos cálculos realizados. Como resultado da análise, foram obtidas a potência refletida, a potência absorvida e a eficiência da blindagem, que representa a quantidade de potência bloqueada pela blindagem eletromagnética. Todos os cálculos e dados utilizados foram fundamentados em informações encontradas no livro de referência <a href="#ref-2">[2]</a>.
</p>

<p style="text-indent: 50px; text-align: justify;"> 
Para 0,001 mm de espessura do aluminio utilizada em teste experimentais encontramos valores próximos dos experimentais o que demonstrou que o código estava correto, como foi utilizado valores do livro referência é importante observar que os mesmo são para o aluminio puro no vácuo, como o valor da frequência é baixo foi tuilizado o valor no vácuo, devido a isto a blindagem esta tão elevada:
</p>

<p style="text-indent: 50px; text-align: justify;"> 
Para a espessura de 0,001 milimetros de alumínio utilizada nos testes experimentais, os resultados obtidos pelo código foram próximos aos valores experimentais de fabricantes, <a href="#fig-16">Fig. 10</a>, evidenciando a precisão e correção do código utilizado <a href="#ref-5">[5]</a>. É importante ressaltar que os valores utilizados foram baseados em dados de alumínio puro encontrados no livro de referência. Portanto, a eficácia da blindagem alcançada pode ser atribuída à utilização desses parâmetros, que resultaram em valores elevados de eficiência <a href="#ref-2">[2]</a>:
</p>

| **Refletividade (Adimensional)** | **Absorvância (Adimensional)** |
|:------------:|:---------------------:|
|     0,99   |     0,00001            |
 
 <p style="text-indent: 50px; text-align: justify;"> 
Para 0,4 mm, a espessura que será utilizada encontrou-se os valores:
</p>

| **Refletividade (Adimensional)** | **Absorvância (Adimensional)** |
|:------------:|:---------------------:|
|     0,99   |     0,23            |

 <p style="text-indent: 50px; text-align: justify;">
 Com os coeficientes foi possivel verificar a refletividade e a absorvância do alumínio escolhido <a href="#fig-17">Fig 17</a>. 
 </p>

  | **Propriedades do Material** | **Potência (Watts)**|
  |:---------------:|:-------------------:|
  | Refletida        | 1485,00           |
  | Absorvida        | 345,00            |
  | Eficiência da blindagem (%)| 99,01|


<div id="fig-16" align="center">
  <img src="../../assets/images/diagrama_eletromag_01.png">
  <h4>Figura 16: Diagrama do código de eletromagnetismo e equações código 01</h4>
</div>

<div id="fig-17" align="center">
  <img src="../../assets/images/diagrama_eletromag_02.png">
  <h4>Figura 17: Diagrama do código de eletromagnetismo e equações código 02</h4>
</div>

#### Análise do Fenômeno Pelicular (Skin Effect)

<p style="text-indent: 50px; text-align: justify;">
Agora com o material definido o próximo passo é calcular a espessura necessária atráves da profundidade pelicular. A profundidade pelicular é a distância  que a onda deve propagar para ter a sua amplitude reduzida em 37%. É expressa por meio da equação <a href="#ref-2">[2]</a>:
</p>

![ \delta = \frac{1}{\sqrt{\pi f \mu \sigma}}](https://latex.codecogs.com/svg.image?\delta=\frac{1}{\sqrt{\pi&space;f\mu\sigma}})

![\delta = \text{Profundidade pelicular} \\
 \pi = 3,14\\
 f = 100 \ Khz\\
 \mu = \text{Permeabilidade magnetica  no  vacuo}\\
 \sigma = \text{Condutividade eletrica do aluminio}](https://latex.codecogs.com/svg.image?%5Cdelta=%5Ctext%7BProfundidade%20pelicular%7D%5C%5C%5Cpi=3,14%5C%5Cf=100%5C;Khz%5C%5C%5Cmu=%5Ctext%7BPermeabilidade%20magnetica%20no%20vacuo%7D%5C%5C%5Csigma=%5Ctext%7BCondutividade%20eletrica%20do%20aluminio%7D)
 
 <p style="text-indent: 50px; text-align: justify;"> Resolvendo a equação de profundidade pelicular chegamos nos valores apresentados:
 </p>

 | Profundidade Pelicular | Espessura (mm) |
 |------------------------|:---------:|
 | Aluminio | 0,00099 |

 <p style="text-indent: 50px; text-align: justify;">
 O alumínio com menor espessura encontrado foi de 0,4 mm o qual tem 404 vezes a espessura da profundidade pelicular, demonstrando que podemos utilizar uma chapa de 0,4 mm com uma grande margem de segurança.
 </p> 

### Sistema de ventilação do circuito de potência

<p style="text-indent: 50px; text-align: justify;">
Com a utilização de um circuito de potência, eletrônica levantou a preocupação de que o circuito possa atingir temperaturas significativas durante o funcionamento do fogão portátil.
</p>

<p style="text-indent: 50px; text-align: justify;"> O dimensionamento do sistema de ventilação do circuito será realizado por meio de uma câmera térmica, proporcionando uma maior precisão ao processo. Com base na distribuição de calor gerado, será instalado um cooler para estabelecer um fluxo de ar que percorrerá o circuito de potência, especialmente pelas aletas localizadas nas caixas de circuitos no  lado mais distante da bobina, aproveitando a alta condutividade térmica dessas aletas. O cooler do sistema de potência será fixado abaixo do circuito de potência com uma abertura para o exterior. Por onde o ar sairá por ranhuras laterais.  Essa abordagem permitirá o controle eficaz da temperatura do circuito, garantindo o desempenho adequado do sistema.  
</p>

### Sistema de ventilação da bobina

<p style="text-indent: 50px; text-align: justify;"> A bobina deve ser fixada o mais próximo possível do corpo de trabalho, a panela, pois uma distância maior leva a uma perda exponêncial, devido a sua próximidade será utilizado uma pasta térmica entre o vidro refrátario e a bobina, para auxiliar no sistema de refrigeração será alocado um cooler para refrigerar a bobina com o intuito de aumenta a sua eficiência.
</p>



## Materiais definidos


| **Estrutura** | **Material** |
|:---------------:|:--------------:|
|Apoio para bobina|  Cerâmica refratária |
|Blindagem eletromagnética| ACM e Alumínio  Liga 1100-H14 (0,4 mm)|
|Estrutura superior| Vidro refratario e temperado |
|Estrutura inferior| ABS (acrilonitrila butadieno estireno) |
|Estrutura do painel solar| Metalon |
|Isolamento térmico| Neoprene |


## Métodos de Fabricação

 <p style="text-indent: 50px; text-align: justify;">
Após a escolha dos matériais foram levantou-se os métodos de fabricação para cada estrutura com bases no material especificado:
</p>

<p style="text-indent: 50px; text-align: justify;"> 
<ul>
      <li>Estrutura inferior fogão: Impressão 3D.</li>
      <li>Estrutura painel solar: Usinagem e Soldagem.</li>
      <li>Estrutura Superior: Corte, Tempera e vedação com silicone de alta temperatura.</li>
      <li>Blindagem Eletromagnética: Fresagem.</li>
</ul> 
</p>
 
 ### Teste de colagem dos vidros

 <p style="text-indent: 50px; text-align: justify;">
Os vidros foram colados utilizando cola para vidro que suportavam até 315°C, porém como não achamos uma cola para vidro com face laminada foi feito o teste para verificar a junção dos vidros, o teste consistiu em colar duas faces laminadas e aguardar o tempo de cura total 24 horas. O teste demonstrou que para esforços laterais a cola para altas temperaturas não descolava.
</p>

## Dimensões Principais

<p style="text-indent: 50px; text-align: justify;">
No presente tópico especifica-se as dimensões gerais do produto, incluindo desenhos técnicos de todos os componentes e vistas explodidas das montagens, visando ilustrar de forma clara o dimensionamento e disposição dos materiais.
</p>
<div align="center">
  <img src="../../assets/images/estrutura_inferior_fogao.png">
  <h4>Figura 18: Estrutura inferior fogão</h4>
</div>

<div align="center">
  <img src="../../assets/images/vidro_fogao.png">
  <h4>Figura 19: Vidro Fogão</h4>
</div>

<div align="center">
  <img src="../../assets/images/vidro_superior.png">
  <h4>Figura 20: Vidro Superior</h4>
</div>

<div align="center">
  <img src="../../assets/images/vista_explodida_paineis.png">
  <h4>Figura 21: Estrutura painel solar</h4>
</div>

<div align="center">
  <img src="../../assets/images/vidro_superior.png">
  <h4>Figura 22: Vidro Superior</h4>
</div>

## Integração

Tópico voltado a integração de estruturas com os demais sistemas e mudanças que tivemos que realizar devido a integração do plano original.

### Sistema de geração de energia

 <p style="text-indent: 50px; text-align: justify;">
A primeira estrutura a ser manufaturada foi a estrutura utilizada por energia para geração de energia. A mesma foi finalizada ainda para o PC2 porém energia não conseguiu realizar testes devido a complicações que apareceram para os subsistemas de eletronica sendo realizado um teste alternativo com o carregamento de um celular. Todo o processo foi realizado pela equipe de estruturas e energia em conjunto.
</p>

<div align="center">
  <img src="../../assets/images/imagems teste vidro.jpg">
  <h4>Figura 23: Teste de Colagem Vidro Laminado </h4>
</div>

### Testes

#### Teste de ligação do sistema de energia

 <p style="text-indent: 50px; text-align: justify;">
O teste foi baseado ligar todo o sistema e carregar um celular para verificar tanto a estrutura como o sistema de energia.
</p>

<div align="center">
  <img src="../../assets/images/EstruturaSolar.jpg">
  <h4>Figura 24: Estrutura solar finalizada </h4>
</div>

Materiais:
<ul>
   
  <li> Multimetro</li>
  <li> Transformador</li>
  <li> Chave de fenda e philips</li>
  <li> Fios</li>
  <li> Carregador de celular</li>
</ul>

 <p style="text-indent: 50px; text-align: justify;">
O objetivo de estruturas era verificar se a estrutura poderia vim a gerar acidentes elétricos e se a estrutura realmente iria comportar o sistema. O resultado foi positivo e não houve imprevistos. Foram utilizados pés rosqueaveis com o intuito de fornecer uma maior estabilidade a estrutura independente do terreno.
</p>

### Integração eletrônica - estrutura

<p style="text-indent: 50px; text-align: justify;">
A segunda estrutura a ser manufaturada foi a estrutura  em ABS feita em impressão 3D. A estrutura sofreu alterações devido os encaixes do circuito de potência terem sido reaproveitados. A divisão caixa quente e caixa fria foi deixada de lado devido o isolamento térmico se mostrar eficaz e não apresentar nenhuma avaria na estrutura onde a caixa quante passou a comportar o circuito de potência e a caixa fria ficou somente com o circuito de comunicação devido o circuito ficar maior do que o previsto. Foi necessario imprimir um extensor aproximando os sensores localizados na bobina do corpo de trabalho (panela), pois devido a distância apresentava uma leitura ruim talvez por usarmos dois vidros e não um vidro sobre um polímero a estrutura superior tenha ficado mais rigida. Com os problemas relatados  foram feitos ajustes validados entre os membros envolvidos nos subsistemas. Após os ajustes houve testes com o aquecimento da água mostrando que a integração fora concluida com exito.
</p> 

### Testes

#### Blindagem eletromagnética

<p style="text-indent: 50px; text-align: justify;">
O sistema de blindagem funcionou porém como a blindagem da antena demonstrou ser o suficiente não foi necessario manter a blindagem de ACM dado que ela diminuiria o alcance da antena.
</p>

### Integração eletrônica - estruturas - energia

 <p style="text-indent: 50px; text-align: justify;">
Quando todos os sistemas foram interligados não houve problemas com a estrutura, porém alguns problemas surgiram na integração geral os membros de estruturas analisaram se a estrutura poderia estar ocasionando os erros porém nenhum erro foi devido a estrutura. 
</p>

 <p style="text-indent: 50px; text-align: justify;">
Ocorreu um erro onde as baterias previstas não conseguiram alimentar o sistema, as baterias foram trocadas porém as novas baterias tinham dimensões muito maiores o que não permitiu um ajuste na estrutura em tempo hábil para um novo encaixe e sem gerar um maior custo de manufatura.
</p>


## Referências

<div id="ref-1"></div>

>[1] *Çengel, Y. A., "Fundamentos da Transferência de Calor e Massa", 5ª edição, McGraw-Hill, 2015.*

<div id="ref-2"></div>

>[2] *Paul, C. R., "Eletromagnetismo para Engenheiros", 1° edição, John Wiley & Sons, 2017.Capitulo 05.*

<div id="ref-3"></div>

>[3] *Brastemp, Disponível em: <https://www.brastemp.com.br/experience/tech/cooktop-por-inducao/>. Acesso em: 13 abr. 2024.*

<div id="ref-4"></div>

>[4] *IBAB. Disponível em: <https://ibabrubber.com.br/compostos.pdf>. Acesso em: 04 abr. 2024.*

<div id="ref-5"></div>

>[5] *Mourão, A. A. C.,Vivaldini, D. O., Pandolfelli, V. C. "Fundamentals and analysis of high emissivity refractory coatings",  Universidade Federal de São Carlos, 2017.*

<div id="ref-6"></div>

>[6] *Silva, H. B., "CONTROLADOR DE TRANSFERÊNCIA DE CALOR (CTC) PARA ENSAIO DE CONDUTIVIDADE TÉRMICA PELO MÉTODO DA PLACA QUENTE PROTEGIDA",  Universidade Federal de Uberlândia,2020. Disponível em: <https://repositorio.ufu.br/bitstream/123456789/30927/2/ControladorTransfer%C3%AAnciaCalor.pdf>. Acesso em: 19 mai. 2024.*

<div id="ref-7"></div>

>[7] *TME, "Specifications for NTC Thermistor". Disponível em: <https://www.tme.eu/Document/f9d2f5e38227fc1c7d979e546ff51768/NTCM-100K-B3950.pdf>. Acesso em: 19 mai. 2024.*

<div id="ref-8"></div>

>[8] *Império dos Metais, "Specifications for NTC Thermistor". Disponível em: <https://www.imperiodosmetais.com.br/pdf/download_ficha_tecnica/aluminio/1100.pdf>. Acesso em: 19 mai. 2024.*

<div id="ref-9"></div>

>[9] *Folha de S.Paulo. Disponível em: <https://www1.folha.uol.com.br/cotidiano/2023/11/com-448oc-municipio-de-mg-bate-recorde-de-calor-no-pais-diz-inmet.shtml#:~:text=Com%2044%2C8%C2%BAC%2C%20munic%C3%ADpio%20de,11%2F2023%20%2D%20Cotidiano%20%2D%20Folha>. Acesso em: 19 mai. 2024.*