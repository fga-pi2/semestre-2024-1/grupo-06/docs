# Códigos de estruturas

## Profundidade pelicular
```
% Frequência em Hertz
frequencia = 100e3; % 100 kHz

% Frequência angular em rad/s
omega = 2 * pi * frequencia;

% Permeabilidade magnética no vácuo (H/m)
mu_0 = 1;

% Condutividade elétrica do alumínio (S/m)
sigma_aluminio = 3.248e6; % Para o alumínio

% Calculando a profundidade pelicular
delta = sqrt(2 / (omega * mu_0 * sigma_aluminio));

fprintf('A profundidade pelicular do alumínio para uma frequência de 100 kHz é de: %.10f milimetros \n', delta*1000);

```

## Function para transformar energia em variação de temperatura
```
function variacao_temperatura = calcular_variacao_temperatura(energia_joules, capacidade_termica)
    % Calcula a variação de temperatura em graus Celsius
    variacao_temperatura = energia_joules / capacidade_termica;
end

```

## Dilatação linear do vidro
```
% Coeficiente de dilatação linear do vidro em m/m°C
alpha = 27e-6;
% 27e-6 vidro comum;
% 9.6e-6 vidro refrátario;
% Variação de temperatura em °C
delta_T = 190;

% Dimensões do vidro
largura_vidro = 0.29;   % Largura do vidro em metros
altura_vidro = 0.37;    % Altura do vidro em metros
espessura_vidro = 0.003;% Espessura do vidro em metros

% Calculando as variações de comprimento em cada dimensão
delta_L_largura = largura_vidro * alpha * delta_T;
delta_L_altura = altura_vidro * alpha * delta_T;
delta_L_espessura = espessura_vidro * alpha * delta_T;

delta_L_largura_mm=delta_L_largura*1000
delta_L_altura_mm=delta_L_altura*1000
delta_L_espessura_mm=delta_L_espessura*1000

% Somando as variações de comprimento para obter a dilatação térmica total

delta_L_total = delta_L_largura * delta_L_altura * delta_L_espessura;
delta_L_total_mm = delta_L_largura_mm * delta_L_altura_mm * delta_L_espessura_mm;

% Imprimindo o resultado
fprintf('A dilatação térmica total do vidro a 190°C é de: %.5f metros^3\n', delta_L_total);
fprintf('A dilatação térmica total do vidro a 190°C é de: %.5f milimetros^3\n', delta_L_total_mm);
```

## Geometria da Panela
```
% Dados fornecidos
altura_parede = 9.97; % cm
diametro = 30; % cm
espessura_parede = 0.3; % cm (1 mm)

% Convertendo o diâmetro para o raio
raio_externo = diametro / 2;

% Altura total da panela
altura_total = altura_parede + espessura_parede;

% Volume do cilindro externo (panela completa)
volume_externo = pi * raio_externo^2 * altura_total;

% Raio do cilindro interno (parte oca)
raio_interno = raio_externo - espessura_parede;

% Volume do cilindro interno (parte oca)
volume_interno = pi * raio_interno^2 * altura_parede;

% Volume da parte oca
volume_oculo = volume_externo - volume_interno;

% Volume da panela (sem a parte oca)
volume_panela = volume_externo - volume_oculo;

% Densidade do ferro fundido em g/cm^3
densidade_ferro_fundido = 7.2; % g/cm^3

% Peso da panela
peso_panela = densidade_ferro_fundido * volume_oculo;

fprintf('Peso da panela de ferro fundido: %.2f gramas\n', peso_panela);
```

## Variação da Temperatura do Liquido, Panela e Vidro - Código 1:
```
close all\
%%%%%%%%%%%%DADOS DO FOGÃO, GEOMETRIA DA PANELA E QUANTIDADE LIQUIDO%%%%%%%\
% Dados do fogão por indução\
potencia = 1500;  % Potência do fogão por indução em watts\
eficiencia = 0.84;  % Eficiência de transferência de energia\
diametro_panela = 0.3; % Diâmetro da panela em metros\
espessura_parede = 0.001; % Espessura da parede da \panela em metros
altura_parede = 0.1; % Altura da parede da panela em metros\


% Área da base da panela em metros quadrados (com a espessura da base)\
raio_base = diametro_panela / 2;\
area_base = (pi * (raio_base^2));

% Área das paredes laterais da panela em metros quadrados
area_lateral = (pi*(diametro_panela / 2)^2)-(pi*((diametro_panela / 2)-(espessura_parede))^2);

% Volume de líquido na panela em metros cúbicos
volume_liquido = 0.001; % 1 litro

%%%%%%%%%%%%%%%%%%%%%%%PROPRIEDADES TERMICAS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\
% Propriedades da panela
massa_panela = 1; % Massa da panela em quilogramas\
calor_especifico_panela = 461;  
% Calor específico do material da panela em joules por quilograma por grau Celsius\
temperatura_inicial_panela = 24; % Temperatura inicial da panela em graus Celsius

% Propriedades do líquido
densidade_liquido = 1000; \
% Densidade do líquido em kg/m³ (por exemplo, água)\
calor_especifico_liquido = 4186;\
% Calor específico do líquido em J/kg°C (por exemplo, água)
temperatura_inicial_liquido = 24; %Temperatura inicial da panela em graus celsius

% Propriedades do vidro
densidade_vidro = 2500; % Densidade do vidro em kg/m³
calor_especifico_vidro = 840; % Calor específico do vidro em J/kg°C
temperatura_inicial_vidro = 24; % temperatura inicial do vidro em graus celsius

% Coeficiente de transferência de calor por convecção das paredes laterais (em watts por metro quadrado por grau Celsius)
h_paredes_panela = 18;  % Ajuste conforme necessário %convecção natural %cilindrica
h_paredes_liquido = 600; % Ajuste conforme necessário %Cilindrica

% Coeficiente de transferência de calor por convecção da base (em watts por metro quadrado por grau Celsius)
h_base = 5;

% Coeficiente de transferência de calor por condução da base  (em watts por metro por grau Celsius)
k_base = 52; % Ajuste conforme necessário %Cilindrica

% Coeficiente de transferência de calor por convecção da base para o líquido (em watts por metro quadrado por grau Celsius)
h_base_liquido = 120; % Ajuste conforme necessário %cilindrica

% Coeficiente de transferência de calor por convecção do vidro para o ambiente (em watts por metro quadrado por grau Celsius)
h_vidro_ambiente = 15; % Ajuste conforme necessário %placa plana

% Temperatura ambiente (em graus Celsius)
temperatura_ambiente = 24; % Temperatura do ambiente usada para convecção com o ar

% Constante de Stefan-Boltzmann (em W/m^2/K^4)
sigma = 5.67e-8;

% Temperatura ambiente em Kelvin
T_amb = temperatura_ambiente + 273.15;

% Dimensões do vidro
largura_vidro = 0.29; % Largura do vidro em metros
altura_vidro = 0.37; % Altura do vidro em metros
espessura_vidro = 0.01; % Espessura do vidro em metros

% Área da superfície do vidro em metros quadrados
area_vidro = largura_vidro * altura_vidro;

% Volume do vidro em metros cúbicos
volume_vidro = largura_vidro * altura_vidro * espessura_vidro;

% Tempo total de simulação (em segundos)
tempo_total = 3600;

% Passo de tempo para integração (em segundos)
passo_tempo = 1;

% Vetor de tempo
tempo = 0:passo_tempo:tempo_total;

% Inicialização dos vetores de temperatura
temperatura_panela = zeros(size(tempo));
temperatura_liquido = zeros(size(tempo));
temperatura_vidro = zeros(size(tempo));
temperatura_panela(1) = temperatura_inicial_panela;
temperatura_liquido(1) = temperatura_inicial_liquido;
temperatura_vidro(1) = temperatura_inicial_vidro;

% Loop para resolver as equações diferenciais
for i = 2:length(tempo)
    % Calcular taxa de aquecimento
    potencia_transferida = eficiencia * potencia;
    taxa_aquecimento_panela = potencia_transferida / massa_panela;
    
    % Calcular transferência de calor pela base da panela por condução
    delta_temperatura_base = temperatura_panela(i-1) - temperatura_ambiente;
    transferencia_conducao_base = k_base * area_base * delta_temperatura_base;
    
    % Calcular transferência de calor pelas paredes laterais da panela por convecção
    delta_temperatura_paredes_panela = temperatura_panela(i-1) - temperatura_ambiente;
    transferencia_conveccao_paredes_panela = h_paredes_panela * area_lateral * delta_temperatura_paredes_panela;
    
    % Calcular transferência de calor pela base da panela por convecção
    delta_temperatura_base = temperatura_panela(i-1) - temperatura_ambiente;
    transferencia_conveccao_base = h_base * area_base * delta_temperatura_base;
    
    % Calcular transferência de calor do líquido para a panela por convecção
    delta_temperatura_liquido_para_panela = temperatura_liquido(i-1) - temperatura_panela(i-1);
    transferencia_conveccao_liquido_para_panela = h_paredes_liquido * area_lateral * delta_temperatura_liquido_para_panela;
    
    % Calcular transferência de calor da base para o líquido por convecção
    delta_temperatura_base_liquido = temperatura_panela(i-1) - temperatura_liquido(i-1);
    transferencia_conveccao_base_liquido = h_base_liquido * area_base * delta_temperatura_base_liquido;
    
    % Calcular radiação térmica emitida pela panela
    temperatura_kelvin = temperatura_panela(i-1) + 273.15;
    rad_radiante = sigma * area_base * (temperatura_kelvin^4 - T_amb^4);
    
    % Calcular taxa de resfriamento total para a panela
    taxa_resfriamento_panela = transferencia_conveccao_paredes_panela + transferencia_conveccao_base + transferencia_conducao_base + rad_radiante;
    
    % Calcular variação de temperatura da panela usando o método de Euler
    temperatura_panela(i) = temperatura_panela(i-1) + (taxa_aquecimento_panela - taxa_resfriamento_panela) * passo_tempo / calor_especifico_panela;
    
    % Calcular taxa de aquecimento do líquido
    taxa_aquecimento_liquido = transferencia_conveccao_liquido_para_panela + transferencia_conveccao_base_liquido;
    
    % Calcular variação de temperatura do líquido usando o método de Euler
    temperatura_liquido(i) = temperatura_liquido(i-1) + taxa_aquecimento_liquido * passo_tempo / (densidade_liquido * calor_especifico_liquido * volume_liquido);
    
    % Calcular transferência de calor da panela para o vidro por condução
    delta_temperatura_panela_vidro = temperatura_panela(i-1) - temperatura_vidro(i-1);
    transferencia_conducao_panela_vidro = k_base * area_base * delta_temperatura_panela_vidro;
    
    % Calcular transferência de calor do vidro para o ambiente por convecção
    delta_temperatura_vidro_ambiente = temperatura_vidro(i-1) - temperatura_ambiente;
    transferencia_conveccao_vidro_ambiente = h_vidro_ambiente * area_vidro * delta_temperatura_vidro_ambiente;
    
    % Calcular taxa de resfriamento total para o vidro
    taxa_resfriamento_vidro = transferencia_conducao_panela_vidro - transferencia_conveccao_vidro_ambiente;
    
    % Calcular variação de temperatura do vidro usando o método de Euler
    temperatura_vidro(i) = temperatura_vidro(i-1) + taxa_resfriamento_vidro * passo_tempo / (densidade_vidro * calor_especifico_vidro * volume_vidro);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%GRÁFICOS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Plotar temperatura ao longo do tempo
figure;
plot(tempo, temperatura_panela, 'b-', 'LineWidth', 2);
hold on;
plot(tempo, temperatura_liquido, 'r--', 'LineWidth', 2);
plot(tempo, temperatura_vidro, 'g:', 'LineWidth', 2);
hold off;
xlabel('Tempo (s)');
ylabel('Temperatura (°C)');
title('Variação da temperatura da panela, do líquido e do vidro ao longo do tempo');
legend('Panela', 'Líquido', 'Vidro');
grid on;
```

## Eficiência do Isolante Térmico - Código 02:
```

% Propriedades dos materiais
condutividade_vidro = 0.8; % Condutividade térmica do vidro em W/(m*K)
espessura_neoprene = 0.003; % Espessura do neoprene em metros

% Dimensões do vidro
largura_vidro = 0.29; % Largura do vidro em metros
altura_vidro = 0.37; % Altura do vidro em metros
espessura_vidro = 0.005; % Espessura do vidro em metros

% Área de contato entre o vidro e o neoprene em metros quadrados
area_contato = largura_vidro * espessura_neoprene;

% Temperatura do vidro
T_vidro = 190 + 273.15; % Temperatura do vidro em Kelvin

% Temperatura do sensor
T_sensor = 24 + 273.15; % Temperatura do sensor em Kelvin

% Diferença de temperatura
delta_T = T_vidro - T_sensor; % Diferença de temperatura entre o vidro e o sensor

% Cálculo do calor que passa pelo vidro para o neoprene por condução térmica
Q_neoprene_conducao = condutividade_vidro * area_contato * (delta_T / espessura_vidro);

% Cálculo da temperatura do neoprene
T_neoprene = T_vidro - (Q_neoprene_conducao * espessura_neoprene) / (condutividade_vidro * area_contato);

% Convertendo a temperatura do neoprene de Kelvin para Celsius
T_neoprene_Celsius = T_neoprene - 273.15;

% Adicionando o calor do neoprene à temperatura do sensor
T_sensor_final = T_sensor + Q_neoprene_conducao;

% Porcentagem da temperatura do sensor em relação à temperatura do vidro em
% graus celsius
porcentagem_temperatura_sensor = ((T_sensor_final-273.15) / (T_vidro-273.15)) * 100;

%Redução da temperatura em porcentagem
porcentagem_temperatura_sensor_reduzida = 100-((T_sensor_final-273.15) / (T_vidro-273.15)) * 100;

%%%%%%%%%%%%%%%%%%%%%PRINT DOS VALORES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp(['O calor que passa pelo vidro a ' num2str(T_vidro - 273.15) '°C é: ' num2str(Q_neoprene_conducao) ' J']);
disp(['A temperatura do neoprene é: ' num2str(T_neoprene_Celsius) ' °C']);
disp(['A temperatura final do sensor, considerando o calor do neoprene, é: ' num2str(T_sensor_final - 273.15) ' °C']);
disp(['A temperatura do sensor representa ' num2str(porcentagem_temperatura_sensor) '% da temperatura do vidro']);
disp(['A temperatura reduzida ' num2str(porcentagem_temperatura_sensor_reduzida) '% da temperatura do vidro']);

%%%%%%%%%%%%%%%%%%%%%GRÁFICOS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tempo de simulação
tempo_simulacao = 0:0.1:10; % Tempo de 0 a 10 segundos

% Temperatura do vidro ao longo do tempo
temperatura_vidro = T_vidro * ones(size(tempo_simulacao)) - 273.15;

% Temperatura do sensor final ao longo do tempo
temperatura_sensor_final = T_sensor_final * ones(size(tempo_simulacao)) - 273.15;

% Porcentagem da temperatura do sensor final em relação à temperatura do vidro ao longo do tempo
porcentagem_temperatura_sensor_reduzida = porcentagem_temperatura_sensor_reduzida * ones(size(tempo_simulacao));

% Plotagem dos gráficos
figure;

subplot(3, 1, 1);
plot(tempo_simulacao, temperatura_vidro, 'b-', 'LineWidth', 2);
xlabel('Tempo (s)');
ylabel('Temperatura do Vidro (°C)');
title('Variação da temperatura do vidro ao longo do tempo');

subplot(3, 1, 2);
plot(tempo_simulacao, temperatura_sensor_final, 'r--', 'LineWidth', 2);
xlabel('Tempo (s)');
ylabel('Temperatura Final do Sensor (°C)');
title('Variação da temperatura final do sensor ao longo do tempo');

subplot(3, 1, 3);
plot(tempo_simulacao, porcentagem_temperatura_sensor_reduzida, 'g:', 'LineWidth', 2);
xlabel('Tempo (s)');
ylabel('Rendimento do Isolante (%)');
title('Redução da temperatura do sensor em relação à temperatura do vidro ao longo do tempo');

sgtitle('Gráficos de Temperatura');
```

## Absorção e Refletância do material - Código 01:

```
% Constante de permeabilidade magnética no vácuo (em H/m)
mu_0 = 1;

% Constante de permissividade elétrica no vácuo (em F/m)
epsilon_0 = 8.854e-12;

% Frequência das ondas eletromagnéticas (em Hz)
frequencia = 100e3; % 100 kHz

% Propriedades do material
espessura = 0.001; % Espessura do material (em metros)
resistividade = 0.04; % Resistividade do material (em ohms)

% Condutividade elétrica do material (em S/m)
sigma_material = 0.61;

% Impedância intrínseca do material (em ohms)
eta = sqrt(mu_0 / epsilon_0);

% Constante de propagação (em 1/m)
alpha = sqrt((1j * 2 * pi * frequencia * mu_0 * sigma_material / 2));

% Absorção do material (em %)
absorcao = 100 * (1 - exp(-2 * alpha * espessura));

% Refletância do material (em %)
refletancia = 100 * ((eta - 1) / (eta + 1))^2;

% Exibir resultados
disp(['Absorção do material: ', num2str(absorcao), '%']);
disp(['Refletância do material: ', num2str(refletancia), '%']);

```

## Eficiência da Blindagem - Código 02:
```

% Propriedades da blindagem
refletividade = 0.98; % Refletividade da blindagem (0 a 1)
absorvancia = 0.56; % Absorvância da blindagem (0 a 1)

% Propriedades das ondas eletromagnéticas
potencia_entrada = 1500; % Potência de entrada das ondas (em watts)
frequencia = 100e3; % Frequência das ondas (em Hz)

% Calcular potência refletida, absorvida e transmitida
potencia_refletida = potencia_entrada * refletividade;
potencia_absorvida = potencia_entrada * absorvancia;
potencia_transmitida = potencia_entrada - potencia_refletida - potencia_absorvida;

% Eficiência da blindagem do material (em %)
eficiencia_blindagem = 100 * (1 - refletividade/100);

% Exibir resultados
disp(['Eficiência da blindagem do material: ', num2str(eficiencia_blindagem), '%']);
disp(['Potência refletida pela blindagem: ', num2str(potencia_refletida), ' watts']);
disp(['Potência absorvida pela blindagem: ', num2str(potencia_absorvida), ' watts']);
disp(['Potência transmitida pela blindagem: ', num2str(abs(potencia_transmitida)), ' watts']);

```
