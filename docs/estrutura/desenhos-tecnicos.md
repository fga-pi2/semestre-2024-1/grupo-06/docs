
# **Desenhos Técnicos mecânicos**

<p style="text-indent: 50px; text-align: justify;">Desenhos técnicos mecânicos: indicação de cotas, cortes (se necessário),
elementos de fixação, simbologia de soldagem (se necessário), lista de
elementos e materiais em desenhos de conjunto, nas legendas indicação do
tipo de material, unidade, massa do elemento, escala, diedro, projetista
ou desenhista, revisor, dentre outras informações que auxiliem na
fabricação das estruturas mecânicas do sistema.</p>

<div align="center">
  <img src="../../assets/images/vista_explodida_fogao.png">
  <h4>Figura 01: Vista Explodida da Estrutura Inferior do Fogão</h4>
</div>

<div align="center">
  <img src="../../assets/images/chapa_aluminio.png">
  <h4>Figura 02: Chapa de alumínio</h4>
</div>

<div align="center">
  <img src="../../assets/images/junta_neoprene.png">
  <h4>Figura 03: Junta de Neoprene</h4>
</div>

<div align="center">
  <img src="../../assets/images/display.png">
  <h4>Figura 04: Display</h4>
</div>

<div align="center">
  <img src="../../assets/images/painel_sola_dimensoes.png">
  <h4>Figura 05: Dimensões Painel Solar</h4>
</div>

<div align="center">
  <img src="../../assets/images/estrutura_paineis.png">
  <h4>Figura 06: Estrutura dos Paineis solares</h4>
</div>

<div align="center">
  <img src="../../assets/images/vista_explodida_paineis.png">
  <h4>Figura 07: Vista Explodida da Estrutura dos paineis solares</h4>
</div>
