
 
# Aspectos de Gerenciamento do Projeto

## **Organização da equipe**

<p style="text-indent: 50px; text-align: justify;">A equipe do projeto está organizada de acordo com o organograma a seguir e uma melhor identificação é possível ser consultada na tabela abaixo.</p>


<div align="center">
  <img src="../../assets/images/OrganizacaoEquipe.png">
  <h4>Figura 01: Organograma Organização da Equipe. </h4>
</div>


<br>

<div>
Tabela de Organização da Equipe. Fonte: Autoria Própria, 2024
</div>


| Nome | Matrícula | Curso | Atribuições |
| :---: | :---: | :---: | :---: |
| Antonio Igor Carvalho | 180030264 | Engenharia de Software | Coordenador Geral |
| Caio Brandao Santos | 170007413 | Engenharia de Software | Desenvolvedor |
| Erik Lopes Targino | 190086823 | Engenharia Eletrônica | Diretor Técnico - Eletrônica |
| Geraldo Victor Alves Barbosa | 170011119 | Engenharia de Software | Desenvolvedor |
| Júlia Beatriz Santos Pereira | 190090081 | Engenharia Eletrônica | Desenvolvedor |
| Luan Mateus Cesar Duarte | 211041221 | Engenharia de Software | Desenvolvedor |
| Lucas Gomes Caldas | 212005426 | Engenharia de Software | Desenvolvedor |
| Lucas Eduardo Batista de Paulo | 190033177 | Engenharia de Energia | Desenvolvedor |
| Mateus Moreira Lima | 180024868 | Engenharia de Software | Diretor de Qualidade |
| João Pedro Alves Machado | 212008197 | Engenharia de Software | Desenvolvedor |
| Pedro Sampaio Dias Rocha | 211043745 | Engenharia de Software | Desenvolvedor |
| Rafael Ramos Xavier de Castro | 190134011 | Engenharia Automotiva | Diretor Técnico - Estrutura |
| Thiago Ribeiro de Moraes | 190038608 | Engenharia de Energia | Desenvolvedor |
| Victório Lázaro Rocha de Morais | 211031860 | Engenharia de Software | Diretor Técnico - Software |
| Washington Siqueira da Macena | 150152124 | Engenharia Aeroespacial | Desenvolvedor |

## **Quadro de Tarefas e Repositórios**

[Grupo 06 - Fogão Indução](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-06)

[Quadro de Tarefas](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-06/fogao-inteligente/-/boards)

[Repositório Documentação](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-06/docs)

[Repositório Subsistema Mobile](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-06/mobile)

[Repositório Subsistema Firmwares](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-06/firmwares)

[Repositório Subsistema de Fornecimento e Armazenamento de Energia](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-06/sistema-de-fornecimento-e-armazenamento-de-energia)

[Repositório Subsistema de Circuito Inteligente](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-06/desenvolvimento-do-circuito-inteligente)

[Repositório Subsistema de Circuito da Bobina](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-06/desenvolvimento-de-circuito-da-bobina)

[Repositório Subsistema de Refrigeração do Circuito](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-06/sistema-de-refrigeracao-do-circuito)

[Repositório Subsistema de Isolamento Térmico e Blindagem Eletromagnético](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-06/estrutura-e-isolamentos-termico-e-eletromagnetico)



## **Análise de riscos**

### Introdução

<p style="text-indent: 50px; text-align: justify;">Os riscos do projeto representam obstáculos e desafios que podem surgir durante o desenvolvimento, interferindo no planejamento previsto. Para lidar com esses riscos, é crucial identificá-los antecipadamente, permitindo que a equipe esteja ciente de sua existência e impacto no produto e no processo de desenvolvimento.</p>

### Métricas 

Para obter a relevância de cada risco, vamos levar em conta dois fatores, sendo eles:

- Probabilidade: A probabilidade representa a chance (%) desse risco ocorrer.

- Impacto: O impacto representa o prejuízo que cada risco oferece ao projeto caso ocorra.

Abaixo, foi representado como foi feita a estimação de cada fator e o que ela representa.

| Impacto       | Descrição                                                                            |
| ------------- | ------------------------------------------------------------------------------------ |
| Nenhum        | Nenhum impacto                                                                       |
| Muito pequeno | Praticamente sem impactos ao projeto                                                 |
| Pequeno       | Impacto pequeno ao projeto                                                           |
| Médio         | Impacto que começa a apresentar algumas consequências ao projeto                     |
| Alto          | Impacto que compromete o andamento saudável do projeto                               |
| Altíssimo     | Impacto que inviabiliza o andamento do projeto caso não seja corrigido ou minimizado |

| Probabilidade  | Descrição                               |
| -------------- | --------------------------------------- |
| Nenhum         | Nenhuma chance de acontecer             |
| Raro           | Menor que 20% de chance de acontecer    |
| Improvável     | Entre 21% e 40% de chance de acontecer  |
| Possível       | Entre 41% e 60% de chance de acontecer  |
| Altamente Possível | Entre 61% e 80% de chance de acontecer  |
| Quase Certo    | Entre 81% e 100% de chance de acontecer |

### Identificação de riscos

<p style="text-indent: 50px; text-align: justify;">A análise dos riscos foi separada em diversas categorias: <em>Materiais</em>, <em>Tecnologia</em>, <em>Recursos humanos</em>, <em>Orçamento</em>, <em>Prazo</em>, <em>Segurança</em>, <em>Qualidade</em>.</p>

Abaixo temos uma tabela com os principais riscos identificados ao decorrer do projeto.

| Categoria de Risco  | Descrição  | Impacto | Probabilidade | Ação Preventiva  | Ação Reativa |
|----|---------|---------|---------|---------|---------|
| Materiais    | Dificuldade para encontrar materiais específicos | Médio | Possível | Utilizar materiais de fácil acesso | Mudança no material utilizado para uma alternativa disponível | 
| Tecnologia    | Problemas de compatibilidade entre componentes | Altíssimo | Raro | Verificar a possibilidade de compatibilidade entre dispositivos antes de sua compra | Buscar uma solução alternativa para possibilitar a compatibilidade |
| Tecnologia    | Problemas de Eficiência Energética | Baixo | Raro | Realizar testes de eficiência energética durante o desenvolvimento | Mudar a bateria escolhida para se adequar a demanda energética |
| Recursos humanos    | Elaboração/Distruibuição ruim de tarefas | Muito Pequeno | Altamente Possível | Criação de cronograma e definição antecipada de tarefas juntamente com a revisão do grupo | Alteração da tarefa ou criação de uma nova |
| Recursos humanos   | Falta de domínio de alguma ferramenta ou software | Possível | Médio | Seleção de ferramentas e tecnologias através de heatmaps de conhecimento  | Mudança de ferramenta ou treinamento em cursos |
| Recursos humanos  | Greves dos professores | Altíssimo | Altamente Possível | - | Suspensão ou pausa do projeto |
| Recursos humanos  | Trancamento/desistência de membros | Alto | Altamente Possível | Engajamento dos membros, motivação da equipe, comunicação ativa com os membros. | Alocação de outros membros no escopo do projeto do desistente |
| Orçamento    | Custo alto dos materiais | Alto | Improvável | Escolher materiais de custo condizente com a situação financeira dos membros da equipe | Realizar vaquinha para arrecadar dinheiro para o financiamento do projeto |
| Prazo        | Não entrega de algum documento  | Médio | Quase Certo | Comunicação eficiente com os membros, acompanhamento individual do projeto | Alocação de membros para produção do documento necessário |
| Prazo    | Atraso na entrega de materiais | Alto | Possível | Encomendar materias de forma antecipada | Realizar a compra de materias presencialmente |
| Segurança    | Risco de acidentes relacionados a equipamentos durante a fabricação do projeto  | Altíssimo | Improvável | Utilização de equipamentos de proteção individual durante o manuseio de ferramentas perigosas | Chamar o serviço de atendimento móvel de urgência (SAMU) |
| Segurança    | Risco de acidentes causados por fatores referentes a materiais utilizados  | Altíssimo | Improvável | Priorizar compra de materiais que não oferecem riscos ao serem manuseados, por exemplo o risco de intoxicação por inalação de algum material | Chamar o serviço de atendimento móvel de urgência (SAMU) |
| Qualidade    | Qualidade de algum material não atender aos requisitos na hora de uso, por exemplo: isolante térmico não realizar sua função conforme esperado | Altíssimo | Raro | Realizar cálculos e simulações antes da definição de uso algum componente que esteja sucetível a falhas de qualidade |Busca por um novo material | 
| Qualidade    | Documentação ruim  | Médio | Possível | Revisões dos documentos | Refatoração dos documentos |


## **Cronograma**

<p style="text-indent: 50px; text-align: justify;">O projeto está programado para ser entregue em três pontos específicos, conforme estabelecido no plano de ensino da disciplina. Com base nessas datas e nas atividades descritas, o grupo desenvolveu um cronograma para facilitar a organização e a distribuição eficiente das tarefas.</p>

O planejamento das atividades seguiu um cronograma com datas de início e fim, detalhadas na tabela abaixo:

### Calendário
|   |   |   | Semana 1 | Semana 2 | Semana 3 | Semana 4 | Semana 5 | Semana 6 | Semana 7 | Semana 8 | Semana 9 | Semana 10 | Semana 11 | Semana 12 |
|--------------|---------|----------------------|---------------|---------------|---------------|---------------|---------------|---------------|---------------|---------------|---------------|---------------|---------------|---------------|
| Atividade | Início | Fim | 07/04 - 13/04 | 14/04 - 20/04 | 21/04 - 27/04 | 28/04 - 04/05 | 05/05 - 11/05 | 12/05 - 18/05 | 19/05 - 25/05 | 26/05 - 01/06 | 02/06 - 08/06 | 09/06 - 15/06 | 16/06 - 22/06 | 23/06 - 29/06 |
| **PONTO DE CONTROLE 1** | 07/04 | 28/04 | x | x | x |  |   |   |   |   |   |   |  |
| Definição dos Subsistemas | 07/04 | 10/04 | x |   |   |   |   |   |  |  | |  |  |
| Concepção e detalhamento da solução | 07/04 | 10/04 | x | | | | | | | | | | |
| Detalhamento do problema | 07/04 | 10/04 | x |  | | | | | | | | | |
| Documento de introdução | 07/04 | 12/04 | x | x | | | | | | | | | |
| Levantamento de normas técnicas relacionadas ao projeto | 07/04 | 12/04 | x | x | | | | | | | | | |
| Identificação de soluções comerciais | 07/04 | 12/04 | x | x | | | | | | | | | |
| Objetivo geral do projeto | 07/04 | 12/04 | x | x | | | | | | | | | |
| Objetivo específico do projeto | 07/04 | 12/04 | x | x | | | | | | | | | |
| Levantamento de Riscos / Plano de Contingência | 07/04 | 12/04 | x | x | | | | | | | | | |
| Definição de Materiais e Componentes | 07/04 | 23/04 | x | x | x | | | | | | | | |
| Termo de Abertura do Projeto (TAP) | 12/04 | 17/04 | x | x | | | | | | | | | |
| Requisitos Gerais | 12/04 | 17/04 | x | x | | | | | | | | | |
| Estrutura Analítica de Projeto (EAP) ou Work Breakdown Structure (WBS) | 12/04 | 17/04 | x | x | | | | | | | | | |
| Definição de arquitetura geral do projeto | 12/04 | 19/04 |x | x | x | | | | | | | | |
| Estimativa de Custos e Orçamentos | 12/04 | 23/04 | x | x |x | | | | | | | | |
| Diagramas com detalhes de lógicas e protocolos de comunicação entre elementos | 12/04 | 23/04 |x | x |x | | | | | | | | |
| Diagramas detalhando barramentos de alimentação | 12/04 | 23/04 |x | x| x| | | | | | | | |
| Diagramas esquemáticos de circuitos eletrônicos | 12/04 | 23/04 |x |x |x | | | | | | | | |
| Diagramas unifilares/trifilares | 12/04 | 23/04 |x | x | x | | | | | | | | |
| Desenhos mecânicos/estruturais | 12/04 | 23/04 |x | x | x | | | | | | | | |
| Requisitos de Software | 12/04 | 17/04 |x | x |x | | | | | | | | |
| Protótipo de baixa fidelidade | 18/04 | 23/04 | | x | x | | | | | | | | |
| Diagramas de casos de uso | 18/04 | 23/04 | | x | x | | | | | | | | |
| Diagramas com protocolos de comunicação entre componentes do software | 18/04 | 23/04 | | x | x | | | | | | | | |
| Diagrama de classe | 18/04 | 23/04 | | x |x | | | | | | | | |
| Revisão | 24/04 | 28/04 | | | x | | | | | | | | |

### Orçamentos

#### Estrutura

| Componente        | Tipo/Dimensão                                    | Quantidade | Preço Unitario |
|-------------------|--------------------------------------------------|------------|----------------|
| Placa de aluminio | Chapa De Alumínio Xadrez 1,2mm X 40 Cm X 60 Cm   | 1          | R$ 85,71       |
| Vidro Refratario   | Chapa de vidro refratario 50x50 | 1          | R$ 200,00         |
| Filamento ABS     | 1 kg De Filamento Abs Para Impressora 3d, 1,75mm | 6          | R$ 288,00        |
|                   |                                                  | Total      | R$ 592,71         |

#### Eletrônica

| Componentes                   | Valor      | Quantidade  | Valor Final |
|-------------------------------|------------|-------------|-------------|
| BUZZER **                     |  R$ 2,07   | 1           |  R$ 2,07    |
| Capacitor 1kV **              |  R$ 0,25   | 2           |  R$ 0,50    |
| Capacitor 450V/6.8uF **       |  R$ 2,10   | 1           |  R$ 2,10    |
| Capacitor 275V/8uF **         |  R$ 38,77  | 1           |  R$ 38,77   |
| Capacitor 50V/10uF **         |  R$ 0,24   | 1           |  R$ 0,24    |
| Capacitor 4.7nF **            |  R$ 0,37   | 1           |  R$ 0,37    |
| Capacitor 104 **              |  R$ 0,06   | 9           |  R$ 0,54    |
| Capacitor 204pF **            |  R$ 2,23   | 1           |  R$ 2,23    |
| Capacitor 5uF/275VAC **       |  R$ 2,85   | 1           |  R$ 2,85    |
| Capacitor 211 **              |  R$ 0,96   | 1           |  R$ 0,96    |
| Capacitor 25V/220uF **        |  R$ 0,29   | 1           |  R$ 0,29    |
| Capacitor 27uF **             |  R$ 4,20   | 1           |  R$ 4,20    |
| Capacitor 0.33uF/630V **      |  R$ 2,92   | 1           |  R$ 2,92    |
| Capacitor 330pF **            |  R$ 4,00   | 1           |  R$ 4,00    |
| Capacitor 105 **              |  R$ 0,80   | 1           |  R$ 0,80    |
| Capacitor 106 **              |  R$ 3,00   | 5           |  R$ 15,00   |
| Capacitor 10V/100uF **        |  R$ 0,29   | 1           |  R$ 0,29    |
| Capacitor 25V/100uF **        |  R$ 0,16   | 1           |  R$ 0,16    |
| Header 4 **                   |  R$ 1,60   | 1           |  R$ 1,60    |
| Res varistor **               |  R$ 0,33   | 1           |  R$ 0,33    |
| Diodo 1N4007 **               |  R$ 0,70   | 4           |  R$ 2,80    |
| Diodo BYV26C **               |  R$ 1,80   | 1           |  R$ 1,80    |
| Bridge diode **               |  R$ 18,24  | 1           |  R$ 18,24   |
| Diode UF4007 **               |  R$ 0,21   | 2           |  R$ 0,42    |
| Fusível 12.5A/250V **         |  R$ 35,00  | 1           |  R$ 35,00   |
| IGBT H20PR5 **                |  R$ 8,50   | 1           |  R$ 8,50    |
| Inductor 360 uH **            |  R$ 9,40   | 1           |  R$ 9,40    |
| Regulador L78L05 **           |  R$ 0,76   | 1           |  R$ 0,76    |
| Transistor S8050 **           |  R$ 0,85   | 1           |  R$ 0,85    |
| Trasformador  QJ-EE10 **      |  R$ 0,93   | 1           |  R$ 0,93    |
| Diodo Zener **                |  R$ 0,21   | 1           |  R$ 0,21    |
| Esp32 *                       |  R$ 25,23  | 1           |  R$ 25,23   |
| Resistor 470k ohms **         |  R$ 0,13   | 12          |  R$ 1,56    |
| Resistor 0 ohms **            |  R$ 0,13   | 5           |  R$ 0,65    |
| Resistor 16k ohm **           |  R$ 0,05   | 1           |  R$ 0,05    |
| Resistor 20k ohm **           |  R$ 0,13   | 1           |  R$ 0,13    |
| Resistor 150k ohm **          |  R$ 0,15   | 3           |  R$ 0,45    |
| Resistor 110k ohm **          |  R$ 0,13   | 7           |  R$ 0,91    |
| Resistor 2k ohm **            |  R$ 0,13   | 1           |  R$ 0,13    |
| Resistor 47k ohm **           |  R$ 0,13   | 3           |  R$ 0,39    |
| Resistor 1k ohm **            |  R$ 0,13   | 1           |  R$ 0,13    |
| Resistor 4.7k ohm **          |  R$ 0,13   | 1           |  R$ 0,13    |
| Resistor 30 ohm **            |  R$ 0,13   | 1           |  R$ 0,13    |
| Resistor 30 ohm **            |  R$ 0,13   | 1           |  R$ 0,13    |
| Touch Screen *                |  R$ 84,09  | 1           |  R$ 84,09   |
|                               |            | Valor Total |  R$ 273,24  |
| LEGENDA                       |            |             |             |
| Compra Necessária     | *         |             |             |
| Compra Não Necessária | **          |             |             |

#### Energia

| Componente                               | Quantidade | Unidade | Valor        |
|------------------------------------------|------------|---------|--------------|
| Módulo Solar Fotovoltaico Kyocera KC45   | 4          | UN      | Já adquirido |
| Inversor De Tensão Xantrex Prosine 1000w | 1          | UN      | Já adquirido |
| Bateria ATM POWER AP 12-18               | 2          | UN      | Já adquirido |
| Cabo de 1,5mm                            | 1          | M       | R$ 2,50      |
| Cabo de 2,5mm                            | 2          | M       | R$ 2,50      |
| CG PWM RBL-30A LCD 30A                   | 1          | UN      | R$ 46,00     |