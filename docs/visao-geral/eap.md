# EAP (Estrutura Analítica de Projeto) Geral do Projeto

<p style="text-indent: 50px; text-align: justify;">A EAP é um documento composto pela decomposição hierárquica em pequenos componentes do trabalho necessário para se concluir um projeto, começando com o projeto como um todo e o dividindo em partes menores.</p>

<p style="text-indent: 50px; text-align: justify;">No contexto do nosso projeto, o documento da EAP foi subdividido em várias seções. Incluímos uma EAP geral, abrangendo todo o escopo do projeto, além de uma EAP específica para cada subsistema identificado pelo grupo.</p>

## **EAP Geral**

<div align="center">
  <img src="../../assets/images/apendice1/eap_Geral.png">
  <h4>Figura 02: EAP Geral dos subsistemas.</h4>
</div>

## **EAP estruturas**

<div align="center">
  <img src="../../assets/images/apendice1/eap_estruturas.png">
  <h4>Figura 03: EAP de estruturas.</h4>
</div>

## **EAP do sistema de Alta Potência**

<div align="center">
  <img src="../../assets/images/apendice1/eap_circuitoAltaPotencia.png">
  <h4>Figura 04: EAP subsistema de Alta Potência.</h4>
</div>

## **EAP do sistema de Baixa Potência**

<div align="center">
  <img src="../../assets/images/apendice1/eap_circuitoBaixoPotencia.png">
  <h4>Figura 05: EAP subsistema de Baixa Potência.</h4>
</div>

## **EAP do Aplicativo Móvel**

<div align="center">
  <img src="../../assets/images/apendice1/eap_aplicativoMovel.png">
  <h4>Figura 06: EAP Aplicativo Móvel.</h4>
</div>