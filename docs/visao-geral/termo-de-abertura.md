# Termo de abertura do projeto

## **Título do Projeto**

<p style="text-indent: 50px; text-align: justify;">
Fogão Inteligente - Fogão a Indução off-grid integrado a sistema web
</p>

## **Descrição do Projeto**

<p style="text-indent: 50px; text-align: justify;">O projeto em questão se trata de um fogão inteligente a indução, alimentado por um sistema off-grid. Com isso, ele será equipado com uma boca de indução e será operado com bateria recarregável alimentada por energia solar como fonte de energia tornando, portanto, o fogão uma opção sustentável e flexível, proporcionando autonomia aos utilizadores em diferentes ambientes que não possuem acesso a eletricidade.</p>

<p style="text-indent: 50px; text-align: justify;">Além disso, o fogão é integrado a um sistema web que permite o usuário controlar as suas funcionalidades remotamente. Por meio de seus celulares, os usuários terão liberdade tanto para ligar e desligar o fogão quanto para programar o tempo de cozimento de diferentes alimentos.</p>

## **Objetivo do projeto**

<p style="text-indent: 50px; text-align: justify;">O projeto Fogão Inteligente tem como objetivo principal proporcionar aos usuários uma experiência inovadora, oferecendo mobilidade e autonomia no preparo de alimentos em qualquer lugar. Dessa maneira, o fogão será alimentado por uma bateria recarregável através da energia solar, excluindo a dependência de energia elétrica. Ademais, além de ter um display físico, o fogão será integrado com uma Aplicação Web, permitindo que os utilizadores consigam controlar algumas funcionalidades de forma remota, proporcionando maior flexibilidade para os usuários.</p>

## **Propósito do projeto**

<p style="text-indent: 50px; text-align: justify;">A criação do projeto se deu pelo propósito de oferecer uma solução sustentável que permita aos utilizadores cozinhar alimentos de forma eficaz e flexível em qualquer ambiente, tendo energia elétrica ou não, resolvendo o problema da falta de autonomia encontrada nos fogões convencionais.</p>

## **Stakeholders**

<p style="text-indent: 50px; text-align: justify;"> <strong>Equipe de Desenvolvimento:</strong> Equipe composta por 15 membros dos cursos de Engenharia Aeroespacial, Engenharia Automotiva, Engenharia de Energia, Engenharia de Software, Engenharia Eletrônica da Universidade de Brasília (UnB). A equipe é responsável por definir o detalhamento do problema, objetivos do projeto e todo o escopo, além de elicitar os requisitos e realizar a construção do Fogão Inteligente.</p>

<p style="text-indent: 50px;text-align: justify;"> <strong>Professores:</strong> Responsáveis por avaliar e aprovar o projeto. Com isso, desempenham um papel de extrema importância na orientação da equipe de desenvolvimento e na avaliação do projeto.</p>

<p style="text-indent: 50px;text-align: justify;"> <strong>Usuário Final:</strong> Interessados em utilizar o fogão em áreas de camping ou em residências. Dessa forma, eles terão acesso ao fogão e à aplicação web para aproveitar todas as funcionalidades oferecidas.</p>