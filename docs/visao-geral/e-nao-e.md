# Lista É / Não É

| É | Não É |
| --- | --- |
| Um fogão a indução alimentado por um sistema off-grid | Alimentado por energia de uma tomada |
| Um fogão que possui display para controlar as suas funcionalidades | Um fogão de múltiplas bocas |
| Um equipamento que proporciona mobilidade e flexibilidade de uso | Um fogão sem capacidade de monitoramento remoto |
| Um fogão que pode ser controlado por um aplicativo | Um fogão somente com botões analógicos |
| Um fogão que utiliza fonte de energia renovável | Um fogão fixo e inflexível |
| Um fogão alimentado por painéis solares |  |