# Visão Geral

## Introdução

<p style="text-indent: 50px; text-align: justify;">Este é um projeto multidisciplinar de engenharia, que une conhecimentos e habilidades de cinco diferentes cursos de engenharia: Engenharia de Software, Engenharia de Energia, Engenharia Eletrônica, Engenharia Aeroespacial e Engenharia Automotiva. Sendo a missão de cada engenharia no desenvolvimento do produto:</p>



<ul>
    <li>
        <span><b>Engenharia de Energia:</b></span> Utilização de tecnologias de energia renovável, como painéis solares, para alimentar o fogão, reduzindo sua dependência de fontes de energia tradicionais. Projeto de sistemas de armazenamento de energia eficientes para garantir que o fogão possa funcionar mesmo em condições de baixa luminosidade solar. Utilização de uma bateria externa para armazenamento de energia que será usada pelo fogão.
    </li>
    <li>
        <span><b>Engenharia Eletrônica:</b></span> Implementação de sistemas eletrônicos de controle e monitoramento para garantir a segurança e o desempenho do fogão. Desenvolvimento de circuitos de controle de potência para regular a intensidade do calor produzido pela boca de indução do fogão.
    </li>
    <li>
        <span><b>Engenharia Aeroespacial:</b></span> Aplicação de materiais leves e resistentes para garantir a portabilidade e durabilidade do fogão. Sistema de refrigeração para bobinas e circuitos. Incorporação de tecnologias de isolamento térmico avançadas para otimizar o uso de energia e garantir um aquecimento eficiente dos alimentos. Desenvolvimento do layout da blindagem eletromagnética com o intuito de aumentar a eficiência da bobina e proteger o circuito de comunicação de interferências internas originadas pela bobina. Auxiliar as outras engenharias em seus subsistemas.
    </li>
    <li>
        <span><b>Engenharia Automotiva:</b></span> Análise de estabilidade para determinadas angulações seguindo normas. Escolha de métodos de fabricação para cada material. Desenhos mecânicos segundo a ABNT. Análises estruturais. Desenvolvimento de encaixes e/ou travas. Auxiliar as outras engenharias em seus subsistemas. 
    </li>
    <li>
        <span><b>Engenharia de Software:</b></span> Implementação de um software para controle e monitoramento de um fogão, sendo o controle feito a partir de uma conexão bluetooth e o monitoramento a partir do software. Para atingir este resultado uma série de artefatos de software são necessários tais como: protótipos de baixa e alta fidelidade, diagrama de classes, diagrama de casos de uso, diagrama de componentes, histórias de usuário. Estes artefatos ajudarão a equipe no desenvolvimento do produto.
    </li>
</ul>

<p style="text-indent: 50px; text-align: justify;">Com o objetivo de promover a integração entre diferentes áreas da engenharia em um projeto multidisciplinar, alunos e professores propuseram o desenvolvimento de um produto que atendesse às exigências acadêmicas da disciplina. Assim, surgiu a ideia de desenvolver um fogão portátil inteligente, que combina inovação e praticidade, atendendo às necessidades de eficiência energética. Além disso, cumpre com os objetivos de desenvolvimento sustentável, a potência do fogão está em 1000W devido aos inversores disponíveis, porém por critérios de segurança entre as engenharias utiliza-se um coeficiente de 1,5, obtendo 1500W de margem de segurança.</p>
<p style="text-indent: 50px; text-align: justify;">
O projeto do Fogão Inteligente propõe um design inovador: um fogão portátil com uma única boca de indução, alimentado por bateria recarregável via energia solar. Este fogão não apenas atende à demanda por soluções de cozinha automatizadas e eficientes, mas também incorpora tecnologia de ponta para melhorar a experiência culinária. Equipado com um display touch para controle local, o fogão também é compatível com um aplicativo para controle remoto, permitindo aos usuários personalizar suas experiências culinárias ao registrar pratos preferidos. Além disso, uma antena integrada amplifica o sinal, garantindo conectividade constante através de IoT, já que a essência do IoT reside na capacidade dos dispositivos de coletar dados em tempo real, processá-los e tomar decisões com base nessas informações. Esse conjunto de características faz do Fogão Inteligente uma solução versátil e conveniente para as necessidades modernas de cozimento. Outro fator importante que vale ressaltar é o fato do fogão utilizar materiais atóxicos que em sua maior parte são recicláveis o que torna o ciclo de vida do produto mais sustentável. 
</p>
<p style="text-indent: 50px; text-align: justify;">

O Fogão Inteligente integra um módulo de recarga externo, sustentável, composto por painéis solares e uma bateria para armazenamento de energia elétrica. Este sistema não apenas reforça o compromisso com a sustentabilidade, mas também garante a autonomia energética do aparelho.

Em conformidade com a Lei de Inovação Tecnológica (Lei 10.973), a inovação tecnológica do projeto se destaca pela possibilidade de monitorar e controlar o fogão remotamente. No entanto, medidas de segurança são implementadas, como a exigência de que o usuário esteja a no máximo 5 metros de distância para ativar o fogão, especialmente se houver objetos sobre sua superfície. Além disso, o fogão permite o cadastro e acompanhamento de receitas através do aplicativo, promovendo uma experiência culinária automatizada e personalizada.

Como variáveis de cozimento foram utilizadas as mesmas variáveis de eficiência térmica. Os cálculos de eficiência térmica do fogão consideram diversas variáveis, incluindo a geometria e o material da panela, a quantidade de líquido nela contida, o tempo de cozimento, a presença de vento no ambiente e a potência do fogão. Esses fatores são essenciais para otimizar o desempenho energético.
</p>

## Detalhamento do problema

<p style="text-indent: 50px; text-align: justify;">Ao fazer qualquer atividade ao ar livre, hoje em dia, torna-se evidente a dependência que temos da energia elétrica, pois a maioria dessas atividades necessitam de um tipo de fonte de alimentação ligada à uma tomada, seja para carregar alguma bateria interna ou até mesmo para uso contínuo em seu funcionamento. Nesse contexto, tendo em vista o preparo de um alimento, por mais que existam fogões a gás e por indução que sejam transportáveis e eficientes, eles não possuem características sustentáveis e inovadoras.</p>

<p style="text-indent: 50px; text-align: justify;">Assim, será desenvolvido um fogão a indução inteligente alimentado por um sistema off grid solar, que é um sistema autossuficiente capaz de gerar energia por meio de painéis solares e armazená-las em baterias para serem utilizadas e recarregadas a qualquer momento. Isso dará mais mobilidade e flexibilidade em seu uso, especialmente em acampamentos, pescarias, entre outros.</p>

O fogão inteligente terá como principais funcionalidades:

- Segurança: desligamento automático e detecção erros; 
- Facilidade de uso: interface de usuário intuitiva e recursos automatizados controlados remotamente, como estado (ligado/desligado), potência e timers; 
- Ferramentas: notificações remotas e no próprio display do fogão, gráficos de consumo de energia ao longo do tempo, receitas com tempo e potência pré-definidas e salvamento de configurações personalizadas;
- Controle mais precisos sobre variáveis de cozimento: temperatura, tempo, vento, potência, geometria e material da panela e quantidade de líquido na panela;
- Eficiência energética: consumo responsável de energia solar e redução dos custos a longo prazo.

O fogão inteligente atenderá aos seguintes requisitos:

- Fogão de indução com uma boca;
- Sistema portátil off-grid com baterias que serão alimentadas por recarregamento de energia solar;
- Controle térmico dos componentes e isolamento adequado da superfície para garantir segurança durante o uso;
- Inclusão de um display no fogão para controlar suas funcionalidades;
- Controle completo dos recursos do fogão por meio de um aplicativo em dispositivos móveis;
- Autonomia planejada do fogão baseado em suas baterias;
- Sistema de controle de superaquecimento;
- Sistema de estabilidade e fixação do fogão;
- Designer portátil.

## Levantamento de normas técnicas relacionadas ao problema

<p style="text-indent: 50px; text-align: justify;">A implementação do fogão por indução deve seguir uma série de normas técnicas para garantir a segurança, eficácia e conformidade com as práticas adequadas, sendo elas:</p>


#### **ABNT NBR NM 60335-1:2010**

  - **Segurança de aparelhos eletrodomésticos e similares:** Essa norma estabelece condições para garantir a segurança relacionado ao isolamento elétrico, proteção contra choques e proteção térmica <a href="#ref-1">[1]</a>.

  - #### **Estrutura**

    - **32 Radiação e toxicidade:**
        - 
        <div style="text-align: justify;">
        Para garantir a conformidade com a cláusula, solicitamos os dados dos materiais para os fabricantes para conferir se os mesmos apresentam algum tipo de radioatividade. Dessa maneira, os materiais não tóxicos foram selecionados para a construção do aparelho como cerâmica refratária, alumínio e vidro refratário.
        </div>
    - **31 Ferrugem:**
        - 
        <div style="text-align: justify;">
        Para garantir a resistência à ferrugem dos materiais metálicos utilizados no painel solar, como o metalon, todos os componentes metálicos foram lixados para remover quaisquer impurezas superficiais e, depois, pintados. Esses tratamentos vão proteger o metal contra a oxidação e vão prolongar a vida útil do aparelho.
        </div>
    - **30 Resistência ao fogo e incêndio:**
        - 
        <div style="text-align: justify;">
        Utilizaremos materiais que resistam à propagação do fogo. Dessa forma, a estrutura superior do fogão por indução, feita de vidro refratário e temperado, é resistente a altas temperaturas e não entra em combustão facilmente, permitindo que o aparelho suporte grandes temperaturas sem comprometer a sua integridade e sem risco de incêncio.
        </div>
    - **11 Aquecimento:**
        - 
        <div style="text-align: justify;">
        Afim de testar o aquecimento do fogão off-grid e garantir que não exceda as temperaturas seguras em superfícies acessíveis, o grupo vai utilizar uma câmera térmica para verificar a temperatura que os componentes do fogão irão atingir. Ademais, análises no Ansys Mechanical foram feitas com o intuito de entender o aquecimento que o sistema pode alcançar em cenários que as temperaturas estão elevadas.
        </div>
    - **20 Estabilidade e riscos mecânicos:**
        - 
        <div style="text-align: justify;">
        Testes de estabilidade serão conduzidos inclinando o painel solar a ângulo 10º, devido à dificuldade de desenvolver um sistema de regulagem de altura em 3D. Este impasse surge por que a equipe não pode utilizar soluções prontas, que geralmente se baseiam em materiais ferromagnéticos, se tornando incompatíveis com as necessidades específicas do nosso projeto.
        </div>
    - **21 Esforços Mecânicos:**
        - 
        <div style="text-align: justify;">
        A construção será projetada para suportar esforços mecânicos esperados durante o uso normal do fogão, garantindo que as partes do aparelho tenham resistência adequada para lidar com o manuseio brusco. Com isso, foram utilizados testes de capacidade máxima das estruturas, com o intuito de torna-lo mais leve, além de garantir a integridade do fogão.
        </div>
    - **22 Construção:**
        - 
        <div style="text-align: justify;">
        O uso de materiais como cerâmica refratária, alumínio, vidro temperado, ABS e metalon garante que a construção atenda aos padrões de segurança e durabilidade exigidos na cláusula.
        </div>

  - #### **Eletrônica**

    - **7: Marcação e Instruções:**
        - 
        <div style="text-align: justify;">
        7.12 Instruções de uso: as instruções de uso vão ser apresentadas por meio de uma sinalização adequada, com adesivos estrategicamente colocados no fogão, para fornecer informações sobre operação e segurança. Esses adesivos vão incluir noções sobre riscos potenciais, como choque elétrico.
        </div>
    - **8 Proteção contra Acesso a Partes Ativas:**
        - 
        <div style="text-align: justify;">
        Para atender a essa cláusula, nossa abordagem será estrutural. O grupo vai garantir, por meio de testes e inspeções, que todas as partes ativas do fogão por indução estejam isoladas e protegidas, afim de evitar qualquer contato acidental durante o uso normal e a manutenção.
        </div>
    - **11 Aquecimento:**
        - 
        <div style="text-align: justify;">
        Para garantir que o fogão off-grid não exceda as temperaturas seguras em superfícies acessíveis, utilizaremos medições de um sensor externo de temperatura.
        </div>
    - **19 Operação Anormal:**
        - 
        <div style="text-align: justify;">
        O grupo vai submeter o fogão por indução a situações atípicas durante os testes de segurança como, falhas simuladas ou condições de operação fora do padrão de costume. Dessa maneira, podemos verificar se o fogão é capaz de desligar-se ou operar de forma segura, mesmo em condições anormais.
        </div>
    - **22 Construção (Especificamente para aparelhos de indução):**
        - 
        <div style="text-align: justify;">
        Iremos analisar detalhadamente o esquemático do fogão. Isso nos permitirá verificar se os componentes de indução foram projetados e montados de acordo com os requisitos específicos para garantir a segurança e eficiência do fogão.
        </div>

#### **NBR 5410:2004**
  
  - **Instalações elétricas de baixa tensão:** Esta norma trata das condições adequadas para o projeto, execução e segurança das instalações elétricas de baixa tensão. <a href="#ref-2">[2]</a>.

  - #### **Energia**

    - **3.1 Componentes da Instalação:**
        - 
        <div style="text-align: justify;">
        Essa cláusula será aplicada, considerando que o sistema de abastecimento por energia fotovoltaica off-grid utiliza diversos componentes elétricos, incluindo módulos fotovoltaicos, baterias, inversores e controladores de carga.
        </div>
    - **3.2 Proteção contra choques elétricos:**
        - **3.2.1 Elemento condutivo ou parte condutiva:** 
        <div style="text-align: justify;">
        A estrutura onde o sistema será instalado, embora não tenha essa finalidade, pode conduzir eletricidade. Por ser composta de material ferroso, como o metalon, essa estrutura externa ao sistema de geração funciona como um elemento condutivo ou parte condutiva, conforme especificado no subitem 3.2.1.
        </div>
        - **3.2.2 Proteção básica:** 
        <div style="text-align: justify;">
        O meio pelo qual os componentes do sistema fotovoltaico podem transmitir energia para a estrutura, ocasionando acidentes, é através das ligações de um componente ao outro. A solução encontrada para minimizar os riscos foi passar os fios por meio de eletrodutos, quando possível, e se estes estiverem em contato com a estrutura.
        </div>
    - **3.5.2 Alimentação ou fonte normal**
        - 
        <div style="text-align: justify;">
        Essa cláusula é cumprida, pois o sistema de alimentação responsável pelo fornecido regular
        de energia elétrica é o sistema fotovoltaico off-grid. Essa tecnologia de abastecimento é independente da rede elétrica e cumpre perfeitamente sua função.
        </div>
    - **4.1.10 Acessibilidade dos componentes:**
        - 
        <div style="text-align: justify;">
        Os componentes da instalação estão dispostos de maneira que facilita a sua instalação inicial, com muito espaço entre um componente e outro. Isso também permite uma fácil manutenção dos itens, uma possível troca, ou algo similar. Além disso, todos os espaços, entre uma bateria e outra, e a orientação do inversor no suporte permitem uma perfeita ventilação dos componentes, evitando assim o superaquecimento dos mesmos.
        </div>
    - **4.1.14 Verificação da instalação:**
        - 
        <div style="text-align: justify;">
        A instalação elétrica será inspecionada pelo professor da área de energia para garantir que todos os componentes estejam conectados corretamente, evitando possíveis danos aos equipamentos fornecidos pela Universidade de Brasília. Além disso, foram realizados ensaios de medição com multímetros nas baterias, assegurando que estão em perfeito estado.
        </div>


#### **ISO/IEC 12207:2020**

  - **Processo de ciclo de vida de software:** Esta norma define atividades associadas ao ciclo de vida de um software, desde a definição dos requisitos até a manutenção do software do sistema <a href="#ref-3">[3]</a>.
- #### **Software**
    - **5.3 Processo de desenvolvimento**
        - **5.3.1 Implementação do processo:** Inicialmente, o grupo avaliou o escopo e complexidade do projeto. Com base nessa análise, definimos o modelo de ciclo de vida de software, o ágil. Dessa forma, podemos ter entregas constantes e a flexibilidade para modificar os requisitos, caso seja necessário. Além disso, selecionamos ferramentas e linguagens de programação apropriados para o projeto como, por exemplo, o React que vai permitir desenvolver o sistema de forma eficaz, atendendo as nossas necessidades.
        - **5.3.4** **Análise dos requisitos do software:** Os requisitos do software foram documentados de forma abrangente, incluindo especificações funcionais e não funcionais. Dessa forma, cada aspecto dos requisitos foi documentado, garantindo uma compreensão clara e completa das necessidades do sistema a ser desenvolvido
    - **6.1 Processo de documentação:**
        - O processo de documentação será feito de forma abrangente para registrar todas as informações ao longo do ciclo de vida do desenvolvimento de software, incluindo planejamento, requisitos, protótipos e desenvolvimento.


## Identificação de soluções comerciais

<p style="text-indent: 50px; text-align: justify;">Soluções comerciais referem-se a uma variedade de produtos e serviços desenvolvidos por empresas que atendem às necessidades comerciais e industriais semelhantes. Estas soluções incluem <em>software</em>, <em>hardware</em> e produtos personalizados que competem diretamente com o produto desenvolvido neste projeto.</p>

<p style="text-indent: 50px; text-align: justify;">Fogões <em>cooktops</em> de indução são produtos já bem disseminados no mercado, tendo vários modelos disponíveis, de diversas marcas, com diferentes recursos e particularidades para cada modelo.</p>

<p style="text-indent: 50px; text-align: justify;">Dado o produto elaborado pela equipe, serão comparados os 8 melhores cooktops de indução com 1 queimador disponíveis no mercado <a href="#ref-4">[4]</a>, comparando-os com as funcionalidades que serão apresentadas no produto desenvolvido pelo grupo de <strong>PI2.</strong> </p>

| -                                         | Eletrolux | Philco  | Midea   | Fischer | Britânia | SUGGAR  | Perfect Cuisine | Tramontina | Projeto PI2 |
| ----------------------------------------- | --------- | ------- | ------- | ------- | -------- | ------- | --------------- | ---------- | ----------- |
| Potência Máxima                           | 2000 W    | 2000 W  | 2000 W  | 2000 W  | 2000 W   | 2000 W  | 2000 W          | 2000 W     | 1000 W      |
| Níveis de Potência                        | 10        | 10      | 10      | 10      | 10       | 9       | 10              | 10         | 10          |
| Diâmetro máximo das panelas               | 220mm     | 220mm   | 195mm   | 180mm   | 220mm    | 220mm   | 260mm           | 200 mm     | 220 mm      |
| Duração do Timer                          | 180 min   | 180 min | 240 min | 180 min | 180 min  | 120 min | 180 min         | 24h        | 180 min     |
| Trava de Segurança                        | Sim       | Não     | Sim     | Sim     | Sim      | Sim     | Sim             | Sim        | Sim         |
| Integração com aplicativo                 | -         | -       | -       | -       | -        | -       | -               | -          | X           |
| Alimentado por Energia Solar Fotovoltaica | -         | -       | -       | -       | -        | -       | -               | -          | X           |

## Inovação

<p style="text-indent: 50px; text-align: justify;">Segundo o <b>Art. 2º da Lei Nº 13.243</b>, de 11 de janeiro de 2016 <a href="#ref-5">[5]</a>, a inovação pode ser definida como a introdução de novidades ou aperfeiçoamentos no ambiente produtivo e social, resultando em novos produtos, serviços ou processos. Isso inclui a agregação de novas funcionalidades ou características a produtos, serviços ou processos já existentes, visando melhorias e ganhos efetivos de qualidade ou desempenho.</p>

<p style="text-indent: 50px; text-align: justify;">Nesse contexto, o projeto de desenvolvimento do fogão a indução inteligente representa uma inovação significativa. Ele se destaca por ser um aperfeiçoamento e adaptação de um equipamento já existente, ao agregar novas funcionalidades ao produto. Por exemplo, sua conectividade via bluetooth permite ao usuário controlar o fogão a partir de uma certa distância. Além disso, o sistema autônomo de energia, composto por baterias que armazenam a energia dos módulos fotovoltaicos, oferece uma solução inovadora e ambientalmente sustentável.</p>

<p style="text-indent: 50px; text-align: justify;">Essa inovação pode ser classificada como incremental, uma vez que adiciona novidades ao produto original, o fogão a indução. É importante ressaltar que essa solução ainda não está presente no mercado. Apesar da existência de várias opções de fogões a indução, nenhuma delas é abastecida por uma fonte de energia renovável e nenhuma oferece a mesma conectividade e funcionalidades encontradas no fogão a indução inteligente que será desenvolvido na disciplina de Projeto Integrador 2.</p>


## Objetivo Geral do Projeto

<p style="text-indent: 50px; text-align: justify;">Desenvolver e implementar um fogão inteligente de indução com uma única boca, alimentado por um sistema solar off-grid, com o objetivo de fornecer uma alternativa inovadora para o preparo de alimentos ao ar livre ou em residências. Os objetivos gerais do projeto são:</p>

- **Desenhar e Desenvolver uma Estrutura Robusta:**
  Desenvolver uma estrutura robusta e confiável para acomodar circuitos eletrônicos e dispositivos IoT, sem comprometer a sua capacidade de coletar dados precisos e tomar decisões corretas, além de protegê-los contra riscos inerentes ao funcionamento do produto, como superaquecimento e interferências eletromagnéticas, assegurando a segurança, estabilidade e funcionalidade do fogão como um todo.  

- **Proporcionar uma Experiência Culinária Inteligente:**
  O fogão permitirá aos usuários controlar variáveis do cozimento, como ligar, desligar, ajustar a potência e definir o tempo de funcionamento. Também permitirá monitoramento em tempo real de métricas como consumo de energia, tempo de uso e potência.

- **Garantir Autonomia Energética:**
  A integração de um sistema de captação de energia solar permitirá que o fogão funcione sem depender da rede elétrica. O armazenamento em baterias garantirá autonomia mesmo em ambientes externos sem acesso à eletricidade convencional.

## Objetivos Específicos do Projeto

#### **Estrutura Robusta:**
  - **Blindagem Eletromagnética:** Para eficiência do fogão e evitar interferências externas.
  - **Segurança:** Garantir uma estrutura estável e durável, utilizando materiais adequados para acomodar os circuitos eletrônicos e dispositivos de IoT do fogão, assegurando sua integridade, funcionalidade e a segurança do usuário.
  - **Sistema de Refrigeração:** Garantir o funcionamento adequado do fogão.
  - **Interface Moderna:** Display touch para operação do fogão.
  
#### **Experiência Culinária Inteligente:**
  - **Aplicativo Móvel:** Controle remoto via aplicativo para configuração do fogão.
  - **Controle Remoto:** Funções de ligar/desligar e ajuste de potência remotamente.
  - **Monitoramento em Tempo Real:** Acompanhamento de métricas como consumo de energia e tempo de uso.
  - **Configurações Personalizadas:** Salvar configurações de cozimento.
  - **Sensores e Interface:** Sensores de temperatura e presença de panela, com interface intuitiva.

#### **Autonomia Energética:**
  - **Alimentação Solar:** Utilização de energia solar para operação em locais remotos.
  - **Armazenamento em Baterias:** Baterias para garantir energia em períodos sem sol.
    
## Fogão Inteligente e os Objetivos de Desenvolvimento Sustentável (ODS)

### **Introdução aos Objetivos de Desenvolvimento Sustentável (ODS)**

<p style="text-indent: 50px; text-align: justify;">Os Objetivos de Desenvolvimento Sustentável (ODS) são uma iniciativa global liderada pela Organização das Nações Unidas (ONU), composta por 17 objetivos interconectados destinados a abordar os desafios mais prementes que o mundo enfrenta atualmente. Esses objetivos foram estabelecidos em 2015 como parte da Agenda 2030 para o Desenvolvimento Sustentável, com o propósito de guiar esforços coletivos em direção a um futuro mais sustentável e inclusivo para todos.</p>

### **O Fogão Inteligente e sua Relevância para os ODS**

<p style="text-indent: 50px; text-align: justify;">O fogão inteligente é uma inovação que se alinha a vários Objetivos de Desenvolvimento Sustentável, principalmente aqueles relacionados à energia limpa e acessível, consumo e produção responsáveis, e ação contra a mudança global do clima.</p>

#### Energia Limpa e Acessível (ODS 7)

<p style="text-indent: 50px; text-align: justify;">O fogão por indução, alimentado por painéis solares, representa uma fonte de energia limpa e acessível. Ao utilizar energia solar para seu funcionamento, contribui para a redução da dependência de combustíveis fósseis e emissões de gases de efeito estufa, promovendo assim um ambiente mais sustentável.</p>

#### Consumo e Produção Responsáveis (ODS 12)

<p style="text-indent: 50px; text-align: justify;">Por ser um fogão por indução, o fogão inteligente é mais eficiente em termos energéticos do que os fogões tradicionais que utilizam combustíveis como gás ou lenha. Isso significa que ele consome menos energia para realizar o mesmo trabalho de cocção, o que se traduz em um consumo mais responsável de recursos energéticos e uma pegada de carbono reduzida.</p>

#### Ação Contra a Mudança Global do Clima (ODS 13)

<p style="text-indent: 50px; text-align: justify;">A utilização de energia solar para alimentar o fogão inteligente contribui diretamente para a mitigação da mudança climática. Ao reduzir a emissão de gases de efeito estufa associados à queima de combustíveis fósseis na produção de energia, o fogão inteligente ajuda a limitar o aquecimento global e seus impactos adversos sobre o meio ambiente e as comunidades.</p>

### **Conclusão**

<p style="text-indent: 50px; text-align: justify;">O fogão inteligente, alimentado por energia solar e controlado por meio de um aplicativo, representa uma inovação que contribui para diversos Objetivos de Desenvolvimento Sustentável, especialmente aqueles relacionados à energia limpa, consumo responsável e ação climática. Ao adotar tecnologias como essa, onde as necessidades das gerações presentes são atendidas sem comprometer a capacidade das gerações futuras de atenderem às suas próprias necessidades.</p>

### Referências

<div id="ref-1"></div>

>[1] *ABNT. ABNT NBR NM 60335-1:2010 Segurança de aparelhos eletrodomésticos e similares Parte 1: Requisitos gerais (IEC 60335-1:2006 - edição 4.2, MOD). 2010. Disponível em: <https://www.abntcatalogo.com.br/pnm.aspx?Q=ZGdGTDJTZEhRNUZ1Tmd1a3drdW9oaDk0NkU4MTdBcnQwNWxNQWRKRXlEZz0=>. Acesso em: 11 de abril de 2024.*

<div id="ref-2"></div>

>[2] *ABNT NBR 5410:2004 Versão Corrigida:2008 - Instalações elétricas de baixa tensão. Disponível em: <https://www.abntcatalogo.com.br/pnm.aspx?Q=bWl2M29BSEtyWlVLY2wwL3VQNStQYUxVODhBS25kay9UaU5rNHo2Qmp1MD0=>. Acesso em: 31 de maio de 2024.*

<div id="ref-3"></div>

>[3] *ABNT. ISO/IEC/IEEE 12207-2:2020 Engenharia de sistemas e software — Processos de ciclo de vida de software — Parte 2: Relação e mapeamento entre ISO/IEC/IEEE 12207:2017 e ISO/IEC 12207:2008. 2020. Disponível em: <https://www.abntcatalogo.com.br/pnm.aspx?Q=U29aeUVCQVg3UUUwU1JJb0hod2hMU0QzTWxxTG92eDJ1N1Y5MmJGRmJhUT0=>. Acesso em: 11 de abril de 2024.*

<div id="ref-4"></div>

>[4] *Site dos Eletrodomésticos. Melhores Cooktops de Indução 1 boca em 2024. Disponível em: <https://www.abntcatalogo.com.br/pnm.aspx?Q=YlZuSWhOUHJocWpDZU9tYlZlTm1OdEdqaVhoekdzMHorTXhzS3hqV0hMND0=>. Acesso em: 17 de abril de 2024.*

<div id="ref-5"></div>

>[5] *BRASIL. LEI Nº 13.243, de 11 de janeiro de 2016. Dispõe sobre estímulos ao desenvolvimento científico, à pesquisa, à capacitação científica e tecnológica e à inovação. Brasília, DF: Diário Oficial da União, 2016. Disponível em: <https://www.planalto.gov.br/ccivil_03/_Ato2015-2018/2016/Lei/L13243.htm>. Acesso em: 17 de abril de 2024.*