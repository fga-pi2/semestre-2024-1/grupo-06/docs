# **Arquitetura do Software**

<p style="text-indent: 50px; text-align: justify;">A arquitetura de software escolhida para o projeto consiste em um Aplicativo Web Progressivo (Progressive Web App ou PWA em inglês) se comunicando com a placa ESP32, responsável pelo controle do fogão, via Bluetooth. O PWA foi escolhido devido à sua capacidade de oferecer uma experiência semelhante a um aplicativo nativo, mas com a conveniência de uma aplicação web. Isso permite que os usuários desfrutem de recursos como notificações push, funcionamento offline e outros benefícios normalmente associados a aplicativos nativos. Como resultado, podemos fornecer uma experiência consistente em diferentes plataformas, simplificando o desenvolvimento e aumentando a acessibilidade para nossos usuários. O PWA pode ser baixado em qualquer dispositivo móvel independente do sistema operacional, desde que tenham acesso a um navegador que suporte PWA.</p>

<p style="text-indent: 50px; text-align: justify;">O PWA será elaborado usando o modelo MVC, que, segundo Suharjito e Sunardi, divide uma aplicação em três partes: Model (responsável pela lógica do sistema), View (interface do sistema com o usuário) e Controller (responsável pelas alterações que aparecem na View). Esse padrão será adotado devido à divisão de responsabilidades entre diferentes camadas da aplicação, facilitando a compreensão e manutenção do código.</p>

<p style="text-indent: 50px; text-align: justify;">A opção pela comunicação via Bluetooth foi feita devido à sua capacidade de permitir que o usuário utilize as funcionalidades do software sem depender de uma conexão Wi-Fi.</p>

<p style="text-align:center;">
  <img src="https://gitlab.com/fga-pi2/semestre-2024-1/grupo-06/docs/-/raw/main/docs/assets/images/diagrama_arquitetura_software.png" alt="Diagrama da Arquitetura de Software" style="width:100%;">
  <div style="text-align:center;">
    Figura 01: Arquitetura do Software. Fonte: Autoria Própria, 2024
  </div>
</p>

# Tecnologias

<p style="text-indent: 50px; text-align: justify;">Ao selecionar as tecnologias para este projeto, levamos em consideração diversos aspectos críticos. Em primeiro lugar, revisamos o nível de competência e a experiência da equipe. Em seguida, examinamos a disponibilidade de suporte e recursos oferecidos pela comunidade de desenvolvedores, além de verificar se cada tecnologia era adequada para resolver o problema. Com base nessa avaliação, decidimos usar as seguintes tecnologias:</p>

## JavaScript e React

<p style="text-indent: 50px; text-align: justify;">O JavaScript é uma das linguagens de programação mais populares para desenvolvimento web. Sua flexibilidade e amplo uso fazem dele uma excelente escolha para criar experiências interativas em uma variedade de plataformas. O JavaScript pode ser usado para desenvolvimento de frontend e backend, suportando desde simples páginas web até complexos sistemas de software.</p>

<p style="text-indent: 50px; text-align: justify;">O React é uma biblioteca JavaScript projetada para criar interfaces de usuário. Ele é amplamente utilizado para desenvolvimento de páginas web e também para a criação de aplicativos móveis híbridos, que são construídos com tecnologias web, mas rodam como aplicativos nativos em dispositivos móveis. Neste projeto, o React será usado para construir um Aplicativo Web Progressivo (PWA), que combina a facilidade de uma aplicação web com recursos de aplicativos nativos, como notificações push ou conexão bluetooth, e sua natureza híbrida permite que funcionem em diferentes sistemas operacionais sem ajustes ou compilações adicionais.</p>

### Motivação:

<p style="text-indent: 50px; text-align: justify;">Escolhemos JavaScript e React para este projeto devido à sua popularidade, flexibilidade e capacidade de criar experiências interativas.</p>

<p style="text-indent: 50px; text-align: justify;">JavaScript é uma escolha versátil, sendo usado tanto no frontend quanto no backend, possui diversas bibliotecas as quais podem ser usadas além de uma vasta documentação.</p>

<p style="text-indent: 50px; text-align: justify;">O React, por sua vez, é ideal para construir interfaces de usuário eficientes e escaláveis, combinadas com a configuração de um Progressive Web Application (PWA), supre todas as necessidades do projeto.</p>

## C++ e Arduino Framework

<p style="text-indent: 50px; text-align: justify;">O Arduino Framework é uma plataforma amplamente utilizada para a programação de dispositivos embarcados, como a placa ESP32. Baseada em C++, a plataforma oferece uma variedade de bibliotecas prontas para uso, o que facilita a integração com sensores e outros componentes de hardware. No contexto deste projeto, a escolha do C++ e do Arduino Framework foi motivada por sua flexibilidade, bem como pela familiaridade da equipe com a arquitetura e pelo nível de experiência com essas tecnologias.</p>

<p style="text-indent: 50px; text-align: justify;">O uso dessas tecnologias nos permite configurar a placa ESP32 para responder a comandos via Bluetooth e executar operações necessárias para o controle de um fogão. Além disso, o Arduino Framework e o C++ são utilizados para criar o firmware do display, que também possui uma ESP32 integrada. Isso permite o desenvolvimento de uma interface que reage a toques, bem como a comunicação entre o display e a ESP32. A comunidade ativa do Arduino Framework oferece um amplo suporte e uma rica gama de recursos, facilitando a resolução de problemas e a implementação de novas funcionalidades. Com o C++ e o Arduino Framework, é possível criar um sistema estável, adaptável e capaz de atender aos requisitos do projeto.</p>

### Motivação

<p style="text-indent: 50px; text-align: justify;">A escolha do Arduino Framework e C++ se deve à flexibilidade, ampla disponibilidade de bibliotecas e à experiência prévia da equipe. Essas tecnologias nos permitem configurar a ESP32 para responder a comandos via Bluetooth, controlar o fogão e estabelecer a conexão entre a ESP e o celular do usuário.</p>

# Diagrama de Sequência

<p style="text-indent: 50px; text-align: justify;">Guedes <a href="#ref-1">[1]</a> define um diagrama de sequência como "diagrama comportamental que preocupa-se com a ordem temporal em que as mensagens são trocadas entre os objetos envolvidos em um determinado processo". O presente trabalho optou por adicionar esse diagrama como parte da documentação do projeto para declarar as ações que um usuário poderá realizar no produto desenvolvido e os objetos envolvidos nas ações. As duas primeiras partes do diagrama estão representadas nas figuras abaixo.</p>

<div style="text-align:center;">
  <img src="../../assets/images/DiagSeq1.png" width="594px">
  <div style="text-align:center;">Figura 02: Primeira Parte do Diagrama de Sequência</div>
</div>

<div style="text-align:center;">
  <img src="../../assets/images/DiagSeq2.png" >
  <div style="text-align:center;">Figura 03: Segunda Parte do Diagrama de Sequência. Fonte: Autoria Própria, 2024</div>
</div>

<p style="text-indent: 50px; text-align: justify;">Essas primeiras partes do diagrama mostram as operações que um usuário pode realizar no aplicativo. É importante ressaltar que a maioria dessas ações exige que o aparelho do usuário consiga se comunicar com a placa ESP32 do fogão via Bluetooth, portanto o usuário precisa estar ao alcance do sinal Bluetooth da ESP32 para realizar as ações especificadas no diagrama.
A última parte do diagrama de sequência está representada na figura abaixo. A imagem especifica as operações que o cliente pode realizar no fogão ao interagir direto com o display do fogão, sem precisar usar um aparelho celular. Uma observação é que garantimos que todas as funções básicas de um fogão possam ser usadas mesmo que o cliente esteja sem um celular ou não consiga se conectar ao aparelho pelo Bluetooth.</p>

<div style="text-align:center;">
  <img src="../../assets/images/DiagSeq3.png" width="594px">
  <div style="text-align:center;">Figura 04: Terceira Parte do Diagrama de Sequência. Fonte: Autoria Própria, 2024</div>
</div>

# Diagrama de Classes

<p style="text-indent: 50px; text-align: justify;">O diagrama de classes é uma representação visual das classes em um sistema, juntamente com seus atributos, métodos e relacionamentos. Ele mostra como as classes estão organizadas e como elas interagem umas com as outras.</p>

<div style="text-align:center;">
  <img src="../../assets/images/diagrama-classes-software.png">
  <div style="text-align:center;">Figura 05: Diagrama de Classes. Fonte: Autoria Própria, 2024</div>
</div>

# Diagrama de Pacotes

<p style="text-indent: 50px; text-align: justify;">O diagrama de pacotes é usado para organizar e mostrar a estrutura geral de um sistema de software em níveis de pacotes. Ele ajuda a visualizar a arquitetura de alto nível do sistema, mostrando como os diferentes componentes estão agrupados e como eles se relacionam.</p>

<div style="text-align:center;">
  <img src="../../assets/images/diagrama-pacotes-software.png">
  <div style="text-align:center;">Figura 06: Diagrama de Pacotes. Fonte: Autoria Própria, 2024</div>
</div>

# Perfis de Usuário do Produto

### Persona 1: O Entusiasta de Camping

- **Nome:** Alex Rivera
- **Idade:** 32 anos
- **Profissão:** Consultor Ambiental
- **Familiaridade com Tecnologia:** Moderada

**Objetivos:**

- Acesso a soluções práticas de cozinha para acampamentos ou caminhadas.
- Operar dispositivos de cozinha sem depender de eletricidade da rede.

**Desafios:**

- Frequentemente em locais remotos sem acesso confiável à energia.
- Necessidade de dispositivos duráveis e aptos para uso externo.

**Utilização do Produto:**

- **Configurações do Fogão:** Utiliza o fogão para preparar alimentos rapidamente, ajustando a potência conforme necessário.
- **Uso do PWA:** Se o celular estiver disponível, inicia o aquecimento através do aplicativo enquanto monta o acampamento, monitorando a temperatura e o tempo de cozimento à distância.
- **Uso do display:** Na ausência do celular, o display permite configurar tempo e potência de cocção para alimentos pré-cadastrados. Assim, é possível ajustar as configurações rapidamente e se afastar para continuar montando o acampamento ou realizar outras atividades.

### Persona 2: A Chef Doméstica

- **Nome:** Sarah Chen
- **Idade:** 28 anos
- **Profissão:** Desenvolvedora de Software
- **Familiaridade com Tecnologia:** Alta

**Objetivos:**

- Incorporar tecnologia inteligente na rotina doméstica.
- Experimentar técnicas de cozinha que exigem controle preciso de tempo e temperatura.

**Desafios:**

- Busca por interfaces simples e informativas.
- Procura por produtos que facilitam técnicas culinárias avançadas.

**Utilização do Produto:**

- **Experimentação Culinária:** Explora novas receitas que exigem condições específicas de cozimento, como sous-vide.
- **Interação com o PWA:** Acessa receitas e configurações pré-definidas, fazendo ajustes em tempo real e recebendo alertas sobre o preparo.

### Persona 3: A Pessoa Simples

- **Nome:** David Kim
- **Idade:** 45 anos
- **Profissão:** Professor Escolar
- **Familiaridade com Tecnologia:** Baixa a Moderada

**Objetivos:**

- Reduzir o consumo de energia.
- Manter simplicidade no ambiente de cozinha.

**Desafios:**

- Pouco interesse em automação residencial.
- Preferência por soluções simples e seguras.

**Utilização do Produto:**

- **Eficiência Energética:** Valoriza o uso de energia solar no fogão, ajudando a reduzir a pegada de carbono.
- **Funcionalidades de Segurança:** Interage com o dispositivo para selecionar temperatura e tempo para receitas simples.

### Persona 4: O Morador Urbano com Espaço Limitado

- **Nome:** Mia Thompson
- **Idade:** 24 anos
- **Profissão:** Estudante de Pós-Graduação
- **Familiaridade com Tecnologia:** Moderada

**Objetivos:**

- Encontrar aparelhos compactos e eficientes para espaços reduzidos.
- Gerenciar custos de vida com tecnologias energéticamente eficientes.

**Desafios:**

- Espaço de cozinha limitado.
- Necessidade de preparo rápido e fácil de refeições devido a uma agenda cheia.

**Utilização do Produto:**

- **Maximização do Espaço:** Opta pelo fogão de indução inteligente devido ao seu design compacto, facilitando a integração em espaços pequenos.
- **Utilização do PWA:** Programa o tempo de cozimento através do aplicativo para otimizar o preparo de alimentos.

#Protótipo de Interface

## Paleta de cores

</br>
<div style="text-align:center;">
  <img src="../../assets/images/paletaCores.png">
  <div style="text-align:center;">Figura 07: Paleta de Cores. Fonte: Autoria Própria, 2024</div>
</div>
</br>

## Tipografia

<p style="text-indent: 50px;text-align: justify;">
Escolhemos a fonte <strong>Poppins</strong> por seu design moderno e geométrico, que oferece excelente legibilidade e um toque contemporâneo, alinhando-se perfeitamente com a estética limpa e profissional desejada para o sistema.
</p>

## Protótipo de representação

### Telas Inicial, Tela Ligar, Tela principal

</br>
<div style="text-align:center;">
  <img src="../../assets/images/prototipo1.png">
  <div style="text-align:center;">Figura 08: Páginas inicial, ligar e principal. Fonte: Autoria Própria, 2024</div>
</div>
</br>

### Telas rotina e histórico de cozimento

</br>
<div style="text-align:center;">
  <img src="../../assets/images/prototipo2.png">
  <div style="text-align:center;">Figura 09: Páginas de rotina e histórico de cozimento. Fonte: Autoria Própria, 2024</div>
</div>
</br>

### Telas rotina e histórico de cozimento

</br>
<div style="text-align:center;">
  <img src="../../assets/images/prototipo3.png">
  <div style="text-align:center;">Figura 10: Notificação de funcionamento do fogão. Fonte: Autoria Própria, 2024</div>
</div>
</br>

## Referências

<div id="ref-1"></div>

> [1] _GUEDES, Gilleanes T. A. UML 2: uma abordagem prática. 2. ed. São Paulo: Novatec Editora, 2011_

<div id="ref-2"></div>

> [2] _SUHARJITO; SUNARDI, Andri. MVC Architecture: A Comparative Study Between Laravel Framework and Slim Framework in Freelancer Project Monitoring System Web Based. In: Conferência Internacional de Ciência da Computação e INteligência Computacional, 4. 2019, Yogyakarta. Disponível em: https://doi.org/10.1016/j.procs.2019.08.150. Acesso em: 24 abr. 2024_

<div id="ref-3"></div>

> [3] _What are Progressive Web Apps?. Web.Dev. Disponível em: <https://web.dev/articles/what-are-pwas>. Acesso em: 24 abr. 2024_
