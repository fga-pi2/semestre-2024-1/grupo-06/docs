# Arquitetura Geral do Projeto de um Fogão Indutivo Controlado Remotamente

<p style="text-indent: 50px; text-align: justify;">
O documento de arquitetura geral descreve os componentes necessários para o desenvolvimento do projeto fogão inteligente, descritos de uma forma superficial, contendo os componentes principais de hardware, software necessário, esquema de comunicação entre o aplicativo e fogão, questões relacionadas a segurança e fonte de alimentação.
</p>

## 1. Hardware

### Fogão Indutivo:
- **Bobina de Indução**: Gera o campo eletromagnético para aquecer o utensílio.

- **Sistema de Controle de Potência**: Circuito que regula a energia para a bobina, utilizando IGBTs ou MOSFETs.

### Microcontrolador:
- **Microcontrolador Principal**: O cérebro do sistema, será utilizada a ESP32.

- **Sensores**: Termômetros e possivelmente sensores de presença de panela.

### Interface de Usuário:
- **Display e Botões**: Para interação local.
- **Indicadores LED**: Mostram o status operacional.

## 2. Software

- **Firmware do Microcontrolador**: Controla as funcionalidades, lê sensores e comanda o sistema de potência.
- **Algoritmos de Controle**: Regulam a energia com base nas entradas do usuário e feedback dos sensores.

## 3. Comunicação

- **Módulo Bluetooth**: Para conexão com dispositivos móveis.
- **Aplicativo Móvel ou Web**: Permite controle remoto, ajuste de temperatura, monitoramento e notificações.

## 4. Segurança e Confiabilidade

- **Proteções de Hardware**: Contra sobrecorrente, sobretensão e superaquecimento.
- **Software de Segurança**: Inclui verificações de erro, gerenciamento de falhas e segurança operacional.

## 5. Fonte de Alimentação

- **Fonte de Alimentação Adequada**: Fornece energia elétrica para os componentes eletrônicos e para a indução.

## 6. Implementação

- **Integração entre os componentes**: Testes rigorosos de segurança elétrica e funcional envolvendo cenários reais de uso e situações extremas para verificar a robustez e a confiabilidade do sistema. 
- **Integração entre Hardware e software**: A interface local deve incluir displays claros e botões responsivos, enquanto a interface remota, acessível via aplicativo móvel. 