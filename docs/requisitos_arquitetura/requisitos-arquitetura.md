# **Concepção e Detalhamento da Solução**

<!-- A seção 02 "Concepção e detalhamento da solução" se refere à fase 02 do
ciclo de vida do projeto e tem por objetivo apresentar a arquitetura da
solução/sistema para o problema. -->

## Requisitos Gerais

<!-- O time está encarregado de coletar os requisitos -- o processo de
definição e documentação do projeto e das características e funções do
produto necessárias para satisfazer todas as necessidades e expectativas
das partes interessadas. Estes requisitos, colocados numa relação de
requisitos, devem incluir:

-   requisitos de funcionamento e de desempenho;

-   outros requisitos essenciais para projeto e desenvolvimento.

-   Resultados da análise crítica dessas entradas quanto a suficiência,
    completeza e ausência de ambiguidades ou conflitos entre si. -->

Tabela 1: Requisitos Funcionais Gerais

| ID  | Descrição                                                                                                                                |
| --- | ---------------------------------------------------------------------------------------------------------------------------------------- |
| 01  | O sistema deve permitir o acionamento ou desligamento manual do fogão por um usuário                                                     |
| 02  | O sistema deve permitir a um usuário a definição manual da temperatura da boca do fogão                                                  |
| 03  | O sistema deve oferecer ao usuário uma visualização da temperatura e do tempo de funcionamento da boca do fogão                          |
| 04  | O sistema deve permitir o acionamento ou desligamento do fogão pelo celular do usuário                                                   |
| 05  | O sistema deve permitir a definição da temperatura da boca do fogão pelo celular do usuário                                              |
| 06  | O sistema deve permitir a temporização do funcionamento da boca do fogão pelo celular do usuário                                         |
| 07  | O sistema deve permitir a visualização do estado de funcionamento e da temperatura da boca de fogão pelo celular do usuário              |
| 08  | O sistema deve permitir a visualização do tempo, da temperatura e do gasto de energia necessários para cozinhar uma receita pré-definida |
| 09  | O sistema permitirá uma visualização de métricas de uso                                                                                  |
| 10  | O sistema deve enviar uma notificação aos celulares dos usuários quando uma boca ficar muito tempo ligada                                |
| 11  | O sistema deve desligar a boca do fogão automaticamente quando o tempo pré-determinado acabar.                                           |

Tabela 2: Requisitos Não-Funcionais Gerais 

| ID  | Descrição                                                                       |
| --- | ------------------------------------------------------------------------------- |
| 01  | O fogão deve ser alimentado por um sistema de energia off-grid                  |
| 02  | O fogão deve ter um sistema de segurança com resfriamento por convecção forçada |
| 03  | O fogão deve possuir somente de uma boca                                        |
| 04  | O fogão deve possuir uma interface de interação do tipo touch screen            |


## Requisitos de Estrutura
Tabela 3: Requisitos Funcionais de Estruturas

| ID  | Requisito                       | Descrição                                                                                                                                                                                                                                                                                                                                                   |
| --- | ------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 01  | Resistência mecânica            | O fogão deverá ser resistente, pois teremos esforços com o peso sobre o mesmo.                                                                                                                                                                                                                                                                               |
| 02  | Estabilidade química            | O fogão será utilizado em cozinhas e ao ar livre, portanto deve ser ter materiais que possibilitem o seu uso sem danifica-lo.                                                                                                                                                                                                                               |
| 03  | Tenacidade                      | A estrutura deve ser capaz de suportar pequenos impactos e se deformar plasticamente antes de fraturar.                                                                                                                                                                                                                                                     |
| 04  | Impermeabilidade                | A estrutura deverá ter um nível de impermeabilidade evitando a entrada de água durante sua limpeza.                                                                                                                                                                                                                                                         |
| 05  | Dureza                          | A dureza dos materiais da superfície de trabalho do fogão é importante para resistir ao desgaste causado pelo atrito com as panelas e utensílios de cozinha. Superfícies duras também são mais fáceis de limpar e menos propensas a arranhões.                                                                                                              |
| 06  | Condutividade térmica           | Embora não seja uma propriedade estritamente mecânica, a condutividade térmica dos materiais utilizados no fogão de indução é crucial para garantir uma distribuição uniforme do calor e uma resposta rápida às mudanças de temperatura. Materiais com alta condutividade térmica, como o cobre ou o alumínio, são comumente usados nas bobinas de indução. |
| 07  | Estabilidade térmica            | Os materiais do fogão devem ser capazes de suportar variações extremas de temperatura sem deformação permanente ou falha estrutural. Isso é especialmente importante para as bobinas de indução, que estão sujeitas a ciclos de aquecimento e resfriamento frequentes.                                                                                      |
| 08  | Compatibilidade eletromagnética | Embora não seja uma propriedade mecânica no sentido tradicional, a compatibilidade eletromagnética dos materiais é fundamental para garantir que eles não interfiram no funcionamento correto do sistema de indução.                                                                                                                                        |


Tabela 4: Requisitos não Funcionais de Estruturas

| ID  | Requisito            | Descrição                                                                           |
| --- | -------------------- | ----------------------------------------------------------------------------------- |
| 01  | Segurança do usuário | A utilização do fogão não deve fornecer risco a integridade física do usuário.      |
| 02  | Manutenção           | O fogão deve permitir fácil acesso caso seja necessário realizar alguma manutenção. |
| 03  | Capacidade           | Peso máximo que a estrutura suporta.                                                |
| 04  | Portátil             | Estrutura leve e com apoios para segura-lá.                                         |

## Requisitos de Eletrônica

Tabela 5: Requisitos Funcionais de Eletrônica

| ID  | Requisito                            | Descrição                                                                                                                                      |
| --- | ------------------------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------- |
| 01  | Controle de Temperatura              | O sistema deve ajustar a temperatura automaticamente com base no input do termistor NTC (faixa de temperatura em ºC: 80, 100, 120, 160, 180, 200, 240, 270).                                                       |
| 02  | Controle de Temperatura pelo usuário | Deve haver a possibilidade de configuração manual da temperatura através da tela touch ou do aplicativo móvel.                                 |
| 03  | Conectividade                        | O fogão deve ser capaz de se conectar a um dispositivo via bluetooth usando a ESP32 para permitir o controle remoto via aplicativo.  A conexão bluetooth deve possuir um requisito de segurança como PIN, códigos ou chave de acesso.          |
| 04  | Comandos                             | Deve suportar atualizações e comandos em tempo real.                                                                                           |
| 05  | Controle de Potência                 | Utilizar um IGBT (funciona como um interruptor eletrônico, acionado conforme modulação da largura dos pulsos de corrente) para gerenciar a potência no circuito ressonante, proporcionando o controle eficiente da energia necessária para o cozimento. |
| 06  | Segurança                            | Implementar medidas de segurança como desligamento automático em caso de superaquecimento ou ausência de panela, por meio de sensores de temperatura e indução.                               |
| 07  | Feedback                             | O sistema deve fornecer feedback visual, com imagens de alertas através do display, e sonoro, por meio de um buzzer, sobre o status de operação.                                                                   |
| 08  | Segurança Harware                    | Incluir proteções contra curtos-circuitos e sobrecargas.                                                                                       |

Tabela 6: Requisitos Não Funcionais de Eletrônica

| ID  | Requisito             | Descrição                                                                                        |
| --- | --------------------- | ------------------------------------------------------------------------------------------------ |
| 01  | Confiabilidade        | O sistema deve ser robusto e capaz de operar em condições variadas de ambiente (entre 10 e 40°C e entre 10 e 85% de umidade).          |
| 02  | Eficiência Energética | O fogão deve operar de forma eficiente em termos de energia para reduzir os custos operacionais. |
| 03  | Durabilidade          | O design deve contemplar a facilidade de manutenção e possíveis substituições de peças. A partir do uso de componentes modulares sendo projetado para elementos que são facilmente removidos e substituídos. Isso inclui módulos para a placa de indução, controle de potência, ventiladores de resfriamento, e outros componentes principais. Será utilizado também conexões de Plug-and-Play facilitando a desconexão e reconexão sem a necessidade de soldagem ou ferramentas especiais.          |


## Requisitos de Energia

Tabela 7: Requisitos Funcionais de Energia

| ID  | Requisito                    | Descrição                                                                                                                                                                                                                      |
| --- | ---------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| 01  | Fornecimento de Energia      | O sistema deve ser capaz de capturar energia solar por meio dos painéis fotovoltaicos.                                                                                                                                         |
| 02  | Armazenamento                | O sistema deve possuir baterias para armazenar a energia produzida a partir dos painéis, garantindo a energia necessária para que o dispositivo funcione adequadamente durante a noite ou em condições de baixa radiação solar |
| 03  | Alimentação dos Dispositivos | O sistema deve fornecer energia em corrente alternada para os dispositivos do fogão.                                                                                                                                           |

Tabela 8: Requisitos Não-Funcionais de Energia

| ID  | Requisito    | Descrição                                                                                                                                                                         |
| --- | ------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 01  | Durabilidade | Todos os componentes elétricos, controladores de carga e bateria, devem ser projetados para suportar condições climáticas adversas, como chuva, vento e variações de temperatura. |
| 02  | Segurança    | O sistema elétrico deve ser projetado e instalado de forma a garantir a segurança dos operadores, entregadores e usuários, seguindo todas as normas de segurança aplicáveis.      |

## Requisitos de Software

Tabela 9: Requisitos Funcionais de Software

| ID  | Requisito                                                 | Descrição                                                                                                                             |
| --- | --------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------- |
| 01  | Conectar a Fogão                                          | O usuário deve ser capaz de se conectar ao fogão                                                                                      |
| 02  | Temporização de Cozimento                                 | O usuário deve ser capaz de pré-determinar o tempo de funcionamento e a potência de uma boca do fogão                                 |
| 03  | Visualização do Estado de Funcionamento                   | O usuário deve ser capaz de visualizar o estado de funcionamento de uma boca pertencente ao fogão                                     |
| 04  | Notificação de Perigo                                     | O usuário deve poder receber uma notificação quando uma boca estiver muito tempo ligada                                               |
| 05  | Visualização de Detalhes de Cozimento                     | O usuário deve ser capaz de visualizar dados históricos do uso de uma boca                                                            |
| 06  | Gerenciar Rotina de Cozimento                             | O usuário deve ser capaz de gerenciar uma rotina de cozimento de uma comida                                                           |
| 07  | Temporização de Cozimento a Partir de Rotina de Cozimento | O usuário deve ser capaz de determinar o tempo de funcionamento e a potência de uma boca do fogão a partir de uma rotina de cozimento |


Tabela 10: Requisitos Não-Funcionais de Software

| ID  | Requisito                | Descrição                                                                          |
| --- | ------------------------ | ---------------------------------------------------------------------------------- |
| 01  | Compatibilidade          | O aplicativo deve ser acessível pelos sistemas operacionais mobile Android e o iOS |
| 02  | Protocolo de Comunicação | O fogão deve se comunicar com o dispositivo móvel do usuário por Bluetooth         |
| 03  | Implementação Aplicativo | O aplicativo deve ser desenvolvido com o framework ReactJS e Javascript            |
| 04  | Implementação Firmware   | O firmware deve ser desenvolvido com o Arduino Framework e C++                     |


## Backlog

<p style="text-indent: 50px; text-align: justify;">Segundo Sommerville (2013)<a href="#ref-1">[1]</a>, o backlog para um time ágil é o conjunto de tarefas que o time precisa realizar para desenvolver o produto. Nesta subseção, foram agrupadas as tarefas advindas dos requisitos que a equipe se propôs a desenvolver. Ademais, como a equipe está seguindo uma metodologia ágil, é importante definir um backlog para orientação e organização do time.</p>

<p style="text-indent: 50px; text-align: justify;">Ademais, os requisitos foram expressos como na metodologia ágil de desenvolvimento de software Extreme Programming (XP), que segundo Sommervile diz: "Em XP, os requisitos do usuário são expressos como cenários ou estórias, e o usuário os prioriza para o desenvolvimento. A equipe de desenvolvimento avalia cada cenário e divide-o em tarefas.". Portanto, os requisitos do projeto foram expressos na forma de estórias de usuário.<a href="#ref-1">[2]</a></p>

<p style="text-indent: 50px; text-align: justify;">Para a priorização foi utilizada a técnica MoSCoW, que segundo o Guia BABOK<sup>TM</sup> (2011) é uma técnica de priorização de requisitos que classifica os requisitos em quatro categorias: Must (Deve), Should (Deveria), Could (Poderia) e Won't (Não irá). Segundo o guia, a definição de cada categoria é a seguinte:<a href="#ref-2">[3]</a></p>

> "Deve: Descreve um requisito que deve ser atendido na solução final para que a mesma seja considerada um sucesso. (...) Deveria: Representa um item de alta prioridade que deveria ser incluído na solução, caso possível. Trata-se frequentemente de um requisito crítico que pode ser atendido de outras formas se for estritamente necessário. (...) Poderia: Descreve um requisito que é considerado desejável, mas não necessário, e que será incluído caso o tempo e os recursos permitam. (...) Não irá: Representa um requisito que as partes interessadas concordaram em não implementar em uma determinada entrega, mas que pode ser considerado no futuro." <a href="#ref-2">[3]</a>.

<p style="text-indent: 50px; text-align: justify;">Usando os conceitos de estórias de usuário e a técnica MoSCoW, o backlog foi elaborado e priorizado. O resultado final está representado abaixo.</p>

Tabela 11: Backlog da Aplicação

<table style="border-collapse: collapse;">
<tr>
<th style="border: 1px solid black;">Épico</th>
<th style="border: 1px solid black;">Feature</th>
<th style="border: 1px solid black;">Estória de Usuário</th>
<th style="border: 1px solid black;">Categoria</th>
<th style="border: 1px solid black;">Descrição</th>
</tr>
<tr>
<td style="border: 1px solid black;" td style="border: 1px solid black;" rowspan="8">E01 - Fogão</td>
<td style="border: 1px solid black;" rowspan="2">F01 - Controle do Fogão</td>
<td style="border: 1px solid black;">US01</td>
<td style="border: 1px solid black;">Must</td>
<td style="border: 1px solid black;">O usuário deve ser capaz de se conectar ao fogão</td>
</tr>
<tr>
<td style="border: 1px solid black;">US02</td>
<td style="border: 1px solid black;">Must</td>
<td style="border: 1px solid black;"> O usuário deve ser capaz de pré-determinar o tempo de funcionamento e a potência de uma boca do fogão</td>
</tr>
<tr>
<td style="border: 1px solid black;" rowspan="2">F02 - Monitoramento do Fogão</td>
<td style="border: 1px solid black;">US03</td>
<td style="border: 1px solid black;">Must</td>
<td style="border: 1px solid black;">O usuário deve ser capaz de visualizar o estado de funcionamento de uma boca pertencente ao fogão</td>
</tr>
<tr>
<td style="border: 1px solid black;">US04</td>
<td style="border: 1px solid black;">Should</td>
<td style="border: 1px solid black;">O usuário deve poder receber uma notificação quando uma boca estiver muito tempo ligada </td>
</tr>
<tr>
<td style="border: 1px solid black;" rowspan="3">F03 - Gestão de Cozimento</td>
<td style="border: 1px solid black;">US05</td>
<td style="border: 1px solid black;">Should</td>
<td style="border: 1px solid black;">O usuário deve ser capaz de visualizar dados históricos do uso de uma boca</td>
</tr>
<tr>
<td  style="border: 1px solid black;">US06</td>
<td  style="border: 1px solid black;">Could</td>
<td  style="border: 1px solid black;">O usuário deve ser capaz de gerenciar uma rotina de cozimento de uma comida</td>
</tr>
<tr>
<td  style="border: 1px solid black;">US07</td>
<td  style="border: 1px solid black;">Could</td>
<td  style="border: 1px solid black;">O usuário deve ser capaz de determinar o tempo de funcionamento e a potência de uma boca do fogão a partir de uma rotina de cozimento</td>
</tr>
</table>

## Referências

<div id="ref-1"></div>

>[1] *SOMMERVILLE, I. Engenharia de software. 9.ed. São Paulo: Pearson Education do Brasil Ltda, 2013*

<div id="ref-2"></div>

>[2] *IIBA. Um guia para o Corpo de Conhecimento de Análise de Negócios™ (Guia BABOK®) Versão 2.0. Toronto, Canadá: International Institute of Business Analysis, 2011.*
<!-- 

# **Concepção e detalhamento da solução**

A seção 02 "Concepção e detalhamento da solução" se refere à fase 02 do
ciclo de vida do projeto e tem por objetivo apresentar a arquitetura da
solução/sistema para o problema.

# **Requisitos gerais**

O time está encarregado de coletar os requisitos -- o processo de
definição e documentação do projeto e das características e funções do
produto necessárias para satisfazer todas as necessidades e expectativas
das partes interessadas. Estes requisitos, colocados numa relação de
requisitos, devem incluir:

-   requisitos de funcionamento e de desempenho;

-   outros requisitos essenciais para projeto e desenvolvimento.

-   Resultados da análise crítica dessas entradas quanto a suficiência,
    completeza e ausência de ambiguidades ou conflitos entre si.

**Tabela 1: Requisitos funcionais para o produto**

| Requisito | Descrição | Observações |
| :-------: | :-------: | :---------: |
|     1     |           |             |
|     2     |           |             |

**Tabela 1: Requisitos não funcionais para o produto**

| Requisito | Descrição | Observações |
| :-------: | :-------: | :---------: |
|     1     |           |             |
|     2     |           |             |
 -->