
# **Arquitetura de Eletrônica**
<p style="text-indent: 50px; text-align: justify;">Para desenvolver a arquitetura eletrônica do fogão inteligente, vamos integrar ao circuito componentes cruciais que asseguram tanto a funcionalidade quanto a segurança operacional do aparelho. Esses elementos, destacam suas funções específicas no sistema de controle de módulos e na regulação da potência necessária para o funcionamento eficiente do produto.</p>

## Diagrama de Blocos de Aplicação do Sistema

<p style="text-indent: 50px; text-align: justify;">O diagrama a seguir destaca a composição dos principais componentes envolvidos no desenvolvimento do hardware do fogão inteligente.</p>

<div align="center">
  <img src="../../assets/images/New Diagrama 1.png">
  <h4>Figura 01: Diagrama de blocos de aplicação do sistema</h4>
</div>

- **Painel Solar e Inversor**: No sistema  de conversão de energia solar, o inversor desempenha um papel crucial, convertendo a energia de corrente contínua (DC) gerada pelos painéis solares em corrente alternada (AC).O processo de conversão utiliza uma técnica conhecida como modulação por largura de pulso (PWM), essencial para controlar a quantidade de energia transferida. O PWM ajusta a tensão e a corrente da saída AC ao simular uma onda senoidal. Ajustando a largura dos pulsos, o inversor pode gerenciar eficientemente a tensão de saída e fazer ajustes finos na forma de onda para que se assemelhe o mais próximo possível de uma onda senoidal perfeita.

- **EMI e Retificador**: A energia AC passa por um filtro de Interferência Eletromagnética (EMI) que limpa a energia de ruídos antes de chegar a um retificador, que converte a energia AC de volta para DC.

- **Buck Flyback**: Esta parte do circuito é responsável por converter a tensão DC em níveis diferentes, indicados como 18V, necessários para operar outras partes do sistema.

- **LDO 5V e Resonance Circuit**: LDO fornece uma tensão estável de 5V para componentes sensíveis à tensão. O circuito de ressonância enfatiza a presença do sistema de indução para operar o elemento de aquecimento.

- **Detecção de Corrente e Tensão**: Estes componentes são usados para monitorar a corrente e a tensão no sistema para garantir a segurança e a eficiência do dispositivo.

- **Microcontrolador (MCU)**: O cérebro do dispositivo, este microcontrolador gerencia as operações do sistema, processa os dados de detecção de corrente e tensão e  executa o controle do dispositivo.

- **IGBT e Transistor**: Estes componentes serão usados para controlar a potência elevada, como a necessária para o aquecimento num fogão. IGBT significa "Insulated Gate Bipolar Transistor", que é um tipo de transistor usado para comutar altas correntes de forma eficiente. 

- **UART e Touch Screem**:  UART é um protocolo de comunicação utilizado para a interface com dispositivos externos. No diagrama, indica uma comunicação com um Touch Screem, permitindo controle e monitoramento remoto através de uma aplicação móvel.

- **Ícone Bluetooth ao lado do Smartphone**: Representa a conectividade sem fio entre o microcontrolador e o smartphone, permitindo a transmissão de dados e comandos entre o dispositivo e o app no smartphone.

## Diagrama do Fluxo de Controle do Sistema de Indução

<p style="text-indent: 50px; text-align: justify;">O diagrama do fluxo de controle do sistema de indução (Figura 2) mostra um sistema de gerenciamento de falhas robusto que assegura a operação contínua do fogão de indução inteligente, garantindo ao mesmo tempo a segurança do usuário e a integridade do dispositivo.</p>

<div align="center">
  <img src="../../assets/images/Diagrama_2.png">
  <h4>Figura 02: Diagrama do Fluxo de Controle do Sistema de Indução</h4>
</div>

- **Iniciar:** Este bloco representa o ponto de partida do fluxo do sistema de indução. Ele indica o início do processo quando o usuário liga o fogão de indução.
- **Inicialização do Sistema:** Neste estágio, o sistema realiza todas as verificações iniciais e configurações necessárias para começar a funcionar corretamente. Isso pode incluir o estabelecimento de linhas de comunicação, a calibração de sensores e a verificação do status dos componentes críticos.
- **Monitoramento de Parâmetros Críticos:** Aqui, o sistema monitora continuamente vários parâmetros críticos como a tensão de alimentação, a corrente elétrica, a temperatura do IGBT (transistor chave para controle de potência) e a temperatura da superfície do cooktop. Essas medições são essenciais para garantir o funcionamento seguro e eficiente do fogão.
- **Decisão de Proteção:** Com base nos dados coletados, o sistema decide se alguma condição de proteção foi atingida. Se qualquer uma das leituras estiver fora dos limites seguros, o sistema passa para a próxima etapa de definição de um sinal de erro.
- **Sinalização de Erro e Armazenamento de Código:** Se o sistema identifica uma condição de falha, ele define um sinalizador em HIGH (alto) para indicar um erro e armazena um código de erro correspondente na memória do sistema. Isso pode acionar um alerta visual ou sonoro e ajudar na diagnóstico de problemas.
- **Interrupção de Proteção de Hardware do Comparador:** O sistema deverá ter uma interrupção específica de hardware configurada para responder imediatamente a condições críticas de proteção, como surtos de tensão ou corrente, protegendo os componentes eletrônicos contra danos. Isso é feito desligando o IGBT ou outras operações de proteção.
- **Controle da Função Principal do Fogão de Indução:** Se nenhuma condição de erro for detectada, o sistema procede ao controle normal da função de indução. Isso inclui ajustar a potência fornecida ao elemento de aquecimento, controlar o tempo e a potência de acordo com as seleções do usuário, e monitorar continuamente as condições operacionais. 

## Diagrama Fluxo de Controle e Interatividade em um Fogão de Indução Inteligente

<p style="text-indent: 50px; text-align: justify;">  O diagrama a seguir demonstra um sistema integrado para o fogão de indução inteligente.</p>

<div align="center">
  <img src="../../assets/images/New Diagrama 3.png">
  <h4>Figura 03: Diagrama Fluxo de Controle e Interatividade em um Fogão de Indução Inteligente</h4>
</div>

- **Microcontrolador ESP32**: O coração do sistema é o microcontrolador ESP32, um poderoso chip que gerencia as funções principais. Ele lê dados do sensor de temperatura, controla o ventilador cooler por PWM (modulação por largura de pulso), envia informações de status para o smartphone via Bluetooth, e responde aos comandos inseridos pelo usuário através do display touch.
- **Ventilador Cooler**: O ventilador é utilizado para resfriar o sistema, garantindo que a temperatura permaneça em níveis seguros. Seu funcionamento é regulado pelo microcontrolador através de sinais PWM, que ajustam a velocidade do ventilador conforme a necessidade, com base na leitura da temperatura.
- **Sensor de Temperatura**: Este sensor monitora constantemente a temperatura do fogão de indução. As leituras são enviadas ao microcontrolador ESP32, que utiliza essas informações para manter o sistema operando dentro dos parâmetros de temperatura seguros, ajustando o ventilador e, se necessário, modulando a potência de indução. 
- **Módulo de Indução**: O componente responsável pelo aquecimento, controlado por PWM pelo microcontrolador ESP32. A modulação por largura de pulso permite o ajuste fino da potência de aquecimento, que é fundamental para cozinhar os alimentos de maneira eficaz e eficiente.
- **Display Touch**: A interface com o usuário, por onde são inseridos os comandos e onde são exibidas as informações do sistema. O display touch permite que o usuário interaja com o fogão, selecionando temperaturas, tempos de cozimento e outras funções.
- **Smartphone (Aplicativo Móvel)**: Permite o controle remoto e monitoramento do fogão de indução. O usuário pode alterar as configurações e verificar o status do sistema através de um aplicativo móvel, que se comunica com o microcontrolador ESP32 via Bluetooth.


## Circuito da Bobina

<p style="text-indent: 50px; text-align: justify;">
No circuito, é utilizado o microcontrolador HT45F0059 da Holtek Semiconductor, utilizado em fogões de indução devido à sua capacidade de gerenciar de forma acertiva a potência de saída, monitorar a temperatura e assegurar a segurança do dispositivo. Ele controla a bobina de indução para aquecer a panela de forma uniforme, ajustando automaticamente a intensidade da corrente elétrica com base nas leituras dos sensores de temperatura. Além disso, suas funções de proteção, como a detecção de sobrecorrente e sobretemperatura, garantem um funcionamento seguro, tornando-o uma escolha excelente para a implementação em eletrodomésticos modernos e inteligentes.
</p>

### Circuito Retificador e filtro

<p style="text-indent: 50px; text-align: justify;"> 
A etapa inicial do circuito é formada por um grupo de componentes que abrange um retificador de onda completa e um mecanismo de filtragem. Inicialmente, o sinal é encaminhado por um circuito de proteção e condicionamento, o qual que inclui um fusível, um potenciômetro, dois resistores em série e um capacitor em paralelo, configuração utilizada para filtrar interferências e assegurar proteção contra sobretensões e picos de energia.
</p>

<p style="text-indent: 50px; text-align: justify;"> 
Após esse primeiro processo, o sinal passa por uma ponte retificadora, a qual transforma a corrente alternada em corrente contínua. Por fim, é introduzido um  filtro, composto por um capacitor e um indutor, com o objetivo de suavizar a corrente contínua, garantindo uma alimentação estável para o bom desempenho de um fogão por indução. 
</p>

<div align="center">
  <img src="../../assets/images/1_circuito_bobina.png" width="500px"">

  <h4>Figura 04 - Circuito Retificador e filtro, composto por um retificador de onda, um potênciomentro, capacitores, resistores e um indutor.</h4>
</div>


### Alimentação chaveada

<p style="text-indent: 50px; text-align: justify;"> 
O circuito de alimentação chaveada opera através do OB2226, que é um controlador de chaveamento que gera pulsos PWM (Modulação por Largura de Pulso), com o objetivo de gerenciar a operação do transistor de potência chaveadoo IGBT. O transformador fornece isolamento elétrico entre o circuito de alta frequência do OB2226 e a fonte de alimentação principal. Ele também desempenha o papel de ajustar a tensão de entrada e de saída conforme necessário para o circuito de chaveamento e para o regulador de tensão 78L05.
</p>

<p style="text-indent: 50px; text-align: justify;"> 
O 78L05 é um regulador de tensão que converte uma tensão de entrada mais alta em uma tensão mais baixa. Nesse circuito é utilizada para fornecer a tensão de +5V para alimentar o buzzer e o display.</p>

<p style="text-indent: 50px; text-align: justify;"> 
Os capacitores, resistores e diodos desempenham funções de filtragem, estabilização, proteção e ajuste no circuito, sendo fundamentais para assegurar um sistema com funcionamento estável, eficaz e seguro.
</p>

<div align="center">
  <img src="../../assets/images/2_circuito_bobina.png" width="500px">
  <h4>Figura 05: Esquemático da alimentação chaveada, composta por um gerador de pulso PWM (OB2226) e regulador de tensão (78L05). </h4>
</div>

### Circuito de ressonância

<p style="text-indent: 50px; text-align: justify;"> 
A terceira parte do esquemático é composta por um circuito ressonante, um indutor do tipo pancake e um IGBT. O circuito ressonante opera em sua frequência natural, o que otimiza a transferência de energia para utensílios que utilizam da tecnologia de indução, garantindo aquecimento rápido e uniforme. Para oferecer uma distribuição uniforme do campo magnético, o indutor pancake é projetado para operar em altas frequências. </p>

<p style="text-indent: 50px; text-align: justify;"> 
O IGBT é um dispositivo semicondutor de potência projetado para operar como um interruptor eletrônico, atuando como interruptor de potência no circuito de ressonância. A tensão utilizada para ativar o gate do IGBT é 18V. Ao ser ativado, permite que a corrente flua através do indutor pancake e do circuito de ressonância, gerando um campo magnético forte. Este campo magnético interage com o utensílio de indução, induzindo corrente elétrica através do princípio da indução eletromagnética, o que resulta em aquecimento.
O controlador PWM ajusta o período de ativação do IGBT para controlar a potência e a frequência do circuito, permitindo um aquecimento controlado e eficiente.</p>


<div align="center">
  <img src="../../assets/images/3_circuito_bobina.png" width="500px">
  <h4>Figura 06: Esquemático do circuito de ressônancia e do interruptor de potência IGTB. </h4>
</div>


## Controle do ventilador e buzzer

<p style="text-indent: 50px; text-align: justify;"> 
O ventilador é responsável pela manutenção do fluxo de ar dentro do sistema, ajudando a dissipar o calor gerado pela indução bobina-panela e dos próprios componentes eletrônicos, mantendo a temperatura dentro de limites seguros de operação.
Quando o sistema de controle detecta que o fogão foi ativado, ele gera um sinal na base do transistor, que ativado, faz com que a corrente flua do coletor para o emissor, alimentando o ventilador e promovendo a circulação do ar e dissipação do calor do sistema pela saída de ar projetada pela estrutura.
Quando o fogão é desligado, o microcontrolador desativa o transistor, interrompendo o fluxo de corrente para o ventilador e desligando-o.</p>

<p style="text-indent: 50px; text-align: justify;"> 
O buzzer também é controlado através do microcontolador, e é ativado sempre que for detectado falhas no sistema, como: superaquecimento, picos de tensão e  corrente. Ademais, será utilizado também para indicar que o tempo de cozimento escolhido pelo usuário se esgotou.
</p>

<div align="center">
  <img src="../../assets/images/4_circuito_bobina.png" width="500px">
  <h4>Figura 07:  Esquemático do circuito de conexão do buzzer e do Cooler utilizandos no sistema. </h4>
</div>

### Controle de superaquecimento da superfície do cooktop e do IGTB

<p style="text-indent: 50px; text-align: justify;"> 
O circuito de proteção contra superaquecimento da superfície do cooktop e do IGBT garante a segurança e o desempenho do fogão de indução. Cada circuito é composto por um termistor NT100K, um capacitor e um resistor. O termistor NT100K monitora a temperatura da superfície do cooktop através da variação da sua resistência em função da temperatura, e com o auxílio do controlador e bibliotecas próprias é possível identificar a temperatura medida. O capacitor possui a função de filtrar e estabilizar o sinal do termistor, reduzindo ruídos e interferências para garantir uma medição precisa da temperatura. O resistor possui a função de formar um divisor de tensão com o termistor, possibilitando o ajuste do sinal de temperatura para que possa ser lido pelo sistema de proteção.
</p>

<p style="text-indent: 50px; text-align: justify;"> 
Os circuitos de proteção contra superaquecimento garantem o monitoramento contínuo das temperaturas críticas, assegurando o acionamento rápida do sistema de ventilação em caso de superaquecimento.
</p>

<div align="center">
  <img src="../../assets/images/6_7_circuito_bobina.png" width="500px">
  <h4>Figura 08: Esquemático do circuitos de proteção contra superaquecimento. </h4>
</div>

### Detecção da corrente

<p style="text-indent: 50px; text-align: justify;"> 
O circuito de detecção de corrente faz o monitoramento da corrente elétrica que flui através do sistema, assegurando uma operação eficiente. Esse circuito é integrado por um potenciômetro de precisão, que é utilizado para calibrar o circuito de detecção de corrente para uma leitura exata. Ademais, é composto por resistores que ajustam a corrente do circuito, mantendo-a dentro da faixa de operação dos componentes eletrônicos. 
Atuando como pontes do circuito, os resistores de 0 ohm permitem a seleção e ajuste de corrente e supressão de ruídos. E os capacitores são utilizados para filtrar e estabilizar o sinal de corrente, diminuindo ruídos e interferências que podem alterar a medição.
</p>

<div align="center">
  <img src="../../assets/images/8_circuito_bobina.png" width="500px">
  <h4>Figura 09: Circuito de detecção de corrente que o monitoramento da corrente elétrica.</h4>
</div>

### Detecção de tensão

<p style="text-indent: 50px; text-align: justify;"> 
O circuito de detecção de tensão possui o objetivo de monitorar a tensão elétrica no sistema do fogão. Este circuito é composto por resistores, resistores de 0 ohm e um diodo. O funcionamento dos dois resistores foram expostos anteriormente. O diodo possui o objetivo de proteger o circuito contra correntes com polaridades contrárias.
</p>


<div align="center">
  <img src="../../assets/images/9_circuito_bobina.png" width="500px">
  <h4>Figura 10:Circuito de detecção de tensão. </h4>
</div>

### Proteção contra sobretensão de surto

<p style="text-indent: 50px; text-align: justify;"> 
O circuito de proteção contra sobretensão de surto protege os componentes eletrônicos do fogão contra picos de tensão indesejados que possam danificar o sistema. o circuito é composto por capacitores, que absorvem a energia de surtos de tensão, já que possuem a capacidade de armazenar energia, e assim, reduz o efeito dos picos de tensão nos demais componentes do sistema eletrônico. Já os resistores são associados aos capacitores para restringir a corrente que flui através do circuito durante um surto de tensão, protegendo os componentes eletrônicos do sistema ao limitar a energia que é dissipada durante os surtos de sobretensão.
</p>


<div align="center">
  <img src="../../assets/images/10_circuito_bobina.png" width="500px">
  <h4>Figura 11: Circuito de proteção contra sobretensão de surto. </h4>
</div>

## Controle do fogão pela EPS32


<div align="center">
  <img src="../../assets/images/Circuito ESP32.png" width="500px">
  <h4>Figura 12:Circuito de detecção de tensão. </h4>
</div>

O circuito será desenvolvido para acionar os botões do fogão de indução utilizando uma ESP32. A ESP32 será responsável por ativar o sensor touch do circuito de acionamento dos botões do fogão. Para definir as funcionalidades desse acionamento, serão utilizados os GPIOs da ESP32, que interagirão com o sensor capacitivo presente no microcontrolador da placa do circuito do fogão. Dessa forma, será possível controlar o fogão de indução de maneira eficiente e precisa, aproveitando as capacidades de toque do sensor e a versatilidade de programação da ESP32.