# 🍳**Grupo 06 - Fogão Inteligente**

**😉 Introdução**

O projeto **“Fogão Inteligente”** está em desenvolvimento pela **“equipe 06”** durante a disciplina **“Projeto Integrador 02”** no semestre 2024/1. O propósito do projeto consiste em construir um fogão por indução que é energizado por bateria recarregável a energia solar. Além disso, o fogão oferece maior controle sobre as variáveis de cozimento através de uma interface de usuário.

**🤝 Documentação**

[Clique aqui](https://docs-fga-pi2-semestre-2024-1-grupo-06-0b350550c6bd5168e7922a7d8.gitlab.io/) para acessar a documentação do projeto.

**⚙️ Instalação**

```markdown
$ pip install mkdocs mkdocs-material

$ git clone https://gitlab.com/fga-pi2/semestre-2024-1/grupo-06/docs.git
$ cd docs
```

**📍 Executar Localmente**

```markdown
$ mkdocs serve
```

📁 **Adição de Novos Documentos**

1. Adicionar novo documento na pasta `./docs`
2. Adicionar o nome do arquivo no `mkdocs.yml`

**😎 Membros da Equipe**

Estudantes da disciplina Projeto Integrador 02 dos cursos de Engenharia Aeroespacial, Engenharia Automotiva, Engenharia de Energia, Engenharia Eletrônica e Engenharia de Software, UnB. Saiba um pouco mais sobre nosso time....

## Alunos

| Aluno | Matrícula | GitLab |
| --- | --- | --- |
| Antônio Igor Carvalho | 180030264 | [spiker32](https://gitlab.com/spiker32) |
| Caio Brandao Santos | 170007413 | [caiobsantos](https://gitlab.com/caiobsantos) |
| Erik Lopes Targino | 190086823 | [Erikloopes](https://gitlab.com/Erikloopes) |
| Geraldo Victor Alves Barbosa | 170011119 | [geraldovictor](https://gitlab.com/geraldovictor) |
| Júlia Beatriz Santos Pereira | 190090081 | [juliabsant](https://gitlab.com/juliabsant) |
| Luan Mateus Cesar Duarte | 211041221 | [luanduartee](https://gitlab.com/luanduartee) |
| Lucas Gomes Caldas | 212005426 | [lucasgcaldas](https://gitlab.com/lucasgcaldas) |
| Lucas Eduardo Batista de Paulo | 190033177 | [Lucasedward](https://gitlab.com/Lucasedward)|
| Mateus Moreira Lima | 180024868 | [mateus_lm](https://gitlab.com/mateus_lm) |
| João Pedro Alves Machado | 212008197 | [pedroblome](https://gitlab.com/pedroblome) |
| Pedro Sampaio Dias Rocha | 211043745 | [PedroSampaioDias](https://gitlab.com/PedroSampaioDias) |
| Rafael Ramos Xavier de Castro | 190134011 | [rrxc33](https://gitlab.com/rrxc33) |
| Thiago Ribeiro de Moraes | 190038608 | [ThiagoRM26](https://gitlab.com/ThiagoRM26) |
| Victório Lázaro Rocha de Morais | 211031860 | [Victorio_Lazaro](https://gitlab.com/Victorio_Lazaro) |
| Washington Siqueira da Macena | 150152124 | [washingtondamacena](https://gitlab.com/washingtondamacena) |